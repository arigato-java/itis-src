/*  This file is generated from config.def by genconfig.  */

#ifndef _CONFIG_
#define _CONFIG_

#define MIN_TSKID	(-7)
#define MAX_TSKID	(32)
#define NUM_TSKID	(35)
#define CHK_TSKID(id)	(((MIN_TSKID) <= (id) && (id) < -4) || (0 < (id) && (id) <= (MAX_TSKID)))
#define SYS_TSKID(id)	((id) < 0)
#define INDEX_TSK(id)	((id) < 0 ? ((id)-(MIN_TSKID)) : ((id)-(MIN_TSKID)-5))
#define ID_TSK(index)	((index) <= (-(MIN_TSKID)-5) ? ((index)+(MIN_TSKID)) : ((index)+(MIN_TSKID)+5))

#define MIN_SEMID	(-8)
#define MAX_SEMID	(64)
#define NUM_SEMID	(68)
#define CHK_SEMID(id)	(((MIN_SEMID) <= (id) && (id) < -4) || (0 < (id) && (id) <= (MAX_SEMID)))
#define SYS_SEMID(id)	((id) < 0)
#define INDEX_SEM(id)	((id) < 0 ? ((id)-(MIN_SEMID)) : ((id)-(MIN_SEMID)-5))
#define ID_SEM(index)	((index) <= (-(MIN_SEMID)-5) ? ((index)+(MIN_SEMID)) : ((index)+(MIN_SEMID)+5))

#define MIN_FLGID	(1)
#define MAX_FLGID	(8)
#define NUM_FLGID	(8)
#define CHK_FLGID(id)	((MIN_FLGID) <= (id) && (id) <= (MAX_FLGID))
#define SYS_FLGID(id)	(0)
#define INDEX_FLG(id)	((id)-(MIN_FLGID))
#define ID_FLG(index)	((index)+(MIN_FLGID))

#define MIN_MBXID	(1)
#define MAX_MBXID	(32)
#define NUM_MBXID	(32)
#define CHK_MBXID(id)	((MIN_MBXID) <= (id) && (id) <= (MAX_MBXID))
#define SYS_MBXID(id)	(0)
#define INDEX_MBX(id)	((id)-(MIN_MBXID))
#define ID_MBX(index)	((index)+(MIN_MBXID))

#define MIN_MBFID	(1)
#define MAX_MBFID	(8)
#define NUM_MBFID	(10)
#define CHK_MBFID(id)	((-4 <= (id) && (id) < -2) || ((MIN_MBFID) <= (id) && (id) <= (MAX_MBFID)))
#define SYS_MBFID(id)	((id) < 0)
#define INDEX_MBF(id)	((id) < 0 ? ((id)-(-4)) : ((id)-(MIN_MBFID)+2))
#define ID_MBF(index)	((index) <= 1 ? ((index)+(-4)) : ((index)+(MIN_MBFID)-2))

#define MIN_PORID	(1)
#define MAX_PORID	(8)
#define NUM_PORID	(8)
#define CHK_PORID(id)	((MIN_PORID) <= (id) && (id) <= (MAX_PORID))
#define SYS_PORID(id)	(0)
#define INDEX_POR(id)	((id)-(MIN_PORID))
#define ID_POR(index)	((index)+(MIN_PORID))

#define MIN_MPLID	(-5)
#define MAX_MPLID	(4)
#define NUM_MPLID	(6)
#define CHK_MPLID(id)	(((MIN_MPLID) <= (id) && (id) < -3) || (0 < (id) && (id) <= (MAX_MPLID)))
#define SYS_MPLID(id)	((id) < 0)
#define INDEX_MPL(id)	((id) < 0 ? ((id)-(MIN_MPLID)) : ((id)-(MIN_MPLID)-4))
#define ID_MPL(index)	((index) <= (-(MIN_MPLID)-4) ? ((index)+(MIN_MPLID)) : ((index)+(MIN_MPLID)+4))

#define MIN_MPFID	(1)
#define MAX_MPFID	(4)
#define NUM_MPFID	(4)
#define CHK_MPFID(id)	((MIN_MPFID) <= (id) && (id) <= (MAX_MPFID))
#define SYS_MPFID(id)	(0)
#define INDEX_MPF(id)	((id)-(MIN_MPFID))
#define ID_MPF(index)	((index)+(MIN_MPFID))

#define NUM_CYC		(20)
#define NUM_ALM		(20)
#define NUM_SVC		(20)

#define MIN_PRI		(1)
#define MAX_PRI		(256)
#define NUM_PRI		(256)
#define CHK_PRI(pri)	((MIN_PRI) <= (pri) && (pri) <= (MAX_PRI))

#define CHK_NOSPT
#define CHK_RSATR
#define CHK_PAR
#define CHK_ID
#define CHK_OACV
#define CHK_CTX1
#define CHK_CTX2

#define SYS_STACK_SIZE	(1024)
#define SYS_MPL_SIZE	(393216)
#define USE_QTSK_PORTION
#define USE_AUTO_ID
#define USE_DEBUG_SUPPORT
#define USE_VGET_TIM

#endif /* _CONFIG_ */
