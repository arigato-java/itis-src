#
# 
# 	    ItIs - ITRON Implementation by Sakamura Lab
# 
# Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
# 
# 			All Rights Reserved
# 
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose is hereby granted without fee, provided
# that both the above copyright notice and this permission notice appear
# in all copies and supporting documentation, and that the names of
# Sakamura Lab and the University of Tokyo not be used in advertising or
# publicity pertaining to distribution of the software without specific,
# written prior permission.  Sakamura Lab makes no representations about
# the suitability of this software for any purpose.  It is provided "as
# is" without express or implied warranty.
# 
#                              NO WARRANTY
#   
# SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
# EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
# USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# 
#  @(#) $Id: Makefile.mcube,v 1.9 1994/01/19 08:24:43 hiro Exp $
# 

#
#  ターゲットシステム名
#
TARGET = mcube

#
#  共通定義
#
include ../Makefile.config

INCLUDES = -I. -I../include -I../$(TARGET) -I/usr4/2b/itron/itis/glibc/include
CFLAGS := -DITIS -DLINK_KERNEL -msoft-float $(INCLUDES) $(CFLAGS)
#CFLAGS := -Wall -DITIS -DLINK_KERNEL -msoft-float $(INCLUDES) $(CFLAGS)
LDFLAGS = -N -L/usr4/2b/itron/itis/glibc/lib

#
#  データセクションの初期化処理を行うかどうかの定義
#
IDATA = true
#IDATA = false

#
#  各セグメントの開始アドレスの定義
#
TEXT_START_ADDRESS=80020000
DATA_START_ADDRESS=80080000

#
#  初期起動タスクの定義
#
FIRST_TASK = scheme

ifeq ($(FIRST_TASK),scheme)
    FTASK_DIR = ../xscheme:../systask
    FTASK_ASMOBJS =
    FTASK_COBJS = xscheme.o xsdmem.o xsimage.o xsio.o xsobj.o \
		  xsprint.o xsread.o xssym.o xsfun1.o xsfun2.o \
		  xsmath.o xsftab.o xsinit.o xscom.o xsint.o \
		  itisstuff.o itis3call.o sysio.o
    FTASK_CFLAGS = -DNO_FLOAT
    FTASK_LIBS = -lc-itis
endif

#
#  システムタスク (システムタスク用ライブラリを含む) の定義
#
STASK_DIR = ../systask:../libitis
STASK_ASMOBJS =
STASK_COBJS = inittask.o serial.o logtask.o \
	      itis_perror.o perror.o strerror.o errno.o syslog.o sysstat.o
STASK_CFLAGS = -msoft-float -I../systask

#
#  カーネル関係の定義
#
KERNEL_DIR = ../kernel:../$(TARGET)
KERNEL_ASMOBJS = $(TARGET)start.o ientry.o dispatch.o
KERNEL_COBJS = startup.o $(TARGET)init.o task.o wait.o timer.o \
	       task_manage.o task_sync.o time_calls.o misc_calls.o \
	       semaphore.o eventflag.o mailbox.o messagebuf.o rendezvous.o \
	       mempool.o mempfix.o debug_calls.o isyslog.o vsprintf.o
KERNEL_CFLAGS = -msoft-float -I../kernel

#
#  ターゲットファイル
#
all: itis.S

# 以下は編集しないこと

SRCDIR = $(KERNEL_DIR):$(STASK_DIR):$(FTASK_DIR)

vpath %.s $(SRCDIR)
vpath %.S $(SRCDIR)
vpath %.c $(SRCDIR)

ifdef TEXT_START_ADDRESS
    LINK_T = -Ttext $(TEXT_START_ADDRESS)
else
    LINK_T =
endif
ifdef DATA_START_ADDRESS
    LINK_D = -Tdata $(DATA_START_ADDRESS)
else
    LINK_D =
endif
LDFLAGS := $(LDFLAGS) $(LINK_T) $(LINK_D)

ASMOBJS = $(KERNEL_ASMOBJS) $(STASK_ASMOBJS) $(FTASK_ASMOBJS)
COBJS = $(KERNEL_COBJS) $(STASK_COBJS) $(FTASK_COBJS)
OBJS = $(ASMOBJS) $(COBJS)
LIBS = $(FTASK_LIBS)

itis.S: itis
	aout2 -v itis

itis.bin: itis
	aout2 -b -v itis

itis: Makefile $(OBJS) version.o
	$(CC) $(LDFLAGS) -o itis version.o $(OBJS) $(LIBS)
ifeq ($(IDATA),true)
	extidata itis
	$(CC) $(LDFLAGS) -o itis version.o $(OBJS) itis-idata.s $(LIBS)
	extidata itis
	$(CC) $(LDFLAGS) -o itis version.o $(OBJS) itis-idata.s $(LIBS)
endif

config.h: config.def ../kernel/genconfig
	../kernel/genconfig config.def

offset.h: makeoffset.s ../kernel/genoffset
	../kernel/genoffset makeoffset.s > offset.h

targettimer.h:
	ln -s ../$(TARGET)/$(TARGET)timer.h targettimer.h

targetserial.h:
	ln -s ../$(TARGET)/$(TARGET)serial.h targetserial.h

version.o: version.c
version.c: Makefile $(OBJS) ../kernel/newversion
	../kernel/newversion -t $(TARGET) > version.c

# config.def がカレントディレクトリにない時は，サンプルをそのまま使う．
config.def:
	ln -s ../sample/config.def

clean:
	rm -f \#* *~ *.o
	rm -f itis itis.S itis.bin version.c itis-idata.s
	rm -f makeoffset.s Makefile.old ems.log

cleandep:
	../utils/deldep Makefile

realclean: cleandep clean
	rm -rf targettimer.h targetserial.h
	rm -rf config.h offset.h

depend: config.h offset.h targettimer.h targetserial.h
	../utils/deldep Makefile
	../utils/makedep -S$(KERNEL_DIR) $(KERNEL_CFLAGS) $(CFLAGS) \
		$(KERNEL_ASMOBJS:.o=.S) $(KERNEL_COBJS:.o=.c) \
		-s makeoffset.c >> Makefile
	../utils/makedep -S$(STASK_DIR) $(STASK_CFLAGS) $(CFLAGS) \
		$(STASK_ASMOBJS:.o=.S) $(STASK_COBJS:.o=.c) >> Makefile
	../utils/makedep -S$(FTASK_DIR) $(FTASK_CFLAGS) $(CFLAGS) \
		$(FTASK_ASMOBJS:.o=.S) $(FTASK_COBJS:.o=.c) >> Makefile

#
#  コンパイラドライバの定義
#
$(KERNEL_COBJS): %.o: %.c
	$(CC) -c $(KERNEL_CFLAGS) $(CFLAGS) $<

$(KERNEL_COBJS:.o=.s): %.s: %.c
	$(CC) -S $(KERNEL_CFLAGS) $(CFLAGS) $<

$(KERNEL_ASMOBJS): %.o: %.S
	$(CC) -c $(KERNEL_CFLAGS) $(CFLAGS) $<

$(STASK_COBJS): %.o: %.c
	$(CC) -c $(STASK_CFLAGS) $(CFLAGS) $<

$(STASK_COBJS:.o=.s): %.s: %.c
	$(CC) -S $(STASK_CFLAGS) $(CFLAGS) $<

ifdef $(STASK_ASMOBJS)
$(STASK_ASMOBJS): %.o: %.S
	$(CC) -c $(STASK_CFLAGS) $(CFLAGS) $<
endif

$(FTASK_COBJS): %.o: %.c
	$(CC) -c $(FTASK_CFLAGS) $(CFLAGS) $<

$(FTASK_COBJS:.o=.s): %.s: %.c
	$(CC) -S $(FTASK_CFLAGS) $(CFLAGS) $<

ifdef $(FTASK_ASMOBJS)
$(FTASK_ASMOBJS): %.o: %.S
	$(CC) -c $(FTASK_CFLAGS) $(CFLAGS) $<
endif

# DO NOT DELETE THIS LINE: makedep depends on it.
mcubestart.o: ../kernel/cregs.h ../mcube/mcubestart.S 
ientry.o: ../include/itron_errno.h ./config.h ../kernel/itable.h \
	../kernel/isysconf.h ../kernel/ientry.S 
dispatch.o: ./offset.h ../kernel/dispatch.S ../kernel/cregs.h 
startup.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/queue.h \
	../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../include/itron_fncode.h \
	../kernel/cpu_inline.h ../kernel/startup.c \
	../kernel/itis_kernel.h ../kernel/patchlevel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
mcubeinit.o: ../include/itron.h ../include/itron_errno.h ./config.h \
	../kernel/common.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../kernel/isysconf.h \
	../mcube/mcubeinit.c ../mcube/mcube.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
task.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/ready_queue.h \
	../kernel/wait.h ../kernel/queue.h ../kernel/task.h \
	../kernel/common.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../kernel/isysconf.h \
	../include/itron_fncode.h ../kernel/task.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
wait.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/task.h \
	../kernel/queue.h ../kernel/wait.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../include/itron_fncode.h \
	../kernel/wait.c ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
timer.o: ../include/itron.h ./targettimer.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/queue.h \
	../kernel/common.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../kernel/isysconf.h \
	../mcube/mcube.h ../include/itron_fncode.h \
	../kernel/cpu_inline.h ../kernel/timer.c \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
task_manage.o: ../include/itron.h ../kernel/task_manage.c \
	../kernel/winfo.h ../kernel/timer.h ../include/itron_errno.h \
	./config.h ../kernel/wait.h ../kernel/task.h \
	../kernel/queue.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
task_sync.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/task_sync.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/limits.h 
time_calls.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/task.h ../kernel/queue.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../kernel/time_calls.c ../include/itron_fncode.h \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
misc_calls.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/queue.h \
	../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/misc_calls.c \
	../kernel/patchlevel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
semaphore.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/semaphore.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
eventflag.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/eventflag.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
mailbox.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/mailbox.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
messagebuf.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/messagebuf.c \
	../kernel/check.h ../include/itron_fncode.h \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
rendezvous.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/rendezvous.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
mempool.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/mempool.c ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
mempfix.o: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/wait.h \
	../kernel/queue.h ../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/check.h \
	../include/itron_fncode.h ../kernel/mempfix.c \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
debug_calls.o: ../include/itron.h ../kernel/debug_calls.c \
	../kernel/winfo.h ../kernel/timer.h ../include/itron_errno.h \
	./config.h ../kernel/queue.h ../kernel/task.h \
	../kernel/common.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../kernel/isysconf.h \
	../kernel/check.h ../include/itron_fncode.h \
	../kernel/cpu_inline.h ../kernel/itis_kernel.h \
	../kernel/cregs.h ../kernel/isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
isyslog.o: ../include/itron.h ../include/itron_errno.h ./config.h \
	../kernel/common.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../kernel/isysconf.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/va-tron.h \
	../kernel/isyslog.c \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/stdarg.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
vsprintf.o: ../include/itron.h ../kernel/vsprintf.c \
	../include/itron_errno.h ./config.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/va-tron.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/stdarg.h \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
makeoffset.s: ../include/itron.h ../kernel/winfo.h ../kernel/timer.h \
	../include/itron_errno.h ./config.h ../kernel/queue.h \
	../kernel/task.h ../kernel/common.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../kernel/isysconf.h ../kernel/makeoffset.c \
	../include/itron_fncode.h ../kernel/cpu_inline.h \
	../kernel/itis_kernel.h ../kernel/cregs.h \
	../kernel/isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
inittask.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../systask/inittask.c \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
serial.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../mcube/mcube.h ./targetserial.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../systask/serial.c ../include/itis_ioctl.h \
	../include/itis_syslog.h ../include/itis_debug.h 
logtask.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_resources.h \
	../include/itis_stddefs.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/va-tron.h \
	../systask/logtask.c \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/stdarg.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
itis_perror.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_stddefs.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h \
	../libitis/itis_perror.c 
perror.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_stddefs.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h \
	../libitis/perror.c 
strerror.o: ../include/itron.h ../include/itis_services.h \
	../libitis/strerror.c ../include/itron_errno.h \
	../include/itis_stddefs.h ../include/itron_fncode.h \
	../include/itis_isyscall.h ../include/itis_debug.h 
errno.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_stddefs.h \
	../include/itron_fncode.h ../libitis/errno.c \
	../include/itis_isyscall.h ../include/itis_syslog.h \
	../include/itis_debug.h 
syslog.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../libitis/syslog.c \
	../include/itis_stddefs.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/va-tron.h \
	/GNU/tron/lib/gcc-lib/tron/2.5.2/include/stdarg.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_syslog.h ../include/itis_debug.h 
sysstat.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_stddefs.h \
	../libitis/sysstat.c ../include/itron_fncode.h \
	../include/itis_isyscall.h ../include/itis_debug.h 
xscheme.o: ../xscheme/xsproto.h ../xscheme/xscheme.h \
	../xscheme/xscheme.c 
xsdmem.o: ../xscheme/xsdmem.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsimage.o: ../xscheme/xsproto.h ../xscheme/xscheme.h \
	../xscheme/xsimage.c 
xsio.o: ../xscheme/xsio.c ../xscheme/xsproto.h ../xscheme/xscheme.h 
xsobj.o: ../xscheme/xsobj.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsprint.o: ../xscheme/xsproto.h ../xscheme/xscheme.h \
	../xscheme/xsprint.c 
xsread.o: ../xscheme/xsread.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xssym.o: ../xscheme/xssym.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsfun1.o: ../xscheme/xsfun1.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsfun2.o: ../xscheme/xsfun2.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsmath.o: ../xscheme/xsmath.c ../xscheme/xsproto.h \
	../xscheme/xscheme.h 
xsftab.o: ../xscheme/osptrs.h ../xscheme/xsftab.c \
	../xscheme/xsproto.h ../xscheme/xscheme.h \
	../xscheme/osdefs.h 
xsinit.o: ../xscheme/xsbcode.h ../xscheme/xsinit.c \
	../xscheme/xsproto.h ../xscheme/xscheme.h 
xscom.o: ../xscheme/xsbcode.h ../xscheme/xscom.c \
	../xscheme/xsproto.h ../xscheme/xscheme.h 
xsint.o: ../xscheme/xsbcode.h ../xscheme/xsproto.h \
	../xscheme/xscheme.h ../xscheme/xsint.c 
itisstuff.o: ../include/itron.h ../include/itis_services.h \
	../xscheme/itisstuff.c ../include/itron_errno.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../xscheme/xsproto.h ../xscheme/xscheme.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_ioctl.h ../include/itis_debug.h 
itis3call.o: ../include/itron.h ../include/itis_services.h \
	../xscheme/itis3call.c ../include/itron_errno.h \
	../include/itis_resources.h ../include/itis_stddefs.h \
	../xscheme/xsproto.h ../xscheme/xscheme.h \
	../include/itron_fncode.h ../include/itis_isyscall.h \
	../include/itis_debug.h 
sysio.o: ../include/itron.h ../include/itis_services.h \
	../include/itron_errno.h ../include/itis_resources.h \
	../include/itis_stddefs.h ../include/itron_fncode.h \
	../include/itis_isyscall.h ../systask/sysio.c \
	../include/itis_debug.h 
