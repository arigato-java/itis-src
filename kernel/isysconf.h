/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-94 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: isysconf.h,v 1.4 1994/01/19 09:34:37 hiro Exp $
 */

#ifndef _ISYSCONF_
#define _ISYSCONF_

/*
 *  システムコール構成設定ファイル
 */

/*
 *  システムスタックサイズのデフォルト値
 */
#ifndef SYS_STACK_SIZE
#define SYS_STACK_SIZE	1024
#endif /* SYS_STACK_SIZE */

/*
 *  システムメモリプールサイズのデフォルト値
 */
#ifndef TMPL_OS_SIZE
#define TMPL_OS_SIZE	0x60000
#endif /* TMPL_OS_SIZE */

/*
 *  使用しないシステムコールに関する定義
 */
#ifndef NUM_PORID
#define _i_cre_por	_no_support
#define _i_vcre_por	_no_support
#define _i_del_por	_no_support
#define _i_cal_por	_no_support
#define _i_pcal_por	_no_support
#define _i_tcal_por	_no_support
#define _i_acp_por	_no_support
#define _i_pacp_por	_no_support
#define _i_tacp_por	_no_support
#define _i_fwd_por	_no_support
#define _i_rpl_rdv	_no_support
#define _i_ref_por	_no_support
#endif /* NUM_PORID */

#ifndef NUM_CYC
#define _i_def_cyc	_no_support
#define _i_vdef_cyc	_no_support
#define _i_act_cyc	_no_support
#define _i_ref_cyc	_no_support
#endif /* NUM_CYC */

#ifndef NUM_ALM
#define _i_def_alm	_no_support
#define _i_vdef_alm	_no_support
#define _i_ref_alm	_no_support
#endif /* NUM_ALM */

#ifndef NUM_SVC
#define _i_def_svc	_no_support
#endif /* NUM_SVC */

#ifndef USE_AUTO_ID
#define _i_vcre_tsk	_no_support
#define _i_vcre_sem	_no_support
#define _i_vcre_flg	_no_support
#define _i_vcre_mbx	_no_support
#define _i_vcre_mbf	_no_support
#define _i_vcre_por	_no_support
#define _i_vcre_mpl	_no_support
#define _i_vcre_mpf	_no_support
#define _i_vdef_cyc	_no_support
#define _i_vdef_alm	_no_support
#endif /* USE_AUTO_ID */

#ifndef USE_DEBUG_SUPPORT
#define _i_vget_reg	_no_support
#define _i_vset_reg	_no_support
#endif /* USE_DEBUG_SUPPORT */

#ifndef USE_VGET_TIM
#define _i_vget_tim	_no_support
#endif /* USE_VGET_TIM */

/*
 *  アセンブラで記述されているシステムコールのための定義
 */
#ifdef _i_dis_dsp
#define _z_dis_dsp	_i_dis_dsp
#endif /* _i_dis_dsp */
#ifdef _i_ena_dsp
#define _z_ena_dsp	_i_ena_dsp
#endif /* _i_ena_dsp */
#ifdef _i_loc_cpu
#define _z_loc_cpu	_i_loc_cpu
#endif /* _i_loc_cpu */
#ifdef _i_unl_cpu
#define _z_unl_cpu	_i_unl_cpu
#endif /* _i_unl_cpu */
#ifdef _i_chg_ims
#define _z_chg_ims	_i_chg_ims
#endif /* _i_chg_ims */
#ifdef _i_ref_ims
#define _z_ref_ims	_i_ref_ims
#endif /* _i_ref_ims */

#endif /* _ISYSCONF_ */
