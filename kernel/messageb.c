/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: messagebuf.c,v 1.5 1994/01/07 03:26:12 hiro Exp $
 */

#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "queue.h"
#include "check.h"

/*
 *  メッセージバッファ管理ブロックの定義
 *
 *  1つのメッセージバッファに対して，受信待ち (TTW_MBF) のタスクと送信
 *  待ち (TTW_SMBF) のタスクが同時に存在することはないため，待ちキュー
 *  を共用できる可能性があるが，メッセージバッファのサイズが 0 の時に，
 *  待ちキューがどちらの目的で使われているか判別するのが面倒になるため，
 *  この方法は採用しない．
 */

typedef struct messagebuffer_control_block {
	QUEUE	wait_queue;	/* メッセージバッファ受信待ちキュー */
	ID	mbfid;		/* メッセージバッファID */
	VP	exinf;		/* 拡張情報 */
	ATR	mbfatr;		/* メッセージバッファ属性 */
	QUEUE	send_queue;	/* メッセージバッファ送信待ちキュー */
	INT	bufsz;		/* メッセージバッファのサイズ */
	INT	maxmsz;		/* メッセージの最大長 */
	INT	frbufsz;	/* 空きバッファのサイズ */
	INT	head;		/* 最初のメッセージの格納場所 */
	INT	tail;		/* 最後のメッセージの格納場所の次 */
	VB	*buffer;	/* メッセージバッファの番地 */
} MBFCB;

static MBFCB	mbfcb_table[NUM_MBFID];

#define get_mbfcb(id)	(&(mbfcb_table[INDEX_MBF(id)]))

/*
 *  未使用のメッセージバッファ管理ブロックのリスト
 */
#ifndef _i_vcre_mbf
QUEUE	free_mbfcb;
#endif /* _i_vcre_mbf */

/* 
 *  メッセージバッファ管理ブロックの初期化
 */
void
messagebuffer_initialize()
{
	INT	i;
	MBFCB	*mbfcb;

#ifndef _i_vcre_mbf
	queue_initialize(&free_mbfcb);
#endif /* _i_vcre_mbf */

	for(mbfcb = mbfcb_table, i = 0; i < NUM_MBFID; mbfcb++, i++) {
		mbfcb->mbfid = 0;
#ifndef _i_vcre_mbf
		if (!SYS_MBFID(ID_MBF(i))) {
			queue_insert(&(mbfcb->wait_queue), &free_mbfcb);
		}
#endif /* _i_vcre_mbf */
	}
}

/*
 *  メッセージバッファ操作ルーチン
 */

typedef INT		HEADER;
#define HEADERSZ	sizeof(HEADER)

#define ROUNDSIZE	sizeof(HEADER)
#define	ROUNDSZ(sz)	(((sz) + (ROUNDSIZE-1)) & ~(ROUNDSIZE-1))

/*
 *  サイズが msgsz メッセージが，メッセージバッファに入るかどうかチェッ
 *  クする．
 *
 *  本来は msgsz ではなく ROUNDSZ(msgsz) とすべきであるが，HEADERSZ も 
 *  mbfcb->frbufsz も ROUNDSZ されているため，同じことになる．
 */
Inline BOOL
mbf_free(MBFCB* mbfcb, INT msgsz)
{
	return(HEADERSZ + msgsz <= mbfcb->frbufsz);
}

/*
 *  メッセージバッファが空かどうかチェックする．
 */
Inline BOOL
mbf_empty(MBFCB* mbfcb)
{
	return(mbfcb->frbufsz == mbfcb->bufsz);
}

/*
 *  メッセージを，メッセージバッファへ追加する．
 */
static void
msg_to_mbf(MBFCB* mbfcb, VP msg, INT msgsz)
{
	INT	tail = mbfcb->tail;
	VB	*buffer = mbfcb->buffer;
	INT	remsz;

	mbfcb->frbufsz -= HEADERSZ + ROUNDSZ(msgsz);
	*((HEADER *) &(buffer[tail])) = msgsz;
	tail += HEADERSZ;
	if (tail >= mbfcb->bufsz) {
		tail = 0;
	}

	if ((remsz = mbfcb->bufsz - tail) < msgsz) {
		memcpy(&(buffer[tail]), msg, remsz);
		((VB *) msg) += remsz;
		msgsz -= remsz;
		tail = 0;
	}
	memcpy(&(buffer[tail]), msg, msgsz);
	tail += ROUNDSZ(msgsz);
	if (tail >= mbfcb->bufsz) {
		tail = 0;
	}
	mbfcb->tail = tail;
}

/*
 *  メッセージバッファから，メッセージを取り出す．
 */
static INT
mbf_to_msg(MBFCB* mbfcb, VP msg)
{
	INT	head = mbfcb->head;
	VB	*buffer = mbfcb->buffer;
	INT	msgsz;
	INT	remsz;

	msgsz = *((HEADER *) &(buffer[head]));
	head += HEADERSZ;
	if (head >= mbfcb->bufsz) {
		head = 0;
	}
	mbfcb->frbufsz += HEADERSZ + ROUNDSZ(msgsz);

	if ((remsz = mbfcb->bufsz - head) < msgsz) {
		memcpy(msg, &(buffer[head]), remsz);
		((VB *) msg) += remsz;
		msgsz -= remsz;
		head = 0;
	}
	memcpy(msg, &(buffer[head]), msgsz);
	head += ROUNDSZ(msgsz);
	if (head >= mbfcb->bufsz) {
		head = 0;
	}
	mbfcb->head = head;
	return(msgsz);
}

/*
 *  メッセージバッファ管理機能
 */

#if !defined(_i_cre_mbf) || !defined(_i_vcre_mbf)

static ER
_cre_mbf(MBFCB *mbfcb, T_CMBF *pk_cmbf, INT bufsz)
{
	ID	mbfid = ID_MBF(mbfcb - mbfcb_table);

	if (bufsz > 0 && (mbfcb->buffer = sys_get_blk(bufsz)) == 0) {
		return(E_NOMEM);
	}

#ifndef _i_vcre_mbf
	if (!SYS_MBFID(mbfid)) {
		queue_delete(&(mbfcb->wait_queue));
	}
#endif /* _i_vcre_mbf */

	queue_initialize(&mbfcb->wait_queue);
	mbfcb->mbfid = mbfid;
	mbfcb->exinf = pk_cmbf->exinf;
	mbfcb->mbfatr = pk_cmbf->mbfatr;
	queue_initialize(&mbfcb->send_queue);
	mbfcb->bufsz = mbfcb->frbufsz = bufsz;
	mbfcb->maxmsz = pk_cmbf->maxmsz;
	mbfcb->head = mbfcb->tail = 0;
	return(E_OK);
}

#endif /* !defined(_i_cre_mbf) || !defined(_i_vcre_mbf) */
#ifndef _i_cre_mbf

SYSCALL ER
i_cre_mbf(ID mbfid, T_CMBF *pk_cmbf)
{
	MBFCB	*mbfcb;
	INT	bufsz;
	ER	ercd = E_OK;

	CHECK_MBFID(mbfid);
	CHECK_MBFACV(mbfid);
	CHECK_RSATR(pk_cmbf->mbfatr, TA_TPRI);
	CHECK_PAR(pk_cmbf->bufsz >= 0);
	CHECK_PAR(pk_cmbf->maxmsz > 0);
	bufsz = ROUNDSZ(pk_cmbf->bufsz);
	mbfcb = get_mbfcb(mbfid);

	BEGIN_CRITICAL_SECTION;
	if (mbfcb->mbfid != 0) {
		ercd =  E_OBJ;
	}
	else {
		ercd = _cre_mbf(mbfcb, pk_cmbf, bufsz);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_mbf */
#ifndef _i_vcre_mbf

SYSCALL ER
i_vcre_mbf(T_CMBF *pk_cmbf)
{
	MBFCB	*mbfcb;
	INT	bufsz;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_cmbf->mbfatr, TA_TPRI);
	CHECK_PAR(pk_cmbf->bufsz >= 0);
	CHECK_PAR(pk_cmbf->maxmsz > 0);
	bufsz = ROUNDSZ(pk_cmbf->bufsz);

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_mbfcb)) {
		ercd =  EV_FULL;
	}
	else {
		mbfcb = (MBFCB *)(free_mbfcb.next);
		ercd = _cre_mbf(mbfcb, pk_cmbf, bufsz);
		if (ercd == E_OK) {
			ercd = (ER)(mbfcb->mbfid);
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_mbf */

SYSCALL ER
i_del_mbf(ID mbfid)
{
	MBFCB	*mbfcb;
	ER	ercd = E_OK;

	CHECK_MBFID(mbfid);
	CHECK_MBFACV(mbfid);
	mbfcb = get_mbfcb(mbfid);

	BEGIN_CRITICAL_SECTION;
	if (mbfcb->mbfid == 0) {
		ercd = E_NOEXS;
	}
	else {
		sys_rel_blk(mbfcb->buffer);
		wait_delete(&(mbfcb->wait_queue));
		wait_delete(&(mbfcb->send_queue));
#ifndef _i_vcre_mbf
		if (!SYS_MBFID(mbfcb->mbfid)) {
			queue_insert(&(mbfcb->wait_queue), &free_mbfcb);
		}
#endif /* _i_vcre_mbf */
		mbfcb->mbfid = 0;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_snd_mbf(ID mbfid, VP msg, INT msgsz)
{
	return(i_tsnd_mbf(mbfid, msg, msgsz, TMO_FEVR));
}

SYSCALL ER
i_psnd_mbf(ID mbfid, VP msg, INT msgsz)
{
	return(i_tsnd_mbf(mbfid, msg, msgsz, TMO_POL));
}

SYSCALL ER
i_tsnd_mbf(ID mbfid, VP msg, INT msgsz, TMO tmout)
{
	MBFCB	*mbfcb;
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_MBFID(mbfid);
	CHECK_MBFACV(mbfid);
	CHECK_PAR(msgsz > 0);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH_POL(tmout);
	mbfcb = get_mbfcb(mbfid);

	BEGIN_CRITICAL_SECTION;
	if (mbfcb->mbfid == 0) {
		ercd = E_NOEXS;
	}
#ifdef CHK_PAR
	else if (msgsz > mbfcb->maxmsz) {
		ercd = E_PAR;
	}
#endif /* CHK_PAR */
	else if (!queue_empty_p(&(mbfcb->wait_queue))) {
		tcb = (TCB *)(mbfcb->wait_queue.next);
		memcpy(tcb->winfo.mbf.msg, msg, msgsz);
		*(tcb->winfo.mbf.p_msgsz) = msgsz;
		wait_release_ok(tcb);
	}
	else if (mbf_free(mbfcb, msgsz)) {
		msg_to_mbf(mbfcb, msg, msgsz);
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_SMBF;
			ctxtsk->wid = mbfid;
			ctxtsk->wercd = &ercd;
			ctxtsk->winfo.smbf.msg = msg;
			ctxtsk->winfo.smbf.msgsz = msgsz;
			make_wait(tmout);
			queue_insert(&(ctxtsk->tskque), &(mbfcb->send_queue));
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_rcv_mbf(VP msg, INT *p_msgsz, ID mbfid)
{
	return(i_trcv_mbf(msg, p_msgsz, mbfid, TMO_FEVR));
}

SYSCALL ER
i_prcv_mbf(VP msg, INT *p_msgsz, ID mbfid)
{
	return(i_trcv_mbf(msg, p_msgsz, mbfid, TMO_POL));
}

SYSCALL ER
i_trcv_mbf(VP msg, INT *p_msgsz, ID mbfid, TMO tmout)
{
	MBFCB	*mbfcb;
	QUEUE	*queue;
	TCB	*tcb;
	INT	msgsz;
	ER	ercd = E_OK;

	CHECK_MBFID(mbfid);
	CHECK_MBFACV(mbfid);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH();
	mbfcb = get_mbfcb(mbfid);
    
	BEGIN_CRITICAL_SECTION;
	if (mbfcb->mbfid == 0) {
		ercd = E_NOEXS;
	}
	else if (!mbf_empty(mbfcb)) {
		*p_msgsz = mbf_to_msg(mbfcb, msg);
		queue = mbfcb->send_queue.next;
		while (queue != &(mbfcb->send_queue)) {
			tcb = (TCB *) queue;
			queue = queue->next;
			msgsz = tcb->winfo.smbf.msgsz;
			if (mbf_free(mbfcb, msgsz)) {
				msg_to_mbf(mbfcb, tcb->winfo.smbf.msg, msgsz);
				wait_release_ok(tcb);
			}
		}
	}
	else if (!queue_empty_p(&(mbfcb->send_queue))) {
		tcb = (TCB *)(mbfcb->send_queue.next);
		*p_msgsz = tcb->winfo.smbf.msgsz;
		memcpy(msg, tcb->winfo.smbf.msg, *p_msgsz);
		wait_release_ok(tcb);
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_MBF;
			ctxtsk->wercd = &ercd;
			ctxtsk->winfo.mbf.msg = msg;
			ctxtsk->winfo.mbf.p_msgsz = p_msgsz;
			gcb_make_wait((GCB *) mbfcb, tmout);
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_ref_mbf(T_RMBF *pk_rmbf, ID mbfid)
{
	MBFCB	*mbfcb;
	ER	ercd = E_OK;

	CHECK_MBFID(mbfid);
	CHECK_MBFACV(mbfid);
	mbfcb = get_mbfcb(mbfid);

	BEGIN_CRITICAL_SECTION;
	if (mbfcb->mbfid == 0) {
		ercd = E_NOEXS;
	}
	else {
		pk_rmbf->exinf = mbfcb->exinf;
		pk_rmbf->wtsk = wait_tskid(&(mbfcb->wait_queue));
		pk_rmbf->stsk = wait_tskid(&(mbfcb->send_queue));
		pk_rmbf->msgsz = mbf_empty(mbfcb) ? FALSE :
				*((HEADER *) &(mbfcb->buffer[mbfcb->head]));
		pk_rmbf->frbufsz = mbfcb->frbufsz;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  メッセージバッファ待ちのタスクの優先度が変更された場合の処理
 */
void
mbf_chg_pri(TCB *tcb)
{
	MBFCB	*mbfcb;

	mbfcb = get_mbfcb(tcb->wid);
	assert(mbfcb->mbfid != 0);
	gcb_change_priority((GCB *) mbfcb, tcb);
}

/*
 *  システムログ用メッセージバッファへの送信
 */

#define log_mbfcb	get_mbfcb(TMBF_OS)

ER
log_snd_mbf(VP msg, INT msgsz)
{
	TCB	*tcb;
	ER	ercd = E_OK;

	BEGIN_CRITICAL_SECTION;
	if (log_mbfcb->mbfid == 0) {
		ercd = E_NOEXS;
	}
	else if (!queue_empty_p(&(log_mbfcb->wait_queue))) {
		tcb = (TCB *)(log_mbfcb->wait_queue.next);
		memcpy(tcb->winfo.mbf.msg, msg, msgsz);
		*(tcb->winfo.mbf.p_msgsz) = msgsz;
		wait_release_ok(tcb);
	}
	else if (mbf_free(log_mbfcb, msgsz)) {
		msg_to_mbf(log_mbfcb, msg, msgsz);
	}
	else {
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}
