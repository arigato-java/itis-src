/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: timer.c,v 1.2 1993/12/07 09:14:26 hiro Exp $
 */

#include "itis_kernel.h"
#include "targettimer.h"
#include "timer.h"

/*
 *  現在時刻 (ソフトウェアクロック)
 */
SYSTIME	current_time;

/* 
 *  タイマイベントキュー
 */
static QUEUE	timer_queue;

/*
 *  C_timer_handler を起動するためのアセンブラルーチン
 */
extern void	timer_handler(void);

/*
 *  タイマモジュールの初期化
 */
void
timer_initialize(void)
{
	current_time = 0;
	queue_initialize(&timer_queue);
	start_hw_timer(timer_handler);
}

/*
 *  タイマの停止
 */
void
timer_shutdown(void)
{
	terminate_hw_timer();
}

/*
 *  タイマイベントをタイマイベントキューへ挿入
 */
static void
enqueue_tmeb(TMEB *event)
{
	QUEUE	*q;

	for (q = timer_queue.next; q != &timer_queue; q = q->next) {
		if (event->time <= ((TMEB *) q)->time) {
			break;
		}
	}
	queue_insert(&(event->queue), q);
}

/*
 *  タイマイベント event を，タイムアウト時間 tmout 後に起動されるよう
 *  にタイマキューへ登録する．タイムアウト時間が来ると，コールバック関 
 *  数 callback に引数 arg を与えて起動する．tmout が TMO_FEVR の時は，
 *  タイマキューには登録しないが，後で timer_delete が呼ばれてもよいよ
 *  うに，キューのエリアを初期化する．
 */
void
timer_insert(TMEB *event, TMO tmout, CBACK callback, VP arg)
{
	event->callback = callback;
	event->arg = arg;

	if (tmout == TMO_FEVR) {
		queue_initialize(&(event->queue));
	}
	else {
		assert(tmout > 0);
		event->time = current_time + tmout;
		enqueue_tmeb(event);
	}
}

/*
 *  タイマイベント event を，(絶対)時刻 time に起動されるようににタイマ
 *  キューへ登録する．
 */
void
timer_insert_abs(TMEB *event, SYSTIME *time, CBACK callback, VP arg)
{
	event->callback = callback;
	event->arg = arg;

	event->time = *time;
	enqueue_tmeb(event);
}

/*
 *  タイマイベント event を，time 時間後に起動されるようににタイマキュー
 *  へ登録する．
 */
void
timer_insert_rel(TMEB *event, SYSTIME *time, CBACK callback, VP arg)
{
	event->callback = callback;
	event->arg = arg;

	event->time = current_time + *time;
	enqueue_tmeb(event);
}

/*
 *  タイマ割込みハンドラ
 *
 *  タイマ割込みハンドラは，ハードウェアタイマにより TIMER_PERIODミリ秒
 *  の周期で起動される．ソフトウェアクロックを更新し，起動時間の来たタ
 *  イマイベントの起動を行う．
 */
void
C_timer_handler(void)
{
	TMEB	*event;

	clear_hw_timer_interrupt();		/* タイマ割込みのクリア */

	BEGIN_CRITICAL_SECTION;
	current_time += TIMER_PERIOD;

	while (!queue_empty_p(&timer_queue)) {
		event = (TMEB *)(timer_queue.next);
		if (event->time <= current_time) {
			queue_delete(&(event->queue));
			if (event->callback) {
				(*(event->callback))(event->arg);
			}
		}
		else {
			break;
		}
	}
	END_CRITICAL_SECTION;
}

/*
 *  C_timer_handler を起動するためのアセンブラルーチン
 *
 *  TRON仕様チップに依存している．
 */
__asm__(".text			\n"
"	.align 1		\n"
"_timer_handler:		\n"
"	stm (r0-r6), @-sp	\n"
"	bsr _C_timer_handler	\n"
"	ldm @sp+, (r0-r6)	\n"
#ifndef TRON_NO_DI
"	reit			\n");
#else /* TRON_NO_DI */
"	bra __ret_int		\n");
#endif /* TRON_NO_DI */

/*
 *  性能評価用システム時刻参照機能
 */

#ifndef _i_vget_tim

SYSCALL ER
i_vget_tim(SYSUTIME *pk_utim)
{
	SYSTIME	ctime;
	TICK	tick;
	BOOL	ireq;

	BEGIN_CRITICAL_SECTION;
	ctime = current_time;
	tick = get_current_hw_time();
	ireq = fetch_hw_timer_interrupt();
	END_CRITICAL_SECTION;

	if (ireq && tick < (TO_TICK(TIMER_PERIOD) - GET_TOLERANCE)) {
		ctime += 1;
	}
	*pk_utim = ctime * 1000 + TO_USEC(tick);
	return(E_OK);
}

#endif /* _i_vget_tim */
