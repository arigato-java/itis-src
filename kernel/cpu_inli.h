/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: cpu_inline.h,v 1.7 1993/12/20 07:29:50 hiro Exp $
 */

#ifndef	_CPU_INLINE_
#define	_CPU_INLINE_

#include <itis_stddefs.h>
#include "cregs.h"

/* 
 *  低レベルの CPU操作ルーチン
 *
 *  TRON仕様チップに依存している関数が多い．CPU に依存せずに記述できな
 *  い部分は，#ifdef tron で囲んでいない．
 */

/*
 *  PSW の現在値の読み出し
 */
Inline UINT
current_psw(void)
{
	UINT	psw;

	Asm("stc %1, %0" : "=g"(psw) : "m"(CREG(PSW)));
	return(psw);
}

/*
 *  IMASK の現在値の読み出し
 *
 *  IMASK の値として，0 以上 15 以下の値が返る．
 */
#define imask_psw(psw)	((psw >> 16) & 15)
#define current_imask() (imask_psw(current_psw()))

/*
 *  IMASK の現在値の変更
 *
 *  imask は，0 以上 15 以下であること．
 */
Inline void
set_imask(INT imask)
{
	Asm("ldc %0, %1" : : "g"(imask << 16), "m"(CREG(IMASK)));
}

/*
 *  NMI を除くすべての割込みを禁止し，禁止前の割込みマスク状態を内部表
 *  現で返す．
 */
Inline INT
disint(void)
{
	INT	old_intmask;

	Asm("stc %1, %0; ldc #0, %1" : "=g"(old_intmask) : "m"(CREG(IMASK)));
	return(old_intmask);
}

/*
 *  disint が返した割込みマスク状態を渡して，割込み禁止前の状態に戻す．
 */
Inline void
enaint(INT intmask)
{
	Asm("ldc %0, %1" : : "g"(intmask), "m"(CREG(IMASK)));
}

/*
 *  DIR (遅延割込み要求レジスタ) の現在値の変更
 *
 *  dir は，0 以上 15 以下であること．
 */
Inline void
set_dir(INT dir)
{
	Asm("ldc %0, %1" : : "g"(dir), "m"(CREG(DIR)));
}

/*
 *  現在のタスクのコンテキストを捨てて，次に実行すべきタスクへ強制的に
 *  ディスパッチする．
 *
 *  システム起動時および ext_tsk, ext_tsk の処理で用いる．
 *
 *  処理内容としては，dispatch_to_schedtsk へ分岐するだけである．分岐後
 *  はタスクのコンテキストは捨てられるので関数呼出しでも基本的には問題
 *  ないが，SPI を使っているモードで呼んだ場合 (正常に使っていれば，シ
 *  ステム起動時のみ) に，bra 命令で分岐した方が関数呼出しを行うよりも
 *  返り番地のスタック領域分が (わずか 4バイトだけではあるが) 節約でき
 *  る．
 */
Inline void volatile
force_dispatch(void)
{
#ifdef tron
	Asm("bra _dispatch_to_schedtsk");
#else /* tron */
	extern void	dispatch_to_schedtsk(void);

	dispatch_to_schedtsk();
#endif /* tron */
}

/*
 *  EITハンドラの設定
 *
 *  ベクトル番号 eitvec の EITベクタテーブルエントリの，EIT属性 (EITハ
 *  ンドラ実行時の，スタックモード，アドレス変換モード，デバッグモード，
 *  割込みマスクを決める) を eitatr，EITハンドラ開始番地を eithdr に設
 *  定する．
 */
Inline void
define_eit(INT eitvec, UINT eitatr, FP eithdr)
{
	struct eit_vector_entry {
		UINT	eitatr;
		FP	eithdr;
	} *eitvt;

	Asm("stc %1, %0" : "=r"(eitvt) : "m"(CREG(EITVB)));
	eitvt[eitvec].eitatr = eitatr;
	eitvt[eitvec].eithdr = eithdr;
}

/*
 *  EIT関連の定義
 */

#define EITATR(smode, imask)	(((smode) << 31) + ((imask) << 16))

#define EITVEC_TRAPA1	0x21		/* TRAPA #1 のベクタ番号 */
#define EITVEC_TRAPA2	0x22		/* TRAPA #2 のベクタ番号 */
#define EITVEC_TRAPA3	0x23		/* TRAPA #3 のベクタ番号 */
#define EITVEC_TRAPA14	0x2e		/* TRAPA #14 のベクタ番号 */
#define EITVEC_DI14	0x5e		/* DI=14 のベクタ番号 */

/*
 *  メモリブロック操作ライブラリ
 *
 *  関数の仕様は，ANSI C ライブラリの仕様と同じ．標準ライブラリのものを
 *  使った方が効率が良い可能性がある．
 */

Inline VP
memcpy(VP dest, VP src, UINT len)
{
#ifdef tron
	if (len > 0) {
		register VP	src_r0 asm("r0") = src;
		register VP	dest_r1 asm("r1") = dest;
		register UINT	len_r2 asm("r2") = len;

		asm("smov/f.b"
		  : /* no output */
		  : "g"(src_r0), "g"(dest_r1), "g"(len_r2)
		  : "r0", "r1", "r2");
	}
#else /* tron */
	VB	*d = dest;
	VB	*s = src;

	while (len-- > 0) {
		*d++ = *s++;
	}
#endif /* tron */
	return(dest);
}

Inline VP
memset(VP str, INT ch, UINT len)
{
#ifdef tron
	if (len > 0) {
		register VP	str_r1 asm("r1") = str;
		register UINT	len_r2 asm("r2") = len;
		register INT	ch_r3 asm("r3") = ch;

		asm("sstr.b"
		  : /* no output */
		  : "g"(str_r1), "g"(len_r2), "g"(ch_r3)
		  : "r1", "r2");
	}
#else /* tron */
	VB	*s = str;

	while (len-- > 0) {
		*s++ = ch;
	}
#endif /* tron */
	return(str);
}

#endif /* _CPU_INLINE_ */
