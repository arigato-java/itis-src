#! /usr/local/bin/perl
#
# 
# 	    ItIs - ITRON Implementation by Sakamura Lab
# 
# Copyright (C) 1989-94 by Sakamura Lab, the University of Tokyo, JAPAN
# 
# 			All Rights Reserved
# 
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose is hereby granted without fee, provided
# that both the above copyright notice and this permission notice appear
# in all copies and supporting documentation, and that the names of
# Sakamura Lab and the University of Tokyo not be used in advertising or
# publicity pertaining to distribution of the software without specific,
# written prior permission.  Sakamura Lab makes no representations about
# the suitability of this software for any purpose.  It is provided "as
# is" without express or implied warranty.
# 
#                              NO WARRANTY
#   
# SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
# EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
# USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# 
#  @(#) $Id: genconfig,v 1.5 1994/01/19 09:31:17 hiro Exp $
#

#
#  システム構成定義生成プログラム
#
#  システム構成設定ファイル (config.def) を読み込んで，以下のファイルを
#  生成する．
#
#	config.h
#

#  大域変数の初期化

$line = "";
$word = "";
$lineno = 0;

#  字句解析ルーチン

sub getline {
	$line = <>;
	$lineno++;
}

sub getword {
	# 空行のスキップ
	$line =~ s/^[ \t]+//;
	while ($line eq "") {
		do getline();
		return($word = "") unless ($line);
		chop $line;
		$line =~ s/^[ \t]+//;
	}

	# コメントのスキップ
	if ($line =~ /^\/\*/) {
		while ($line !~ /\*\/(.*)$/) {
			do getline();
			return($word = "") unless ($line);
			chop $line;
		}
		$line =~ /\*\/(.*)$/;
		$line = $1;
		$word = do getword(); 
		return($word);
	}

	# 語の切り出し
	if ($line =~ /^([A-Za-z_][A-Za-z0-9_]*)(.*)$/) {
		$word = $1;
		$line = $2;
	}
	elsif ($line =~ /^(-?)0x([0-9A-Fa-f]+)(.*)$/) {
		$word = hex($1.$2);
		$line = $3;
	}
	elsif ($line =~ /^(-?[0-9]+)(.*)$/) {
		$word = $1;
		$line = $2;
	}
	else {
		$word = substr($line, 0, 1);
		$line = substr($line, 1);
	}
	$word =~ y/A-Z/a-z/;
	return($word);
}

#  エラー出力ルーチン

sub out_error {
	if ($word) {
		print STDERR "Syntax error at line: ",$lineno,".\n";
	}
	else {
		print STDERR "Unexpected EOF.\n";
	}
	exit(1);
}

#  次の語のチェック

sub nextword {
	local($next) = @_;

	if ($word ne $next) {
		do out_error();
	}
	else {
		do getword();
	}
}

#  シンボル定義を読み込む

sub readvalue {
	local($var) = @_;

	if ($word eq "=") {
		do getword();
		$val{$var} = $word;
		do getword();
	}
	do nextword(";");
}

#  定義テーブルを読み込む

sub readtable {
	local($id, %spec) = @_;
	local($var);

	do nextword("=");
	do nextword("{");
	while ($spec{$word}) {
		$var = $word;
		do getword();
		do readvalue($id."$".$var);
	}
	do nextword("}");
	do nextword(";");
}

#  シンボルの定義

%resource_spec = (
	"min_tskid", 1,
	"max_tskid", 1,
	"min_semid", 1,
	"max_semid", 1,
	"min_flgid", 1,
	"max_flgid", 1,
	"min_mbxid", 1,
	"max_mbxid", 1,
	"min_mbfid", 1,
	"max_mbfid", 1,
	"min_porid", 1,
	"max_porid", 1,
	"min_mplid", 1,
	"max_mplid", 1,
	"min_mpfid", 1,
	"max_mpfid", 1,
	"num_sys_mbf", 1,
	"num_sys_mpl", 1,
	"num_cyc", 1,
	"num_alm", 1,
	"num_svc", 1,
);

%priority_spec = (
	"min_pri", 1,
	"max_pri", 1,
);

%errorcheck_spec = (
	"chk_nospt", 1,
	"chk_rsatr", 1,
	"chk_par", 1,
	"chk_id", 1,
	"chk_oacv", 1,
	"chk_ctx1", 1,
	"chk_ctx2", 1,
);

%rootvar_spec = (
	"sys_stack_size", 1,
	"sys_mpl_size", 1,
	"use_qtsk_portion", 1,
	"use_auto_id", 1,
	"use_debug_support", 1,
	"use_vget_tim", 1,
);

#
#  定義ファイルの読み込み
#

do getword();
while ($word) {
	if ($word eq "errorcheck") {
		do getword();
		do readtable("errorcheck", %errorcheck_spec);
	}
	elsif ($word eq "resource") {
		do getword();
		do readtable("resource", %resource_spec);
	}
	elsif ($word eq "priority") {
		do getword();
		do readtable("priority", %priority_spec);
	}
	elsif ($rootvar_spec{$word}) {
		$var = $word;
		do getword();
		do readvalue($var);
	}
	else {
		do out_error();
	}
}

#
#  各種の定義生成ルーチン
#

sub out_object_def {
	print FILE "#define MIN_",$objstr,"ID\t(",$min,")\n";
	print FILE "#define MAX_",$objstr,"ID\t(",$max,")\n";
	print FILE "#define NUM_",$objstr,"ID\t(",$num,")\n";

	print FILE "#define CHK_",$objstr,"ID(id)\t";
	if ($min < 0) {
		print FILE "(((MIN_",$objstr,"ID) <= (id)";
		print FILE " && (id) < -",4-$sysres,")";
		print FILE " || (0 < (id) && (id) <= (MAX_",$objstr,"ID)))\n";
	}
	elsif ($sysres) {
		print FILE "((-4 <= (id) && (id) < -",4-$sysres,")";
		print FILE " || ((MIN_",$objstr,"ID) <= (id)";
		print FILE " && (id) <= (MAX_",$objstr,"ID)))\n";
	}
	else {
		print FILE "((MIN_",$objstr,"ID) <= (id)";
		print FILE " && (id) <= (MAX_",$objstr,"ID))\n";
	}

	print FILE "#define SYS_",$objstr,"ID(id)\t";
	if ($min < 0 || $sysres) {
		print FILE "((id) < 0)\n";
	}
	else {
		print FILE "(0)\n";
	}

	print FILE "#define INDEX_",$objstr,"(id)\t";
	if ($min < 0) {
		print FILE "((id) < 0 ? ((id)-(MIN_",$objstr,"ID)) : ",
				"((id)-(MIN_",$objstr,"ID)-",
				5-$sysres,"))\n";
	}
	elsif ($sysres) {
		print FILE "((id) < 0 ? ((id)-(-4)) : ",
				"((id)-(MIN_",$objstr,"ID)+",$sysres,"))\n";
	}
	else {
		print FILE "((id)-(MIN_",$objstr,"ID))\n";
	}

	print FILE "#define ID_",$objstr,"(index)\t";
	if ($min < 0) {
		print FILE "((index) <= (-(MIN_",$objstr,"ID)-",5-$sysres,
				") ? ((index)+(MIN_",$objstr,"ID)) : ",
				"((index)+(MIN_",$objstr,"ID)+",5-$sysres,
				"))\n";
	}
	elsif ($sysres) {
		print FILE "((index) <= ",$sysres-1," ? ((index)+(-4)) : ",
				"((index)+(MIN_",$objstr,"ID)-",$sysres,"))\n";
	}
	else {
		print FILE "((index)+(MIN_",$objstr,"ID))\n";
	}
	print FILE "\n";
}

sub gen_object_def {
	local($obj, $sysres) = @_;
	local($minvar, $maxsvar, $objstr);
	local($min, $max, $num);

	$minvar = "min_".$obj."id";
	$maxvar = "max_".$obj."id";
	$objstr = $obj; $objstr =~ y/a-z/A-Z/;
	$min = $val{"resource\$".$minvar};
	$max = $val{"resource\$".$maxvar};

	if ($min ne "" && $max ne "") {
		if (-4 <= $min && $min <= 0) {
			print STDERR $minvar," is illegal.\n";
			return;
		}
		if ($max < 0) {
			print STDERR $maxvar," is illegal.\n";
			return;
		}
		$num = $max - $min + ($min < 0 ? -4 : 1) + $sysres;
		if ($num < 0) {
			print STDERR $minvar," is larger than ",$maxvar."\n";
			return;
		}
		do out_object_def() if ($num > 0);
	}
	elsif ($min ne "") {
		print STDERR $maxvar," is not defined.\n"
	}
	elsif ($max ne "") {
		print STDERR $minvar," is not defined.\n"
	}
	elsif ($sysres) {
		$min = 1;
		$max = 0;
		$num = $sysres;
		do out_object_def();
	}
}

sub gen_resnum_def {
	local($res) = @_;
	local($numvar, $resstr, $num);

	$numvar = "num_".$res;
	$resstr = $res; $resstr =~ y/a-z/A-Z/;
	$num = $val{"resource\$".$numvar};
	if ($num < 0) {
		print STDERR $numvar," is illegal.\n";
		return;
	}
	if ($num > 0) {
		print FILE "#define NUM_",$resstr,"\t\t(",$num,")\n";
	}
}

sub gen_priority_def {
	local($minvar, $maxsvar);
	local($min, $max, $num);

	$minvar = "min_pri";
	$maxvar = "max_pri";
	$min = $val{"priority\$".$minvar};
	$max = $val{"priority\$".$maxvar};
	$num = $max - $min + 1;

	if ($min eq "") {
		print STDERR $mixvar," is not defined.\n"
	}
	if ($max eq "") {
		print STDERR $maxvar," is not defined.\n"
	}
	if ($num <= 0) {
		print STDERR $minvar," is larger than ",$maxvar."\n";
		return;
	}
	print FILE "#define MIN_PRI\t\t(",$min,")\n";
	print FILE "#define MAX_PRI\t\t(",$max,")\n";
	print FILE "#define NUM_PRI\t\t(",$num,")\n";
	print FILE "#define CHK_PRI(pri)\t";
	print FILE "((MIN_PRI) <= (pri) && (pri) <= (MAX_PRI))\n";

	print FILE "\n";
}

sub gen_errorcheck_def {
	local($class) = @_;
	local($flagvar, $classstr);

	$flagvar = "chk_".$class;
	if ($val{"errorcheck\$".$flagvar}) {
		$classstr = $class; $classstr =~ y/a-z/A-Z/;
		print FILE "#define CHK_",$classstr,"\n";
	}
}

sub gen_value_def {
	local($var) = @_;
	local($varstr, $num);

	$varstr = $var; $varstr =~ y/a-z/A-Z/;
	$num = $val{$var};
	if ($num < 0) {
		print STDERR $var," is illegal.\n";
		return;
	}
	if ($num > 0) {
		print FILE "#define ",$varstr,"\t(",$num,")\n";
	}
}

sub gen_flag_def {
	local($flagvar) = @_;
	local($flagstr);

	if ($val{$flagvar}) {
		$flagstr = $flagvar; $flagstr =~ y/a-z/A-Z/;
		print FILE "#define ",$flagstr,"\n";
	}
}

#
#  config.h の生成
#

open(FILE, "> config.h");
print FILE <<END_OF_HEADER;
/*  This file is generated from config.def by genconfig.  */

#ifndef _CONFIG_
#define _CONFIG_

END_OF_HEADER

# オブジェクトID の範囲定義の生成
do gen_object_def("tsk", 0);
do gen_object_def("sem", 0);
do gen_object_def("flg", 0);
do gen_object_def("mbx", 0);
do gen_object_def("mbf", $val{"resource\$num_sys_mbf"});
do gen_object_def("por", 0);
do gen_object_def("mpl", $val{"resource\$num_sys_mpl"});
do gen_object_def("mpf", 0);

do gen_resnum_def("cyc");
do gen_resnum_def("alm");
do gen_resnum_def("svc");
print FILE "\n";

# 拡張SVC を使う時は，準タスク部が必要
if ($val{"resource\$num_svc"} > 0 && !$val{"use_qtsk_portion"}) {
	$val{"use_qtsk_portion"} = 1;
}

# 優先度値の範囲定義の生成
do gen_priority_def();

# エラーチェック仕様の定義の生成
do gen_errorcheck_def("nospt");
do gen_errorcheck_def("rsatr");
do gen_errorcheck_def("par");
do gen_errorcheck_def("id");
do gen_errorcheck_def("oacv");
do gen_errorcheck_def("ctx1");
do gen_errorcheck_def("ctx2");
print FILE "\n";

# その他の定義の生成
do gen_value_def("sys_stack_size");
do gen_value_def("sys_mpl_size");
do gen_flag_def("use_qtsk_portion");
do gen_flag_def("use_auto_id");
do gen_flag_def("use_debug_support");
do gen_flag_def("use_vget_tim");

print FILE <<END_OF_TAILER;

#endif /* _CONFIG_ */
END_OF_TAILER
close(FILE);
