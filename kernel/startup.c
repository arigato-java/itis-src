/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: startup.c,v 1.1 1993/10/15 09:50:20 hiro Exp $
 */

#include "itis_kernel.h"
#include "cpu_inline.h"
#include "task.h"
#include "timer.h"
#include "patchlevel.h"

/*
 *  EITハンドラの定義
 */
extern void	trapa_ientry(void);
#ifndef TRON_NO_DI
extern void	dispatch_entry(void);
#else /* TRON_NO_DI */
extern void	dispatch_entry(void);
extern void	z_ret_int(void);
#endif /* TRON_NO_DI */

/*
 *  バージョン情報 (version.c)
 */
extern char	version[];

/*
 *  ItIsカーネルの初期化と初期化タスクの生成・起動
 */
void
main()
{
	ER	ercd;

	DISABLE_INTERRUPT;
	i_syslog(LOG_KERN|LOG_ALERT,
		version, MAJOR_REL, MINOR_REL, PATCH_LEVEL);

	/*
	 *  各モジュールの初期化
	 */
	target_initialize();
	task_initialize();
	semaphore_initialize();
	eventflag_initialize();
	mailbox_initialize();
	messagebuffer_initialize();
#ifdef NUM_PORID
	rendezvous_initialize();
#endif /* NUM_POIRD */
	memorypool_initialize();
	fix_memorypool_initialize();
#ifdef NUM_CYC
	cyclichandler_initialize();
#endif /* NUM_CYC */
#ifdef NUM_ALM
	alarmhandler_initialize();
#endif /* NUM_ALM */
#ifdef NUM_SVC
	extended_svc_initialize();
#endif /* NUM_SVC */
	timer_initialize();

	/*
	 *  システムメモリプールの生成
	 */
	if ((ercd = sys_cre_mpl()) < 0) {
		i_syslog(LOG_KERN|LOG_EMERG,
			"Cannot alloc system memory pool: %d\n", ercd);
		itis_exit();
	}

	/*
	 *  EITハンドラの定義
	 */
	define_eit(EITVEC_TRAPA1, EITATR(1, 15), trapa_ientry);
#ifndef TRON_NO_DI
	define_eit(EITVEC_DI14, EITATR(1, 14), dispatch_entry);
#else /* TRON_NO_DI */
	define_eit(EITVEC_TRAPA3, EITATR(0, 15), z_ret_int);
	define_eit(EITVEC_TRAPA14, EITATR(1, 15), dispatch_entry);
#endif /* TRON_NO_DI */

	/*
	 *  初期化タスクの生成・起動
	 */
	if ((ercd = i_cre_tsk(TSK_INIT, &((T_CTSK) TSK_INIT_CTSK))) < 0) {
		i_syslog(LOG_KERN|LOG_EMERG,
			"Failed to create initial task: (%d).\n", ercd);
		itis_exit();
	}
	if ((ercd = i_sta_tsk(TSK_INIT, 0)) < 0) {
		i_syslog(LOG_KERN|LOG_EMERG,
			"Failed to startup initial task: (%d).\n", ercd);
		itis_exit();
	}

	/*
	 *  カーネルの動作を開始する．
	 */
	force_dispatch();
}

/*
 *  ItIs 終了処理
 */
void
itis_exit()
{
	timer_shutdown();
	target_exit();
}
