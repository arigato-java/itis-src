/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: isyslog.c,v 1.1 1993/10/15 09:49:29 hiro Exp $
 */

#include <stdarg.h>
#include "itis_kernel.h"

/*
 *  ログタスクの変数
 *
 *  タスクの変数をカーネルが参照するという例外的な処理になっている．
 */
extern int	logtask_alive;		/* ログタスクが動いているか */
extern int	log_msg_maxmsz;		/* ログメッセージの最大長 */

/*
 *  システムログ用メッセージバッファへの送信 (messagebuf.c)
 */
extern ER	log_snd_mbf(VP msg, INT msgsz);

/*
 *  カーネル用簡易 vsprintf関数 (vsprintf.c)
 */
extern int	itis_vsprintf(char *buf, const char *format, va_list ap);

/*
 *  カーネル用システムログ出力用ライブラリ
 */

static int	i_logmask = LOG_UPTO(LOG_NOTICE);

#define	FORMAT_BUFSIZ	1024

static char	i_syslog_buf[FORMAT_BUFSIZ];

/*
 *  ログメッセージの出力
 *
 *  ログタスクが動いている場合は，ログメッセージバッファへ送る．動いて
 *  いない場合は，直接低レベルの出力ルーチンを使って出力する．
 */
static void
i_syslog_send(const char *string, int len)
{
	if (len > 0) {
		if (logtask_alive) {
			if (len > log_msg_maxmsz) {
				len = log_msg_maxmsz;
			}
			if (log_snd_mbf((VP) string, len) != E_OK) {
				logtask_alive = 0;
			}
		}
		if (!logtask_alive) {
			target_write(string, len);
			target_write("\n", 1);
		}
	}
}

/*
 *  カーネル用 syslog 関数本体
 */
void
i_syslog(int priority, const char *format, ...)
{
	va_list	ap;
	int	len;

	if (i_logmask & LOG_MASK(priority & LOG_PRIMASK)) {
		va_start(ap, format);
		BEGIN_CRITICAL_SECTION;
		if ((priority & LOG_PRIMASK) == LOG_EMERG) {
			logtask_alive = 0;
		}
		len = itis_vsprintf(i_syslog_buf, format, ap);
		i_syslog_send(i_syslog_buf, len);
		END_CRITICAL_SECTION;
		va_end(ap);
	}
}

/*
 *  カーネル用の assertマクロのメッセージ出力
 */
void
i_assert_failed(const char *file, int line, const char *expr)
{
	i_syslog(LOG_KERN|LOG_EMERG,
		"Failed assertion `%s' in line %d of `%s'.",
		expr, line, file);
	itis_exit();
}
