/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: ientry.S,v 1.3 1993/11/22 14:35:57 hiro Exp $
 */

#include "config.h"
#include "isysconf.h"

#include <itron_errno.h>

/*
 *  システムコールエントリテーブル
 */

#include "itable.h"
_itable:
	.int _no_support		/* 機能番号 0 に対応するエントリ */

/*
 *  システムコール起動ルーチン
 *
 *  TRAPA を使ってシステムコールを起動するための分岐ルーチン．機能コー
 *  ドに従って，各システムコールの処理ルーチンへ分岐する．
 */
	.text
	.align 1
	.globl _trapa_ientry
_trapa_ientry:
	cmp #0, r6
	bgt _trapa_ientry_1
	cmp #FIRST_FNCD, r6		/* 機能コードの上限のチェック */
	blt _trapa_ientry_2
	jsr @@(_itable,r6*4)		/* システムコール処理ルーチンへ分岐 */
	reit

_trapa_ientry_1:
#ifdef NUM_SVC
	jsr @_svc_ientry		/* 拡張SVCハンドラへの分岐 */
	reit
#endif /* NUM_SVC */

_trapa_ientry_2:
	mov #E_RSFN, r0
	reit

/*
 *  未サポートシステムコールのエントリ
 */

	.globl _no_support
_no_support:
	mov #E_RSFN, r0
	rts

/*
 *  システムコール (アセンブラで記述する必要があるもの)
 */

/*
 *  SYSCALL ER dis_dsp()
 */

#ifndef _z_dis_dsp

_z_dis_dsp:
#ifdef CHK_CTX
	bfextu #12,#4,@(4,sp), r0	/* IMASK を EIT情報から取り出す．*/
	cmpu #14, r0			/* IMASK < 14 なら E_CTXエラー．*/
	blt _context_error
#endif /* CHK_CTX */
	bfinsu #14, #12,#4,@(4,sp)	/* IMASK を EIT情報中に設定する．*/
	mov #E_OK, r0
	rts

#endif /* _z_dis_dsp */

/*
 *  SYSCALL ER ena_dsp()
 */

#ifndef _z_ena_dsp

_z_ena_dsp:
#ifdef CHK_CTX
	bfextu #12,#4,@(4,sp), r0	/* IMASK を EIT情報から取り出す．*/
	cmpu #14, r0			/* IMASK < 14 なら E_CTXエラー．*/
	blt _context_error
#endif /* CHK_CTX */
	bfinsu #15, #12,#4,@(4,sp)	/* IMASK を EIT情報中に設定する．*/
	mov #E_OK, r0
	rts

#endif /* _z_ena_dsp */

/*
 *  SYSCALL ER loc_cpu()
 */

#ifndef _z_loc_cpu

_z_loc_cpu:
#ifdef CHK_CTX
	btst #0,@(4,sp)			/* タスク独立部なら E_CTXエラー．*/
	beq _context_error
#endif /* CHK_CTX */
	bfinsu #0, #12,#4,@(4,sp)	/* IMASK を EIT情報中に設定する．*/
	mov #E_OK, r0
	rts

#endif /* _z_loc_cpu */

/*
 *  SYSCALL ER unl_cpu()
 */

#ifndef _z_unl_cpu

_z_unl_cpu:
#ifdef CHK_CTX
	btst #0,@(4,sp)			/* タスク独立部なら E_CTXエラー．*/
	beq _context_error
#endif /* CHK_CTX */
	bfinsu #15, #12,#4,@(4,sp)	/* IMASK を EIT情報中に設定する．*/
	mov #E_OK, r0
	rts

#endif /* _z_unl_cpu */

/*
 *  SYSCALL ER chg_ims(UINT imask)
 */

#ifndef _z_chg_ims

_z_chg_ims:
#ifdef CHK_PAR
	btst #0,@(4,sp)			/* タスク独立部なら z_chg_ims_1 へ．*/
	beq z_chg_ims_1
	cmpu #15, r0			/* IMASK > 15 なら E_PARエラー．*/
	bra z_chg_ims_2
z_chg_ims_1:
	cmpu #7, r0			/* IMASK > 7 なら E_PARエラー．*/
z_chg_ims_2:
	bgt _parameter_error
#endif /* CHK_PAR */
	bfinsu r0, #12,#4,@(4,sp)	/* IMASK を EIT情報中に設定する．*/
	mov #E_OK, r0
	rts

#endif /* _z_chg_ims */

/*
 *  SYSCALL ER ref_ims(UINT *p_imask)
 */

#ifndef _z_ref_ims

_z_ref_ims:
	bfextu #12,#4,@(4,sp), r1	/* IMASK を EIT情報から取り出す．*/
	mov r1, @r0
	mov #E_OK, r0
	rts

#endif /* _z_ref_ims */

/*
 *  エラー処理
 */

#ifdef CHK_CTX
_context_error:
	mov #E_CTX, r0
	rts
#endif /* CHK_CTX */

#ifdef CHK_PAR
_parameter_error:
	mov #E_PAR, r0
	rts
#endif /* CHK_PAR */
