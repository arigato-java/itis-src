/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: winfo.h,v 1.1 1993/10/15 09:50:57 hiro Exp $
 */

#ifndef _WINFO_
#define _WINFO_

/*
 *  同期・通信オブジェクト毎に必要な待ち情報の定義
 */

#include "queue.h"
#include "timer.h"

/*
 *  イベントフラグ待ち (TTW_FLG)
 */
typedef struct {
	UINT	waiptn;		/* 待ちビットパターン */
	UINT	wfmode;		/* 待ちモード */
	UINT	*p_flgptn;	/* 待ち解除時ビットパターンを入れるアドレス */
} TTW_FLG_WINFO;

/*
 *  メイルボックス待ち (TTW_MBX)
 */
typedef struct {
	T_MSG	**ppk_msg;	/* メッセージパケットの先頭を入れるアドレス */
} TTW_MBX_WINFO;

/*
 *  メッセージバッファ受信/送信待ち (TTW_MBF, TTW_SMBF)
 */
typedef struct {
	VP	msg;		/* 受信メッセージを入れるアドレス */
	INT	*p_msgsz;	/* 受信メッセージのサイズを入れるアドレス */
} TTW_MBF_WINFO;

typedef struct {
	VP	msg;		/* 送信メッセージの先頭アドレス */
	INT	msgsz;		/* 送信メッセージのサイズ */
} TTW_SMBF_WINFO;

/*
 *  ランデブ呼出/受付/終了待ち (TTW_CAL, TTW_ACP, TTW_RDV)
 */
typedef struct {
	UINT	calptn;		/* 呼出側選択条件を表すビットパターン */
	VP	msg;		/* メッセージを入れるアドレス */
	INT	cmsgsz;		/* 呼出メッセージのサイズ */
	INT	*p_rmsgsz;	/* 返答メッセージのサイズを入れるアドレス */
} TTW_CAL_WINFO;

typedef struct {
	UINT	acpptn;		/* 受付側選択条件を表すビットパターン */
	VP	msg;		/* 呼出メッセージを入れるアドレス */
	RNO	*p_rdvno;	/* ランデブ番号を入れるアドレス */
	INT	*p_cmsgsz;	/* 呼出メッセージのサイズを入れるアドレス */
} TTW_ACP_WINFO;

typedef struct {
	RNO	rdvno;		/* ランデブ番号 */
	VP	msg;		/* メッセージを入れるアドレス */
	INT	maxrmsz;	/* 返答メッセージの最大長 */
	INT	*p_rmsgsz;	/* 返答メッセージのサイズを入れるアドレス */
} TTW_RDV_WINFO;

/*
 *  可変長メモリプール待ち (TTW_MPL)
 */
typedef struct {
	INT	blksz;		/* メモリブロックサイズ */
	VP	*p_blk;		/* メモリブロックの先頭を入れるアドレス */
} TTW_MPL_WINFO;

/*
 *  固定長メモリプール待ち (TTW_MPF)
 */
typedef struct {
	VP	*p_blf;		/* メモリブロックの先頭を入れるアドレス */
} TTW_MPF_WINFO;

/*
 *  タスクコントロールブロック中に持つ待ち情報の定義
 */
typedef union {
	TTW_FLG_WINFO	flg;
	TTW_MBX_WINFO	mbx;
	TTW_MBF_WINFO	mbf;
	TTW_SMBF_WINFO	smbf;
#ifdef NUM_PORID
	TTW_CAL_WINFO	cal;
	TTW_ACP_WINFO	acp;
	TTW_RDV_WINFO	rdv;
#endif /* NUM_PORID */
	TTW_MPL_WINFO	mpl;
	TTW_MPF_WINFO	mpf;
} WINFO;

#endif /* _WINFO_ */
