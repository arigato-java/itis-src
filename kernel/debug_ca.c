/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: debug_calls.c,v 1.3 1993/12/10 05:12:30 hiro Exp $
 */

/* 
 *  デバッグサポート機能
 */

#include "itis_kernel.h"
#include "cpu_inline.h"
#include "task.h"
#include "check.h"

#ifdef USE_DEBUG_SUPPORT

/*
 *  タスクのレジスタ内容の設定，参照
 */

#ifndef _i_vset_reg

SYSCALL ER
i_vset_reg(ID tskid, T_REGS* pk_regs, T_EIT* pk_eit, T_CREGS* pk_cregs)
{
	TCB	*tcb;
	VW	*ssp;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	if (pk_eit != NADR) {
		CHECK_PAR(pk_eit->psw & 0x80000000);
	}
	CHECK_INTSK();
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (tcb->state == TS_NONEXIST) {
		ercd = E_NOEXS;
		goto error_exit;
	}

	if (pk_cregs != NADR) {
		memcpy(&(tcb->tskctxb), pk_cregs, sizeof(T_CREGS));
	}
	ssp = tcb->tskctxb.sp0;

	if (pk_eit != NADR) {
		*(ssp + 17) = pk_eit->pc;
		*(ssp + 15) = pk_eit->psw;
	}

	if (pk_regs != NADR) {
		memcpy(ssp, pk_regs, sizeof(VW) * 15);
		if (pk_cregs == NADR) {
			assert(*(ssp + 15) & 0x80000000);
			switch (*(ssp + 15) & 0x60000000) {
			case 0:
				tcb->tskctxb.sp0 = (VP)(pk_regs->sp);
				break;
			case 0x20000000:
				tcb->tskctxb.sp1 = (VP)(pk_regs->sp);
				break;
			case 0x40000000:
				tcb->tskctxb.sp2 = (VP)(pk_regs->sp);
				break;
			case 0x60000000:
				tcb->tskctxb.sp3 = (VP)(pk_regs->sp);
				break;
			}
		}
	}

    error_exit:
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vset_reg */
#ifndef _i_vget_reg

SYSCALL ER
i_vget_reg(T_REGS* pk_regs, T_EIT* pk_eit, T_CREGS* pk_cregs, ID tskid)
{
	TCB	*tcb;
	VW	*ssp, psw;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	CHECK_INTSK();
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (tcb->state == TS_NONEXIST) {
		ercd = E_NOEXS;
		goto error_exit;
	}

	ssp = tcb->tskctxb.sp0;
	psw = *(ssp + 15);
	if (pk_regs != NADR) {
		memcpy(pk_regs, ssp, sizeof(VW) * 15);
		assert(psw & 0x80000000);
		switch (psw & 0x60000000) {
		case 0:
			pk_regs->sp = (VW) ssp;
			break;
		case 0x20000000:
			pk_regs->sp = (VW)(tcb->tskctxb.sp1);
			break;
		case 0x40000000:
			pk_regs->sp = (VW)(tcb->tskctxb.sp2);
			break;
		case 0x60000000:
			pk_regs->sp = (VW)(tcb->tskctxb.sp3);
			break;
		}
	}

	if (pk_eit != NADR) {
		pk_eit->pc = *(ssp + 17);
		pk_eit->psw = psw;
	}

	if (pk_cregs != NADR) {
		memcpy(pk_cregs, &(tcb->tskctxb), sizeof(T_CREGS));
	}

    error_exit:
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vget_reg */
#endif /* USE_DEBUG_SUPPORT */
