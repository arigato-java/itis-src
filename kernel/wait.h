/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: wait.h,v 1.1 1993/10/15 09:50:55 hiro Exp $
 */

#ifndef _WAIT_
#define _WAIT_

/*
 *  タスク間同期・通信オブジェクト汎用ルーチン
 */

#include "queue.h"
#include "timer.h"
#include "task.h"

/*
 *  タスクを待ち行列からはずし，タスク状態を更新する．レディ状態になる
 *  場合は，レディキューにつなぐ．
 *
 *  タスクが待ち状態 (二重待ちを含む) の時に呼ぶこと．
 */
extern void	wait_queue_delete_task(TCB *tcb);

/*
 *  実行中のタスクを待ち状態に移行させ，タイマイベントキューにつなぐ．
 */
extern void	make_wait(TMO tmout);

/*
 *  待ちキューの先頭のタスクの ID を取り出す．
 */
extern ID	wait_tskid(QUEUE *wait_queue);

/*
 *  タスクの待ち状態を解除する．
 *
 *  タスクをタイマキューおよび待ちキューからはずし，タスク状態を更新す
 *  る．wait_release_ok は，待ち解除されたタスクに E_OK を渡す．
 */
extern void	wait_release(TCB *tcb);
extern void	wait_release_ok(TCB *tcb);

/*
 *  待ちキューにつながっているタスクの待ち状態をすべて解除し，E_DLTエラー
 *  とする．
 *
 *  タスク間同期・通信オブジェクトが削除された時に使う．
 */
extern void	wait_delete(QUEUE *wait_queue);

/*
 *  コントロールブロック共通部分操作ルーチン
 *
 *  タスク間同期・通信オブジェクトはいずれも，コントロールブロックの先
 *  頭部分が共通になっている．以下は，その共通部分を扱うための汎用ルー
 *  チンである．共通部分を，GCB (汎用コントロールブロック) という型にす
 *  る．複数の待ちキューを持つオブジェクトの場合，2つめ以降の待ちキュー
 *  を操作する場合には，これらのルーチンは使えない．また，オブジェクト
 *  属性の TA_TPRIビットを参照するので，このビットを他の目的に使ってい
 *  る場合も，これらのルーチンは使えない．
 */

typedef struct generic_control_block {
	QUEUE	wait_queue;	/* 待ちキュー */
	ID	objid;		/* オブジェクトID */
	VP	exinf;		/* 拡張情報 */
	ATR	objatr;		/* オブジェクト属性 */
	/*  これ以降に他のフィールドがあってもよいが， */
	/*  汎用操作ルーチンでは扱われない． */
} GCB ;

/*
 *  実行中のタスクを待ち状態に移行させ，タイマイベントキューおよびオブ
 *  ジェクトの待ちキューにつなぐ．また，ctxtsk の wid を設定する．
 */
extern void	gcb_make_wait(GCB *gcb, TMO tmout);

/*
 *  タスクの優先度が変わった際に，待ちキューの中でのタスクの位置を修正
 *  する．オブジェクト属性に TA_TPRI が指定されていない場合は，何もしな
 *  い．
 */
extern void	gcb_change_priority(GCB *gcb, TCB *tcb);

#endif /* _WAIT_ */
