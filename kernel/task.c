/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: task.c,v 1.10 1994/01/17 06:58:28 hiro Exp $
 */

#include "itis_kernel.h"
#include "cpu_inline.h"
#include "task.h"
#include "wait.h"
#include "ready_queue.h"

/*
 *  各種の定数の定義
 *
 *  TRON仕様チップに依存している．
 */
#ifndef TRON_NO_DI
#define DI14_EITINF	0x0003005e	/* DI=14 に対する EITINF */
#else /* TRON_NO_DI */
#define TRAPA14_EITINF	0x0201002e	/* TRAPA #14 に対する EITINF */
#endif /* TRON_NO_DI */

#define INI_PSW_RNG0	0x800f6000	/* PSW の初期値 (リングレベル0) */
#define INI_PSW_RNG1	0xa00f6000	/* PSW の初期値 (リングレベル1) */
#define INI_PSW_RNG2	0xc00f6000	/* PSW の初期値 (リングレベル2) */
#define INI_PSW_RNG3	0xe00f6000	/* PSW の初期値 (リングレベル3) */

#define INI_CSW		0x00070000	/* CSW の初期値 */

/* 
 *  実行中のタスク
 */
TCB	*ctxtsk;

/*
 *  実行すべきタスク
 */
TCB	*schedtsk;

/* 
 *  TCB のエリア
 */
TCB	tcb_table[NUM_TSKID];

/*
 *  タスクが実行すべき状態にあるかを判断するマクロ
 *
 *  シングルプロセッサの場合は schedtsk が 1つのみなので，schedtsk との
 *  比較で判断する．マルチプロセッサ対応にする時は，TCB にフラグを入れる．
 */
#define SCHEDULED(tcb)	(tcb == schedtsk)

/*
 *  レディキュー
 */ 
RDYQUE	ready_queue;

/*
 *  未使用の TCB のリスト
 */
#ifndef _i_vcre_tsk
QUEUE	free_tcb;
#endif /* _i_vcre_tsk */

/* 
 *  TCB の初期化
 */
void
task_initialize(void)
{
	INT	i;
	TCB	*tcb;
	ID	tskid;

	ready_queue_initialize(&ready_queue);
#ifndef _i_vcre_tsk
	queue_initialize(&free_tcb);
#endif /* _i_vcre_tsk */

	for(tcb = tcb_table, i = 0; i < NUM_TSKID; tcb++, i++) {
		tskid = ID_TSK(i);
		tcb->tskid = tskid;
		tcb->state = TS_NONEXIST;
#ifdef NUM_PORID
		tcb->wrdvno = (W)(UH) tskid;
#endif /* NUM_PORID */
#ifndef _i_vcre_tsk
		if (!SYS_TSKID(tskid)) {
			queue_insert(&(tcb->tskque), &free_tcb);
		}
#endif /* _i_vcre_tsk */
	}
#ifdef TRON_NO_DI
	dispatch_disabled = 0;
#endif /* TRON_NO_DI */
}

/*
 *  タスクディスパッチャの起動要求を出す．
 */
Inline void
dispatch_request(void)
{
#ifndef TRON_NO_DI
	set_dir(14);
#endif /* TRON_NO_DI */
}

/*
 *  タスクの実行準備をする．
 */
void
make_dormant(TCB *tcb)
{
	VW	*ssp;

	/*
	 *  DORMANT状態ではリセットされているべき変数を初期化．
	 */
	tcb->state = TS_DORMANT;
	tcb->priority = tcb->ipriority;
	tcb->wupcnt = 0;
	tcb->suscnt = 0;
#ifdef USE_QTSK_PORTION
	tcb->sysmode = tcb->isysmode;
#endif /* USE_QTSK_PORTION */

	/*
	 *  システムスタック上に，EITスタックフレームを作る．
	 */
	ssp = (VW *)(tcb->isstack);	/* 初期システムスタックポインタ */
#ifndef TRON_NO_DI
	*--ssp = (VW)(tcb->task);	/* タスク起動アドレスを積む */
	*--ssp = DI14_EITINF;		/* EIT情報を積む */
#else /* TRON_NO_DI */
	*--ssp = 0;			/* EXPC (ダミー) を積む */
	*--ssp = (VW)(tcb->task);	/* タスク起動アドレスを積む */
	*--ssp = TRAPA14_EITINF;	/* EIT情報を積む */
#endif /* TRON_NO_DI */

	/*
	 *  PSW の初期値をシステムスタックに積み，ユーザスタックポイン
	 *  タを初期化する．
	 */
	switch (tcb->tskatr & TA_RNG3) {
	case TA_RNG0:
		*--ssp = INI_PSW_RNG0;
		break;
	case TA_RNG1:
		tcb->tskctxb.sp1 = tcb->istack;
		*--ssp = INI_PSW_RNG1;
		break;
	case TA_RNG2:
		tcb->tskctxb.sp2 = tcb->istack;
		*--ssp = INI_PSW_RNG2;
		break;
	case TA_RNG3:
		tcb->tskctxb.sp3 = tcb->istack;
		*--ssp = INI_PSW_RNG3;
		break;
	}
	ssp -= 15;			/* R14～R0 の分のエリアを取る */

	tcb->tskctxb.sp0 = ssp;
	tcb->tskctxb.csw = INI_CSW;
}

/*
 *  タスクを実行可能状態にする．
 */
void
make_ready(TCB *tcb)
{
	tcb->state = TS_READY;
	if (schedtsk == 0) {
		/*
		 *  実行状態のタスクがない場合．
		 */
		schedtsk = tcb;
		dispatch_request();
	}
	else if (tcb->priority < schedtsk->priority) {
		/*
		 *  tcb が指すタスクが，実行状態のタスクよりも高い優先
		 *  度を持つ場合．
		 */
		ready_queue_insert_top(&ready_queue, schedtsk);
		schedtsk = tcb;
		dispatch_request();
	}
	else {
		/*
		 *  tcb が指すタスクの優先度が，実行状態のタスクの優先
		 *  度以下の場合．
		 */
		ready_queue_insert(&ready_queue, tcb);
	}
}

/*
 *  タスクを実行可能以外の状態にする．
 */
void
make_non_ready(TCB *tcb)
{
	assert(tcb->state == TS_READY);

	if (SCHEDULED(tcb)) {
		if (schedtsk = ready_queue_top(&ready_queue)) {
			ready_queue_delete(&ready_queue, schedtsk);
		}
		dispatch_request();
	}
	else {
		ready_queue_delete(&ready_queue, tcb);
	}
}

/*
 *  実行状態のタスクを変更する．
 */
void
change_running_task(void)
{
	TCB	*tcb;

	if (schedtsk && ready_queue_top_priority(&ready_queue)
						<= schedtsk->priority) {
		tcb = ready_queue_top(&ready_queue);
		ready_queue_insert(&ready_queue, schedtsk);

		schedtsk = tcb;
		ready_queue_delete(&ready_queue, tcb);
		dispatch_request();
	}
}

/*
 *  タスクの優先度を変更する．
 */
void
change_task_priority(TCB *tcb, INT priority)
{
	if (tcb->state == TS_READY) {
		if (SCHEDULED(tcb)) {
			tcb->priority = priority;
			change_running_task();
		}
		else {
			/*
			 *  タスクをレディキューから削除する際に TCB の
			 *  priority フィールドの値が必要になるため，レ
			 *  ディキューからの削除は，tcb->priority を書
			 *  き換える行わなければならない．
			 */
			ready_queue_delete(&ready_queue, tcb);
			tcb->priority = priority;
			make_ready(tcb);
		}
	}
	else {
		tcb->priority = priority;
		if (tcb->state & TS_WAIT) {
			switch (tcb->tskwait) {
			case TTW_SEM:
				sem_chg_pri(tcb);
				break;
			case TTW_MBX:
				mbx_chg_pri(tcb);
				break;
			case TTW_MBF:
				mbf_chg_pri(tcb);
				break;
			case TTW_MPL:
				mpl_chg_pri(tcb);
				break;
			case TTW_MPF:
				mpf_chg_pri(tcb);
				break;
			}
		}
	}
}

/*
 *  レディキューを回転させる．
 */
void
rotate_ready_queue(INT priority)
{
	if (schedtsk && priority == schedtsk->priority) {
		change_running_task();
	}
	else {
		ready_queue_rotate(&ready_queue, priority);
	}
}

/*
 *  遅延割込みを使わない場合の定義．
 */

#ifdef TRON_NO_DI

BOOL	dispatch_disabled;

void
dispatch()
{
	if (!dispatch_disabled && ctxtsk != schedtsk) {
		Asm("trapa #14");
	}
}

#endif /* TRON_NO_DI */
