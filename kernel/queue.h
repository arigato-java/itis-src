/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: queue.h,v 1.1 1993/10/15 09:50:05 hiro Exp $
 */

#ifndef	_QUEUE_
#define	_QUEUE_

#include <itis_stddefs.h>

/*
 *  ダブルリンクキュー
 * 
 *  ダブルリンクキューの構造は，TRON仕様チップのキュー操作命令が扱う形
 *  式にあわせてある．
 */
typedef struct queue {
	struct queue *next;
	struct queue *prev;
} QUEUE;

/*
 *  キュー操作ライブラリ
 */

/*
 *  キューの初期化
 */
Inline void
queue_initialize(QUEUE *queue)
{
	queue->prev = queue->next = queue;
}

/*
 *  キューへエントリを挿入
 *
 *  queue の直前に entry を挿入する．queue がキューヘッダを指す場合には，
 *  キューの最後尾に entry を挿入することになる．
 */
Inline void
queue_insert(QUEUE *entry, QUEUE *queue)
{
#ifdef tron
	Asm("qins %a0, %a1" : : "g"(entry), "g"(queue));
#else /* tron */
	entry->prev = queue->prev;
	entry->next = queue;
	queue->prev->next = entry;
	queue->prev = entry;
#endif /* tron */
}

/*
 *  キューからエントリを削除
 *
 *  entry をキューから削除する．
 */
Inline void
queue_delete(QUEUE *entry)
{
#ifdef tron
	Asm("qdel %a0, r0" : : "g"(entry->prev) : "r0");
#else /* tron */
	if (entry->next != entry) {
		entry->prev->next = entry->next;
		entry->next->prev = entry->prev;
	}
#endif /* tron */
}

/*
 *  キューの先頭のエントリの取り出し
 *
 *  queue の直後のエントリをキューから削除し，削除したエントリを返す．
 *  queue がキューヘッダを指す場合には，キューの先頭のエントリを取り出
 *  すことになる．
 */
Inline QUEUE *
queue_delete_next(QUEUE *queue)
{
	QUEUE	*entry;

	assert(queue->next != queue);
#ifdef tron
	Asm("qdel %a1, %0" : "r="(entry) : "g"(queue));
#else /* tron */
	entry = queue->next;
	queue->next = entry->next;
	entry->next->prev = queue;
#endif /* tron */
	return(entry);
}

/*
 *  キュー中のエントリのサーチ
 *
 *  queue で示すキューから，offset で示すフィールドが val 以上のエント
 *  リをサーチし，そのエントリを返す．該当するエントリがない場合は，
 *  queue を返す．
 */
Inline QUEUE *
queue_search_ge(QUEUE *queue, INT val, INT offset)
{
#if defined(tron) && !defined(TRON_LEVEL1)
	register QUEUE	*r0 asm("r0") = queue;
	register QUEUE	*r2 asm("r2") = queue;
	register INT	r3 asm("r3") = val;
	register INT	r5 asm("r5") = offset;

	Asm("qsch/ge/f"
	  : "g="(r0)
	  : "0"(r0), "g"(r2), "g"(r3), "g"(r5)
	  : "r0", "r1");
	return(r0);
#else /* defined(tron) && !defined(TRON_LEVEL1) */
	QUEUE	*entry;

	for (entry = queue->next; entry != queue; entry = entry->next) {
		if (*((INT *)(((VB *) entry) + offset)) >= val) {
			break;
		}
	}
	return(entry);
#endif /* defined(tron) && !defined(TRON_LEVEL1) */
}

/*
 *  キュー中のエントリのサーチ
 *
 *  queue で示すキューから，offset で示すフィールドが val より大きいの
 *  エントリをサーチし，そのエントリを返す．該当するエントリがない場合
 *  は，queue を返す．
 */
Inline QUEUE *
queue_search_gt(QUEUE *queue, INT val, INT offset)
{
#if defined(tron) && !defined(TRON_LEVEL1)
	register QUEUE	*r0 asm("r0") = queue;
	register QUEUE	*r2 asm("r2") = queue;
	register INT	r3 asm("r3") = val + 1;
	register INT	r5 asm("r5") = offset;

	Asm("qsch/ge/f"
	  : "g="(r0)
	  : "0"(r0), "g"(r2), "g"(r3), "g"(r5)
	  : "r0", "r1");
	return(r0);
#else /* defined(tron) && !defined(TRON_LEVEL1) */
	QUEUE	*entry;

	for (entry = queue->next; entry != queue; entry = entry->next) {
		if (*((INT *)(((VB *) entry) + offset)) > val) {
			break;
		}
	}
	return(entry);
#endif /* defined(tron) && !defined(TRON_LEVEL1) */
}

/*
 *  キューが空かどうかのチェック
 */
Inline BOOL
queue_empty_p(QUEUE *queue)
{
	if (queue->next == queue) {
		assert(queue->prev == queue);
		return(1);
	}
	return(0);
}

#endif /* _QUEUE_ */
