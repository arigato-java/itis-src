/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: eventflag.c,v 1.3 1993/11/19 09:17:45 hiro Exp $
 */

#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "check.h"

/*
 *  イベントフラグ管理ブロックの定義
 */
typedef struct eventflag_control_block {
	QUEUE	wait_queue;	/* イベントフラグ待ちキュー */
	ID	flgid;		/* イベントフラグID */
	VP	exinf;		/* 拡張情報 */
	ATR	flgatr;		/* イベントフラグ属性 */
	UINT	flgptn;		/* イベントフラグ現在パターン */
} FLGCB;

static FLGCB	flgcb_table[NUM_FLGID];

#define get_flgcb(id)	(&(flgcb_table[INDEX_FLG(id)]))
    
/*
 *  未使用のイベントフラグ管管理ブロックのリスト
 */
#ifndef _i_vcre_flg
QUEUE	free_flgcb;
#endif /* _i_vcre_flg */

/* 
 *  イベントフラグ管理ブロックの初期化
 */
void
eventflag_initialize(void)
{
	INT	i;
	FLGCB	*flgcb;

#ifndef _i_vcre_flg
	queue_initialize(&free_flgcb);
#endif /* _i_vcre_flg */

	for(flgcb = flgcb_table, i = 0; i < NUM_FLGID; flgcb++, i++) {
		flgcb->flgid = 0;
#ifndef _i_vcre_flg
		if (!SYS_FLGID(ID_FLG(i))) {
			queue_insert(&(flgcb->wait_queue), &free_flgcb);
		}
#endif /* _i_vcre_flg */
	}
}

/*
 *  イベントフラグ待ち解除条件のチェック
 */
Inline BOOL
eventflag_cond(FLGCB *flgcb, UINT waiptn, UINT wfmode)
{
	if (wfmode & TWF_ORW) {
		return(flgcb->flgptn & waiptn);
	}
	else {
		return((flgcb->flgptn & waiptn) == waiptn);
	}
}

/*
 *  イベントフラグ管理機能
 */

#if !defined(_i_cre_flg) || !defined(_i_vcre_flg)

static void
_cre_flg(FLGCB *flgcb, T_CFLG *pk_cflg)
{
	ID	flgid = ID_FLG(flgcb - flgcb_table);

#ifndef _i_vcre_flg
	if (!SYS_FLGID(flgid)) {
		queue_delete(&(flgcb->wait_queue));
	}
#endif /* _i_vcre_flg */

	queue_initialize(&(flgcb->wait_queue));
	flgcb->flgid = flgid;
	flgcb->exinf = pk_cflg->exinf;
	flgcb->flgatr = pk_cflg->flgatr;
	flgcb->flgptn = pk_cflg->iflgptn;
}

#endif /* !defined(_i_cre_flg) || !defined(_i_vcre_flg) */
#ifndef _i_cre_flg

SYSCALL ER
i_cre_flg(ID flgid, T_CFLG *pk_cflg)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	CHECK_RSATR(pk_cflg->flgatr, TA_WMUL);
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid != 0) {
		ercd =  E_OBJ;
	}
	else {
		_cre_flg(flgcb, pk_cflg);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_flg */
#ifndef _i_vcre_flg

SYSCALL ER
i_vcre_flg(T_CFLG *pk_cflg)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_cflg->flgatr, TA_WMUL);

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_flgcb)) {
		ercd =  EV_FULL;
	}
	else {
		flgcb = (FLGCB *)(free_flgcb.next);
		_cre_flg(flgcb, pk_cflg);
		ercd = (ER)(flgcb->flgid);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_flg */

SYSCALL ER
i_del_flg(ID flgid)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid == 0) {
		ercd = E_NOEXS;
	}
	else {
		wait_delete(&(flgcb->wait_queue));
#ifndef _i_vcre_flg
		if (!SYS_FLGID(flgcb->flgid)) {
			queue_insert(&(flgcb->wait_queue), &free_flgcb);
		}
#endif /* _i_vcre_flg */
		flgcb->flgid = 0;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_set_flg(ID flgid, UINT setptn)
{
	FLGCB	*flgcb;
	TCB	*tcb;
	QUEUE	*queue;
	UINT	wfmode;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid == 0) {
		ercd = E_NOEXS;
		goto error_exit;
	}
	flgcb->flgptn |= setptn;
    
	queue = flgcb->wait_queue.next;
	while (queue != &(flgcb->wait_queue)) {
		tcb = (TCB *) queue;
		queue = queue->next;
		wfmode = tcb->winfo.flg.wfmode;
		if (eventflag_cond(flgcb, tcb->winfo.flg.waiptn, wfmode)) {
			*(tcb->winfo.flg.p_flgptn) = flgcb->flgptn;
			wait_release_ok(tcb);
			if (wfmode & TWF_CLR) {
				flgcb->flgptn = 0;
				break;
			}
		}
	}

    error_exit:
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_clr_flg(ID flgid, UINT clrptn)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid == 0) {
		ercd = E_NOEXS;
	}
	else {
		flgcb->flgptn &= clrptn; 
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_wai_flg(UINT* p_flgptn, ID flgid, UINT waiptn, UINT wfmode)
{
	return(i_twai_flg(p_flgptn, flgid, waiptn, wfmode, TMO_FEVR));
}

SYSCALL ER
i_pol_flg(UINT* p_flgptn, ID flgid, UINT waiptn, UINT wfmode)
{
	return(i_twai_flg(p_flgptn, flgid, waiptn, wfmode, TMO_POL));
}

SYSCALL ER
i_twai_flg(UINT* p_flgptn, ID flgid, UINT waiptn, UINT wfmode, TMO tmout)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	CHECK_PAR(waiptn != 0);
	CHECK_PAR((wfmode & ~(TWF_ORW|TWF_CLR)) == 0);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH();
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid == 0) {
		ercd = E_NOEXS;
	}
	else if (!(flgcb->flgatr & TA_WMUL)
			&& !queue_empty_p(&(flgcb->wait_queue))) {
		ercd = E_OBJ;
	}
	else if (eventflag_cond(flgcb, waiptn, wfmode)) {
		*p_flgptn = flgcb->flgptn;
		if (wfmode & TWF_CLR) {
			flgcb->flgptn = 0;
		}
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_FLG;
			ctxtsk->wercd = &ercd;
			ctxtsk->winfo.flg.waiptn = waiptn;
			ctxtsk->winfo.flg.wfmode = wfmode;
			ctxtsk->winfo.flg.p_flgptn = p_flgptn;
			gcb_make_wait((GCB *) flgcb, tmout);
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_ref_flg(T_RFLG *pk_rflg, ID flgid)
{
	FLGCB	*flgcb;
	ER	ercd = E_OK;

	CHECK_FLGID(flgid);
	CHECK_FLGACV(flgid);
	flgcb = get_flgcb(flgid);

	BEGIN_CRITICAL_SECTION;
	if (flgcb->flgid == 0) {
		ercd = E_NOEXS;
	}
	else {
		pk_rflg->exinf = flgcb->exinf;
		pk_rflg->wtsk = wait_tskid(&(flgcb->wait_queue));
		pk_rflg->flgptn = flgcb->flgptn;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}
