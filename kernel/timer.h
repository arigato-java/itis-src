/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: timer.h,v 1.1 1993/10/15 09:50:46 hiro Exp $
 */

#ifndef _TIMER_
#define _TIMER_

/*
 *  タイマモジュール
 */

#include "queue.h"

/* 
 *  タイマイベントブロックの定義
 */

typedef void	(*CBACK)(VP);	/* コールバック関数の型 */

typedef struct timer_event_block {
	QUEUE	queue;		/* タイマイベントキュー */
	SYSTIME	time;		/* イベント時刻 */
	CBACK	callback;	/* コールバック関数 */
	VP	arg;		/* コールバック関数へ渡す引数 */
} TMEB;

/*
 *  現在時刻 (ソフトウェアクロック)
 */
extern SYSTIME	current_time;

/*
 *  タイマの初期化と停止
 */
extern void	timer_initialize(void);
extern void	timer_shutdown(void);

/*
 *  タイマイベントのタイマキューへの登録
 */
extern void	timer_insert(TMEB *event, TMO tmout,
				CBACK callback, VP arg);
extern void	timer_insert_abs(TMEB *event, SYSTIME *time,
				CBACK callback, VP arg);
extern void	timer_insert_rel(TMEB *event, SYSTIME *time,
				CBACK callback, VP arg);

/*
 *  タイマキューからの削除
 */
Inline void
timer_delete(TMEB *event)
{
	queue_delete(&(event->queue));
}

#endif /* _TIMER_ */
