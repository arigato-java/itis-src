/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: wait.c,v 1.1 1993/10/15 09:50:52 hiro Exp $
 */

/*
 *  タスク間同期・通信オブジェクト汎用ルーチン
 */

#include "itis_kernel.h"
#include "wait.h"

/*
 *  タスクを待ち行列からはずし，タスク状態を更新する．レディ状態になる
 *  場合は，レディキューにつなぐ．
 */
__inline__ void
wait_queue_delete_task(TCB *tcb)
{
	assert(tcb->state & TS_WAIT);

	queue_delete(&(tcb->tskque));
	if (tcb->state == TS_WAIT) {
		make_ready(tcb);
	}
	else {
		tcb->state = TS_SUSPEND;
	}
}

/*
 *  実行中のタスクを待ち状態に移行させ，タイマイベントキューにつなぐ．
 *
 *  ctxtsk は RUN状態になっているのが通常であるが，実行中のシステムコー 
 *  ルの途中で割込みが発生し，その割込みハンドラ中で呼ばれたシステムコー 
 *  ルによって ctxtsk がその他の状態になる場合がある．ただし，WAIT状態
 *  になることはない．
 */
void
make_wait(TMO tmout)
{
	assert(!(ctxtsk->state & TS_WAIT));

	switch (ctxtsk->state) {
	case TS_READY:
		make_non_ready(ctxtsk);
		ctxtsk->state = TS_WAIT;
		break;
	case TS_SUSPEND:
		ctxtsk->state = TS_WAITSUS;
		break;
	}
	timer_insert(&(ctxtsk->wtmeb), tmout,
			(CBACK) wait_queue_delete_task, ctxtsk);
}

/*
 *  待ちキューの先頭のタスクの ID を取り出す．
 */
ID
wait_tskid(QUEUE *wait_queue)
{
	if (queue_empty_p(wait_queue)) {
		return((ID) FALSE);
	}
	else {
		return(((TCB *)(wait_queue->next))->tskid);
	}
}

/*
 *  タスクの待ち状態を解除する．
 */
__inline__ void
wait_release(TCB *tcb)
{
	timer_delete(&(tcb->wtmeb));
	wait_queue_delete_task(tcb);
}

void
wait_release_ok(TCB *tcb)
{
	*(tcb->wercd) = E_OK;
	wait_release(tcb);
}

/*
 *  待ちキューにつながっているタスクの待ち状態をすべて解除し，E_DLTエラー
 *  とする．
 */
void
wait_delete(QUEUE *wait_queue)
{
	TCB	*tcb;

	while (!queue_empty_p(wait_queue)) {
		tcb = (TCB *)(wait_queue->next);
		*(tcb->wercd) = E_DLT;
		wait_release(tcb);
	}
}

/*
 *  タスクを優先度順の待ちキューにつなぐ．
 */
Inline void
queue_insert_tpri(TCB *tcb, QUEUE *queue)
{
	QUEUE	*q;

	q = queue_search_gt(queue, tcb->priority, offsetof(TCB, priority));
	queue_insert(&(tcb->tskque), q);
}

/*
 *  実行中のタスクを待ち状態に移行させ，タイマイベントキューおよびオブ
 *  ジェクトの待ちキューにつなぐ．また，ctxtsk の wgcb を設定する．
 */
void
gcb_make_wait(GCB *gcb, TMO tmout)
{
	ctxtsk->wid = gcb->objid;
	make_wait(tmout);
	if (gcb->objatr & TA_TPRI) {
		queue_insert_tpri(ctxtsk, &(gcb->wait_queue));
	}
	else {
		queue_insert(&(ctxtsk->tskque), &(gcb->wait_queue));
	}
}

/*
 *  タスクの優先度が変わった際に，待ちキューの中でのタスクの位置を修正
 *  する．オブジェクト属性に TA_TPRI が指定されていない場合は，何もしな
 *  い．
 */
void
gcb_change_priority(GCB *gcb, TCB *tcb)
{
	if (gcb->objatr & TA_TPRI) {
		queue_delete(&(tcb->tskque));
		queue_insert_tpri(tcb, &(gcb->wait_queue));
	}
}
