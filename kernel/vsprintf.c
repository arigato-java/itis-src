/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: vsprintf.c,v 1.2 1993/11/01 15:54:24 hiro Exp $
 */

/*
 *  ItIsカーネル用 簡易版 vsprintfライブラリ
 */

#include <stdarg.h>
#include "itis_kernel.h"

#ifdef USE_LONGLONG
typedef long long		LONG;
typedef unsigned long long	ULONG;
#else /* USE_LONGLONG */
typedef int		LONG;
typedef unsigned int	ULONG;
#endif /* USE_LONGLONG */

static void
convert(ULONG val, char **bufpt, int radix,
	const char *radchar, int width, int minus, int padzero)
{
	char	buf[24];
	int	i, j;

	i = 0;
	do {
		buf[i++] = radchar[val % radix];
		val /= radix;
	} while (val);
	if (minus) {
		buf[i++] = '-';
	}

	for (j = i; j < width; j++) {
		*(*bufpt)++ = padzero ? '0' : ' ';
	}
	while (i > 0) {
		*(*bufpt)++ = buf[--i];
	}
}

static char const raddec[] = "0123456789";
static char const radhex[] = "0123456789abcdef";
static char const radHEX[] = "0123456789ABCDEF";

#ifdef USE_LONGLONG
#define GET_ARG	((longlong) ? va_arg(ap, long long) : va_arg(ap, int))
#else /* USE_LONGLONG */
#define GET_ARG	(va_arg(ap, int))
#endif /* USE_LONGLONG */

int 
itis_vsprintf(char *buf, const char *format, va_list ap)
{
	int	c;
	char	*bufpt;
#ifdef USE_LONGLONG
	int	longlong;
#endif /* USE_LONGLONG */
	LONG	val;
	int	width;
	int	padzero;
	char	*str;

	bufpt = buf;
	while (c = *format++) {
		if (c != '%') {
			*bufpt++ = c;
			continue;
		}

		width = padzero = 0;
#ifdef USE_LONGLONG
		longlong = 0;
#endif /* USE_LONGLONG */
		if ((c = *format++) == '0') {
			padzero = 1;
			c = *format++;
		}
		while ('0' <= c && c <= '9') {
			width = width*10 + c - '0';
			c = *format++;
		}
#ifdef USE_LONGLONG
		if (c == 'L') {
			longlong++;
			c = *format++;
		}
#endif /* USE_LONGLONG */
		switch (c) {
		case 'd':
			val = GET_ARG;
			if (val >= 0) {
				convert(val, &bufpt, 10, raddec,
						 width, 0, padzero);
			}
			else {
				convert(-val, &bufpt, 10, raddec,
						width, 1, padzero);
			}
			break;
		case 'u':
			val = GET_ARG;
			convert(val, &bufpt, 10, raddec, width, 0, padzero);
			break;
		case 'x':
			val = GET_ARG;
			convert(val, &bufpt, 16, radhex, width, 0, padzero);
			break;
		case 'X':
			val = GET_ARG;
			convert(val, &bufpt, 16, radHEX, width, 0, padzero);
			break;
		case 'c':
			*bufpt++ = va_arg(ap, int);
			break;
		case 's':
			str = va_arg(ap, char *);
			while (c = *str++) {
				*bufpt++ = c;
			}
			break;
		case '%':
			*bufpt++ = '%';
			break;
		case 0:
			format--;
			break;
		default:
			break;
		}
	}
	*bufpt = 0;
	return(bufpt - buf);
}
