/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: cregs.h,v 1.1 1993/11/12 15:17:05 hiro Exp $
 */

#ifndef _CREGS_
#define _CREGS_

/*
 *  TRON仕様チップの制御レジスタの番地の定義
 */
#define PSW		0x0004		/* Gmicro/200, Gmicro/300, TX-2 */
#define SMRNG		0x000c		/* Gmicro/200, Gmicro/300, TX-2 */
#define IMASK		0x0014		/* Gmicro/200, Gmicro/300, TX-2 */
#define EITVB		0x0024		/* Gmicro/200, Gmicro/300, TX-2 */
#define JRNGVB		0x002c		/* Gmicro/200, Gmicro/300, TX-2 */
#define CTXBB		0x0034		/* Gmicro/200, Gmicro/300, TX-2 */
#define SATB		0x0044		/* Gmicro/200, Gmicro/300 */
#define UATB		0x004c		/* Gmicro/200, Gmicro/300 */
#define LSID		0x0054		/* Gmicro/300 */
#define IOADDR		0x0064
#define IOMASK		0x006c
#define MJRNGV		0x0074		/* TX-2 */
#define CSW		0x0084		/* Gmicro/200, Gmicro/300, TX-2 */
#define DIR		0x008c		/* Gmicro/200, Gmicro/300, TX-2 */
#define DCT		0x0094		/* TX-2 */
#define CTXBFM		0x009c		/* TX-2 */
#define SPI		0x0104		/* Gmicro/200, Gmicro/300, TX-2 */
#define SP0		0x0124		/* Gmicro/200, Gmicro/300, TX-2 */
#define SP1		0x012c		/* Gmicro/200, Gmicro/300, TX-2 */
#define SP2		0x0134		/* Gmicro/200, Gmicro/300, TX-2 */
#define SP3		0x013c		/* Gmicro/200, Gmicro/300, TX-2 */

/*
 *  Gmicro/200
 */
#define G200_DCW	0x0404
#define G200_DCCW	0x040c
#define G200_IBA0	0x0484
#define G200_IBA1	0x048c
#define G200_OBA	0x0504

/*
 *  Gmicro/300
 */
#define G300_DBIW	0x0444
#define G300_RBCW	0x044c
#define G300_IBA0	0x0484
#define G300_IBA1	0x048c
#define G300_OBA0	0x0504
#define G300_OBA1	0x050c

/*
 *  TX-2
 */
#define TX2_DBIW	0x0444
#define TX2_IBA0	0x0484
#define TX2_IBA1	0x048c
#define TX2_OBA0	0x0504
#define TX2_OBA1	0x050c

/*
 *  制御レジスタアクセス用マクロ
 */
#define CREG(ADDR)	(*((UINT *)(ADDR)))

#endif /* _CREGS_ */
