/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: task.h,v 1.7 1993/11/19 04:29:44 hiro Exp $
 */

#ifndef _TASK_
#define _TASK_

#include "queue.h"
#include "timer.h"
#include "winfo.h"

/*
 *  タスク状態の内部表現の定義
 *
 *  タスクが待ち状態かどうかは (state & TS_WAIT) でチェックできる.
 *  タスクが強制待ち状態かどうかは (state & TS_SUSPEND) でチェックできる.
 */
typedef enum task_state {
	TS_NONEXIST = 0,	/* 未登録状態 */
	TS_READY = 1,		/* 実行または実行可能状態 */
	TS_WAIT = 2,		/* 待ち状態 */
	TS_SUSPEND = 4,		/* 強制待ち状態 */
	TS_WAITSUS = 6,		/* 二重待ち状態 */
	TS_DORMANT = 8		/* 休止状態 */
} TSTAT;

/*
 *  タスクが生きているか (NON-EXISTENT，DORMANT以外か) のチェック
 */
Inline BOOL
task_alive(TSTAT state)
{
	return(state & (TS_READY|TS_WAIT|TS_SUSPEND));
}

/*
 *  タスク優先度の内部/外部表現変換マクロ
 */
#define int_priority(x)	((INT)((x) - MIN_PRI))
#define ext_tskpri(x)	((PRI)((x) + MIN_PRI))

/*
 *  タスクコンテキストブロックの定義
 *
 *  TRON仕様チップに依存している．
 */
typedef struct {
	VW	csw;		/* CSW (Context Status Word) */
	VP	sp0;		/* SP0 */
	VP	sp1;		/* SP1 */
	VP	sp2;		/* SP2 */
	VP	sp3;		/* SP3 */
	VW	uatb;		/* UATB */
	VW	lsid;		/* LSID */
} CTXB;

/*
 *  タスクコントロールブロック (TCB) の定義
 */
typedef	struct task_control_block {
	QUEUE	tskque;		/* タスクキュー */
	ID	tskid;		/* タスクID */
	VP	exinf;		/* 拡張情報 */
	ATR	tskatr;		/* タスク属性 */
	FP	task;		/* タスク起動アドレス */
	INT	ipriority;	/* タスク起動時優先度 */
	INT	stksz;		/* ユーザスタックサイズ */
	INT	sstksz;		/* システムスタックサイズ */
	INT	priority;	/* 現在の優先度 */
	TSTAT	state;		/* タスク状態 (内部表現) */
	UINT	tskwait;	/* 待ち要因 */
	ID	wid;		/* 待ちオブジェクトID */
	INT	wupcnt;		/* 起床要求キューイング数 */
	INT	suscnt;		/* SUSPEND要求ネスト数 */
	INT	sysmode;	/* タスク動作モード，準タスク部呼出しレベル */
#ifdef USE_QTSK_PORTION
	INT	isysmode;	/* タスク動作モード初期値 */
#endif /* USE_QTSK_PORTION */

	VP	istack;		/* ユーザスタックポインタの初期値 */
	VP	isstack;	/* システムスタックポインタの初期値 */
#ifdef NUM_PORID
	RNO	wrdvno;		/* ランデブ番号生成用 */
#endif /* NUM_PORID */
	ER	*wercd;		/* 待ちエラーコード設定エリア */
	WINFO	winfo;		/* 待ち情報 */
	TMEB	wtmeb;		/* 待ちタイマイベントブロック */
	CTXB	tskctxb;	/* タスクコンテキストブロック */
} TCB;

/*
 *  実行中のタスク
 *
 *  ctxtsk は，実行中のタスク (= CPU がコンテキストを持っているタスク) 
 *  の TCB を指す変数．システムコールの処理中で，システムコールを要求し
 *  たタスクに関する情報を参照する場合は，ctxtsk を使う．ctxtsk を書き
 *  換えるのは，タスクディスパッチャのみ．
 */
extern TCB *ctxtsk;

/*
 *  実行すべきタスク
 *
 *  schedtsk は，実行すべきタスクの TCB を指す変数．遅延ディスパッチや
 *  ディスパッチの禁止によりディスパッチが遅延されている状態では，
 *  ctxtsk と一致しない．
 */
extern TCB *schedtsk;

/*
 *  TCB のエリア
 *
 *  TCB のエリアは，システム生成時に静的に割り付けている．
 */
extern TCB tcb_table[];

/*
 *  未使用の TCB のリスト
 */
#ifndef _i_vcre_tsk
extern QUEUE free_tcb;
#endif /* _i_vcre_tsk */

/*
 *  タスクID から TCB を取り出す．
 */
#define get_tcb(id)	 (&tcb_table[INDEX_TSK(id)])
#define get_tcb_self(id) ((id)==TSK_SELF ? ctxtsk : &tcb_table[INDEX_TSK(id)])

/*
 *  タスクの実行準備をする．
 */
extern void	make_dormant(TCB *tcb);

/*
 *  タスクを実行可能状態にする．
 *
 *  tcb で示されるタスクの優先度が，現在実行中のタスクの優先度よりも高
 *  い場合は，実行状態にする．そうでない場合は，タスクをレディキューに
 *  つなぐ．
 */
extern void	make_ready(TCB *tcb);

/*
 *  タスクを実行可能以外の状態にする．
 *
 *  tcb で示されるタスクを実行可能以外の状態，つまり待ち状態，強制待ち
 *  状態，休止状態へ移行させる．この関数を呼び出す際には，タスクが実行
 *  可能状態になっていること．tcb->state は，呼び出した側で，この関数か
 *  らリターン後に変更する．
 */
extern void	make_non_ready(TCB *tcb);

/*
 *  実行状態のタスクを変更する．
 *
 *  (実行状態のタスクを除く) レディキューの先頭のタスクが，実行状態のタ
 *  スクと同じか，より高い優先度を持っている場合には，そのタスクを実行
 *  状態にする．そうでない場合はそのまま．
 */
extern void	change_running_task(void);

/*
 *  タスクの優先度を変更する．
 *
 *  tcb で示されるタスクの優先度を priority に変更する．それに伴って必
 *  要となるタスクの状態遷移を起こさせる．
 */
extern void	change_task_priority(TCB *tcb, INT priority);

/*
 *  レディキューを回転させる．
 *
 *  priority で示される優先度レベルのレディキューを回転させる．
 */
extern void	rotate_ready_queue(INT priority);

/*
 *  遅延割込みを使わない場合の定義．
 *
 *  dispatch_disabled は，タスクディスパッチ禁止状態を記憶しておくため
 *  の変数．dispatch() は，タスクディスパッチャを起動するため関数．
 */
#ifdef TRON_NO_DI
extern BOOL	dispatch_disabled;
extern void	dispatch(void);
#endif /* TRON_NO_DI */

#endif /* _TASK_ */
