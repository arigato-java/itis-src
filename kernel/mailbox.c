/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: mailbox.c,v 1.4 1993/11/19 09:17:49 hiro Exp $
 */

#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "queue.h"
#include "check.h"

/*
 *  メイルボックス管理ブロックの定義
 */

typedef struct mailbox_control_block {
	QUEUE	wait_queue;	/* メイルボックス待ちキュー */
	ID	mbxid;		/* メイルボックスID */
	VP	exinf;		/* 拡張情報 */
	ATR	mbxatr;		/* メイルボックス属性 */
	QUEUE	msg_queue;	/* メッセージキュー */
} MBXCB;

static MBXCB	mbxcb_table[NUM_MBXID];

#define get_mbxcb(id)	(&(mbxcb_table[INDEX_MBX(id)]))

/*
 *  未使用のメイルボックス管理ブロックのリスト
 */
#ifndef _i_vcre_mbx
QUEUE	free_mbxcb;
#endif /* _i_vcre_mbx */

/* 
 *  メイルボックス管理ブロックの初期化
 */
void mailbox_initialize()
{
	INT	i;
	MBXCB	*mbxcb;

#ifndef _i_vcre_mbx
	queue_initialize(&free_mbxcb);
#endif /* _i_vcre_mbx */

	for(mbxcb = mbxcb_table, i = 0; i < NUM_MBXID; mbxcb++, i++) {
		mbxcb->mbxid = 0;
#ifndef _i_vcre_mbx
		if (!SYS_MBXID(ID_MBX(i))) {
			queue_insert(&(mbxcb->wait_queue), &free_mbxcb);
		}
#endif /* _i_vcre_mbx */
	}
}

/*
 *  優先度ベースのメッセージキューの操作
 */
Inline void
queue_insert_mpri(T_MSG *pk_msg, QUEUE *queue)
{
	QUEUE	*q;

	q = queue_search_gt(queue, pk_msg->msgpri, offsetof(T_MSG, msgpri));
	queue_insert((QUEUE *)(&(pk_msg->msgque[0])), q);
}

/*
 *  メイルボックス管理機能
 */

#if !defined(_i_cre_mbx) || !defined(_i_vcre_mbx)

static void
_cre_mbx(MBXCB *mbxcb, T_CMBX *pk_cmbx)
{
	ID	mbxid = ID_MBX(mbxcb - mbxcb_table);

#ifndef _i_vcre_mbx
	if (!SYS_MBXID(mbxid)) {
		queue_delete(&(mbxcb->wait_queue));
	}
#endif /* _i_vcre_mbx */

	queue_initialize(&(mbxcb->wait_queue));
	mbxcb->mbxid = mbxid;
	mbxcb->exinf = pk_cmbx->exinf;
	mbxcb->mbxatr = pk_cmbx->mbxatr;
	queue_initialize(&(mbxcb->msg_queue));
}

#endif /* !defined(_i_cre_mbx) || !defined(_i_vcre_mbx) */
#ifndef _i_cre_mbx

SYSCALL ER
i_cre_mbx(ID mbxid, T_CMBX *pk_cmbx)
{
	MBXCB	*mbxcb;
	ER	ercd = E_OK;

	CHECK_MBXID(mbxid);
	CHECK_MBXACV(mbxid);
	CHECK_RSATR(pk_cmbx->mbxatr, TA_MPRI|TA_TPRI);
	mbxcb = get_mbxcb(mbxid);

	BEGIN_CRITICAL_SECTION;
	if (mbxcb->mbxid != 0) {
		ercd =  E_OBJ;
	}
	else {
		_cre_mbx(mbxcb, pk_cmbx);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_mbx */
#ifndef _i_vcre_mbx

SYSCALL ER
i_vcre_mbx(T_CMBX *pk_cmbx)
{
	MBXCB	*mbxcb;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_cmbx->mbxatr, TA_MPRI|TA_TPRI);

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_mbxcb)) {
		ercd =  EV_FULL;
	}
	else {
		mbxcb = (MBXCB *)(free_mbxcb.next);
		_cre_mbx(mbxcb, pk_cmbx);
		ercd = (ER)(mbxcb->mbxid);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_mbx */

SYSCALL ER
i_del_mbx(ID mbxid)
{
	MBXCB	*mbxcb;
	ER	ercd = E_OK;

	CHECK_MBXID(mbxid);
	CHECK_MBXACV(mbxid);
	mbxcb = get_mbxcb(mbxid);
    
	BEGIN_CRITICAL_SECTION;
	if (mbxcb->mbxid == 0) {
		ercd = E_NOEXS;
	}
	else {
		wait_delete(&(mbxcb->wait_queue));
#ifndef _i_vcre_mbx
		if (!SYS_MBXID(mbxcb->mbxid)) {
			queue_insert(&(mbxcb->wait_queue), &free_mbxcb);
		}
#endif /* _i_vcre_mbx */
		mbxcb->mbxid = 0;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_snd_msg(ID mbxid, T_MSG *pk_msg)
{
	MBXCB	*mbxcb;
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_MBXID(mbxid);
	CHECK_MBXACV(mbxid);
	mbxcb = get_mbxcb(mbxid);
    
	BEGIN_CRITICAL_SECTION;
	if (mbxcb->mbxid == 0) {
		ercd = E_NOEXS;
	}
	else if (!queue_empty_p(&(mbxcb->wait_queue))) {
		tcb = (TCB *)(mbxcb->wait_queue.next);
		*(tcb->winfo.mbx.ppk_msg) = pk_msg;
		wait_release_ok(tcb);
	}
	else {
		if (mbxcb->mbxatr & TA_MPRI) {
			queue_insert_mpri(pk_msg, &(mbxcb->msg_queue));
		}
		else {
			queue_insert((QUEUE *)(&(pk_msg->msgque[0])),
					&(mbxcb->msg_queue));
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_rcv_msg(T_MSG **ppk_msg, ID mbxid)
{
	return(i_trcv_msg(ppk_msg, mbxid, TMO_FEVR));
}

SYSCALL ER
i_prcv_msg(T_MSG **ppk_msg, ID mbxid)
{
	return(i_trcv_msg(ppk_msg, mbxid, TMO_POL));
}

SYSCALL ER
i_trcv_msg(T_MSG **ppk_msg, ID mbxid, TMO tmout)
{
	MBXCB	*mbxcb;
	ER	ercd = E_OK;
    
	CHECK_MBXID(mbxid);
	CHECK_MBXACV(mbxid);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH();
	mbxcb = get_mbxcb(mbxid);
    
	BEGIN_CRITICAL_SECTION;
	if (mbxcb->mbxid == 0) {
		ercd = E_NOEXS;
	}
	else if (!queue_empty_p(&(mbxcb->msg_queue))) {
		*ppk_msg = (T_MSG *) queue_delete_next(&(mbxcb->msg_queue));
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_MBX;
			ctxtsk->wercd = &ercd;
			ctxtsk->winfo.mbx.ppk_msg = ppk_msg;
			gcb_make_wait((GCB *) mbxcb, tmout);
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_ref_mbx(T_RMBX *pk_rmbx, ID mbxid)
{
	MBXCB	*mbxcb;
	ER	ercd = E_OK;

	CHECK_MBXID(mbxid);
	CHECK_MBXACV(mbxid);
	mbxcb = get_mbxcb(mbxid);
    
	BEGIN_CRITICAL_SECTION;
	if (mbxcb->mbxid == 0) {
		ercd = E_NOEXS;
	}
	else {
		pk_rmbx->exinf = mbxcb->exinf;
		pk_rmbx->wtsk = wait_tskid(&(mbxcb->wait_queue));
		if (queue_empty_p(&(mbxcb->msg_queue))) {
			pk_rmbx->pk_msg = NADR;
		}
		else {
			pk_rmbx->pk_msg = (T_MSG *)(mbxcb->msg_queue.next);
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  メイルボックス待ちのタスクの優先度が変更された場合の処理
 */
void
mbx_chg_pri(TCB *tcb)
{
	MBXCB	*mbxcb;

	mbxcb = get_mbxcb(tcb->wid);
	assert(mbxcb->mbxid != 0);
	gcb_change_priority((GCB *) mbxcb, tcb);
}
