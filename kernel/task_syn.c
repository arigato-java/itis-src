/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 * 
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: task_sync.c,v 1.1 1993/10/15 09:50:36 hiro Exp $
 */

/*
 *  タスク付属同期機能
 */

#include <limits.h>
#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "check.h"

SYSCALL ER
i_sus_tsk(ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (!task_alive(state = tcb->state)) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else if (tcb->suscnt == INT_MAX) {
		ercd =  E_QOVR;
	}
	else {
		++(tcb->suscnt);
		if (state == TS_READY) {
			make_non_ready(tcb);
			tcb->state = TS_SUSPEND;
		}
		else if (state == TS_WAIT) {
			tcb->state = TS_WAITSUS;
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_rsm_tsk(ID tskid)
{
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	switch (tcb->state) {
	case TS_NONEXIST:
		ercd = E_NOEXS;
		break;
	case TS_DORMANT:
	case TS_READY:
	case TS_WAIT:
		ercd = E_OBJ;
		break;
	case TS_SUSPEND:
		if (--(tcb->suscnt) == 0) {
			make_ready(tcb);
		}
		break;
	case TS_WAITSUS:
		if (--(tcb->suscnt) == 0) {
			tcb->state = TS_WAIT;
		}
		break;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_frsm_tsk(ID tskid)
{
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	switch (tcb->state) {
	case TS_NONEXIST:
		ercd = E_NOEXS;
		break;
	case TS_DORMANT:
	case TS_READY:
	case TS_WAIT:
		ercd = E_OBJ;
		break;
	case TS_SUSPEND:
		tcb->suscnt = 0;
		make_ready(tcb);
		break;
	case TS_WAITSUS:
		tcb->suscnt = 0;
		tcb->state = TS_WAIT;
		break;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_slp_tsk(void)
{
	return(i_tslp_tsk(TMO_FEVR));
}

SYSCALL ER
i_tslp_tsk(TMO tmout)
{
	ER	ercd = E_OK;

	CHECK_TMOUT(tmout);
	CHECK_DISPATCH();

	BEGIN_CRITICAL_SECTION;
	if (ctxtsk->wupcnt > 0) {
		(ctxtsk->wupcnt)--;
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_SLP;
			ctxtsk->wid = 0;
			ctxtsk->wercd = &ercd;
			make_wait(tmout);
			queue_initialize(&(ctxtsk->tskque));
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_wup_tsk(ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (!task_alive(state = tcb->state)) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else if (tcb->wupcnt == INT_MAX) {
		ercd =  E_QOVR;
	}
	else if ((state & TS_WAIT) && tcb->tskwait == TTW_SLP) {
		wait_release_ok(tcb);
	}
	else {
		++(tcb->wupcnt);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_can_wup(W *p_wupcnt, ID tskid)
{
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_TSKID_SELF(tskid);
	CHECK_TSKACV(tskid);
	tcb = get_tcb_self(tskid);

	BEGIN_CRITICAL_SECTION;
	switch (tcb->state) {
	case TS_NONEXIST:
		ercd = E_NOEXS;
		break;
	case TS_DORMANT:
		ercd = E_OBJ;
		break;
	default:
		*p_wupcnt = tcb->wupcnt;
		tcb->wupcnt = 0;
		break;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}
