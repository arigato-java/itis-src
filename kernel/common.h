/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: common.h,v 1.4 1993/12/20 07:29:46 hiro Exp $
 */

#ifndef	_COMMON_
#define	_COMMON_

#include "cpu_inline.h"

/*
 *  クリティカルセクション定義用マクロ
 *
 *  遅延割込みを使わない場合には，クリティカルセクション終了時に，ディ
 *  スパッチが必要かどうかをチェックし，必要ならディスパッチャを起動す
 *  る必要がある．
 */

#ifndef TRON_NO_DI

#define BEGIN_CRITICAL_SECTION	{ INT intmask = disint();
#define END_CRITICAL_SECTION	enaint(intmask); }

#else /* TRON_NO_DI */

#define BEGIN_CRITICAL_SECTION	{ INT intmask = disint();
#define END_CRITICAL_SECTION	enaint(intmask);			\
				if (imask_psw(intmask) == 15) {		\
					dispatch();			\
				} }
#endif /* TRON_NO_DI */

#define DISABLE_INTERRUPT	{ disint(); }

/*
 *  システム状態判別用マクロ
 */

/*
 *  in_indp(): システムコールが，タスク独立部から呼ばれたかを判別するた
 *  めのマクロ．
 *
 *  システムコール呼出しの TRAPA では，PSW中のスタックモードは変化しな
 *  いので，in_indp が呼ばれた時点のスタックモードが 0 ならタスク独立部
 *  から呼ばれたと判別できる．
 */
#define in_indp()	((current_psw() & 0x80000000) == 0)

/*
 *  in_ddsp(): システムコールがディスパッチ禁止中に呼ばれたかを判別する
 *  ためのマクロ．タスク独立部も，ディスパッチ禁止中に含む．
 *
 *  システムコール呼び出しの TRAPA では IMASK は変化しないので，in_ddsp 
 *  が呼ばれた時点の IMASK が 15 未満なら，ディスパッチ禁止中に呼ばれた
 *  と判別できる．タスク独立部で IMASK が 15 になるケースは考えない．
 *
 *  遅延割込みを使わない場合には，IMASK = 14 に相当する状態が，
 *  dispatch_disabled を TRUE にすることで記憶されるため，
 *  dispatch_disabled のチェックも必要である．
 */
#ifndef TRON_NO_DI
#define in_ddsp()	(current_imask() < 15)
#else /* TRON_NO_DI */
#define in_ddsp()	(current_imask() < 15 || dispatch_disabled)
#endif /* TRON_NO_DI */

/*
 *  in_loc(): システムコールが CPUロック中に呼ばれたかを判別するための
 *  マクロ．タスク独立部も，CPUロック中に含む．
 *
 *  システムコール呼び出しの TRAPA では IMASK は変化しないので，in_loc 
 *  が呼ばれた時点の IMASK が 14 未満なら，CPUロック中に呼ばれたと考え
 *  る．実際には，loc_cpu システムコールを使うと，IMASK は 0 になる．タ
 *  スク独立部で IMASK が 14 ないしは 15 になるケースは考えない．
 *
 *  遅延割込みを使わない場合には，IMASK が 14 になることはなく，15 未満
 *  かどうかでチェックする．
 */
#ifndef TRON_NO_DI
#define in_loc()	(current_imask() < 14)
#else /* TRON_NO_DI */
#define in_loc()	(current_imask() < 15)
#endif /* TRON_NO_DI */

/*
 *  in_sysmode(): システムコールがシステムリソースをアクセスできる状態か
 *  ら呼ばれたかを判別するためのマクロ．E_OACVエラーのチェックに用いる．
 *
 *  PSW中のリングレベルは，システムコール呼び出しの TRAPA により変化する
 *  ため，in_sysmode が呼ばれた時点の RNG を見て判別することはできず，
 *  ctxtsk の sysmodeフィールドを見て判別する必要がある．
 */
#define	in_sysmode()	(in_indp() || ctxtsk->sysmode)

/*
 *  in_qtsk(): システムコールが，準タスク部を実行中から呼ばれたかを判別
 *  するためのマクロ．タスク独立部とは区別しないので，in_indp() が FALSE 
 *  の時にのみ使うこと．
 *
 *  この判別を可能にするために，TCB 中に isysmodeフィールドを用意する．
 *  USE_QTSK_PORTION が定義されていない場合には，準タスク部は使われない
 *  ので，常に FALSE とする．
 */
#ifdef USE_QTSK_PORTION
#define in_qtsk()	(ctxtsk->sysmode > ctxtsk->isysmode)
#else /* USE_QTSK_PORTION */
#define in_qtsk()	0
#endif /* USE_QTSK_PORTION */

/*
 *  システム初期化用関数 (各モジュール)
 */
extern void	target_initialize(void);
extern void	task_initialize(void);
extern void	semaphore_initialize(void);
extern void	eventflag_initialize(void);
extern void	mailbox_initialize(void);
extern void	messagebuffer_initialize(void);
extern void	rendezvous_initialize(void);
extern void	memorypool_initialize(void);
extern void	fix_memorypool_initialize(void);
extern void	cyclichandler_initialize(void);
extern void	alarmhandler_initialize(void);
extern void	extended_svc_initialize(void);

/*
 *  システム終了用関数 (startup.c)
 */
extern void	itis_exit(void);

/*
 *  システムメモリプールからのメモリブロックの取得と返却 (mempool.c)
 *
 *  システムメモリプールは，ID が TMPL_OS のメモリプールとしてアクセス
 *  することもできるが，カーネル内から用いるためのインタフェースを用意
 *  している．sys_get_blk は，メモリブロックが取れなかった場合に，待ち
 *  状態には入らずに，即座に NULL を返す．
 */
extern ER	sys_cre_mpl(void);
extern VP	sys_get_blk(INT size);
extern void	sys_rel_blk(VP blk);

/*
 *  カーネル用システムログ記録ライブラリ (syslog.c)
 */
extern void	i_syslog(int class, const char *format, ...);

/*
 *  システムの低レベルルーチン (sysinit.c)
 */
extern int	target_write(const char *buf, unsigned int len);
extern void	target_exit(void);

/*
 *  その他の定義
 */
#define	offsetof(structure, field) ((INT) &(((structure *) 0)->field))

#define SYSCALL

#endif /* _COMMON_ */
