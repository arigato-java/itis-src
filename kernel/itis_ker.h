/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itis_kernel.h,v 1.4 1993/11/18 14:29:51 hiro Exp $
 */

#ifndef _ITIS_KERNEL_
#define _ITIS_KERNEL_

/*
 *  ItIsカーネル 標準インクルードファイル
 */

/*
 *  システム構成設定ファイル
 */
#include "config.h"
#include "isysconf.h"

/*
 *  ItIs 標準インクルードファイル
 */
#include <itron.h>
#include <itron_errno.h>
#include <itron_fncode.h>
#include <itis_debug.h>
#include <itis_stddefs.h>
#include <itis_resources.h>
#include <itis_syslog.h>

/*
 *  カーネル用 assertマクロの定義
 */
#ifndef NDEBUG

extern void	i_assert_failed(const char *file, int line, const char *expr);
#define assert(exp) \
	{ if (!(exp)) { i_assert_failed(__FILE__, __LINE__, #exp); }}

#else /* NDEBUG */

#define assert(exp)

#endif /* NDEBUG */

/*
 *  カーネル標準インクルードファイル
 */
#include "isyscall.h"
#include "common.h"

#endif /* _ITIS_KERNEL_ */
