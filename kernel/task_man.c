/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-94 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: task_manage.c,v 1.13 1994/01/19 09:36:05 hiro Exp $
 */

/*
 *  タスク管理機能
 */

#include "itis_kernel.h"
#include "cpu_inline.h"
#include "timer.h"
#include "task.h"
#include "wait.h"
#include "check.h"

/*
 *  各種の定数の定義
 */
#define MIN_SYS_STACK_SIZE	256	/* システムスタックの最小サイズ */

#ifndef TRON_NO_DI
#define EIT_FRAME_SIZE	3
#else /* TRON_NO_DI */
#define EIT_FRAME_SIZE	4
#endif /* TRON_NO_DI */

/*
 *  タスクの生成と削除
 */

#if !defined(_i_cre_tsk) || !defined(_i_vcre_tsk)

static ER
_cre_tsk(TCB *tcb, T_CTSK *pk_ctsk, INT stksz, INT sstksz, INT sysmode)
{
	VP	stack, sstack;

	if ((sstack = sys_get_blk(sstksz)) == 0) {
		return(E_NOMEM);
	}
	if (stksz > 0 && (stack = sys_get_blk(stksz)) == 0) {
		sys_rel_blk(sstack);
		return(E_NOMEM);
	}

#ifndef _i_vcre_tsk
	if (!SYS_TSKID(tcb->tskid)) {
		queue_delete(&(tcb->tskque));
	}
#endif /* _i_vcre_tsk */

	tcb->exinf = pk_ctsk->exinf;
	tcb->tskatr = pk_ctsk->tskatr;
	tcb->task = pk_ctsk->task;
	tcb->ipriority = int_priority(pk_ctsk->itskpri);
	tcb->stksz = stksz;
	tcb->sstksz = sstksz;

	if (stksz > 0) {
		tcb->istack = (VP)(((VB *) stack) + stksz);
	}
	tcb->isstack = (VP)(((VB *) sstack) + sstksz);
#ifdef USE_QTSK_PORTION
	tcb->isysmode = sysmode;
#else /* USE_QTSK_PORTION */
	tcb->sysmode = sysmode;
#endif /* USE_QTSK_PORTION */

	make_dormant(tcb);
	return(E_OK);
}

#endif /* !defined(_i_cre_tsk) || !defined(_i_vcre_tsk) */
#ifndef _i_cre_tsk

SYSCALL ER
i_cre_tsk(ID tskid, T_CTSK *pk_ctsk)
{
	TCB	*tcb;
	INT	stksz, sstksz, sysmode;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_RSATR(pk_ctsk->tskatr, TA_HLNG|TA_SSTKSZ|TA_RNG3);
	CHECK_PRI(pk_ctsk->itskpri);
	CHECK_PAR(pk_ctsk->stksz >= 0);

	stksz = pk_ctsk->stksz;
	if (pk_ctsk->tskatr & TA_SSTKSZ) {
		CHECK_PAR(pk_ctsk->sstksz >= MIN_SYS_STACK_SIZE);
		sstksz = pk_ctsk->sstksz;
	}
	else {
		sstksz = SYS_STACK_SIZE;
	}
	if (SYS_TSKID(tskid) || (pk_ctsk->tskatr & TA_RNG3) == TA_RNG0) {
		sysmode = 1;
		sstksz += stksz;
		stksz = 0;
	}
	else {
		sysmode = 0;
	}
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (tcb->state != TS_NONEXIST) {
		ercd = E_OBJ;
	}
	else {
		ercd = _cre_tsk(tcb, pk_ctsk, stksz, sstksz, sysmode);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_tsk */
#ifndef _i_vcre_tsk

SYSCALL ER
i_vcre_tsk(T_CTSK *pk_ctsk)
{
	TCB	*tcb;
	INT	stksz, sstksz, sysmode;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_ctsk->tskatr, TA_HLNG|TA_SSTKSZ|TA_RNG3);
	CHECK_PRI(pk_ctsk->itskpri);
	CHECK_PAR(pk_ctsk->stksz >= 0);

	stksz = pk_ctsk->stksz;
	if (pk_ctsk->tskatr & TA_SSTKSZ) {
		CHECK_PAR(pk_ctsk->sstksz >= MIN_SYS_STACK_SIZE);
		sstksz = pk_ctsk->sstksz;
	}
	else {
		sstksz = SYS_STACK_SIZE;
	}
	if ((pk_ctsk->tskatr & TA_RNG3) == TA_RNG0) {
		sysmode = 1;
		sstksz += stksz;
		stksz = 0;
	}
	else {
		sysmode = 0;
	}

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_tcb)) {
		ercd = EV_FULL;
	}
	else {
		tcb = (TCB *)(free_tcb.next);
		ercd = _cre_tsk(tcb, pk_ctsk, stksz, sstksz, sysmode);
		if (ercd == E_OK) {
			ercd = (ER)(tcb->tskid);
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_tsk */

static void
_del_tsk(TCB *tcb)
{
	sys_rel_blk((VP)((VB *)(tcb->isstack) - tcb->sstksz));
	if (tcb->stksz > 0) {
		sys_rel_blk((VP)((VB *)(tcb->istack) - tcb->stksz));
	}
#ifndef _i_vcre_tsk
	if (!SYS_TSKID(tcb->tskid)) {
		queue_insert(&(tcb->tskque), &free_tcb);
	}
#endif /* _i_vcre_tsk */
	tcb->state = TS_NONEXIST;
}

SYSCALL ER
i_del_tsk(ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if ((state = tcb->state) != TS_DORMANT) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else {
		_del_tsk(tcb);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  タスクの起動と終了
 */

SYSCALL ER
i_sta_tsk(ID tskid, INT stacd)
{
	TCB	*tcb;
	VW	*ssp;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if ((state = tcb->state) != TS_DORMANT) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else {
		ssp = tcb->tskctxb.sp0;
		*ssp = stacd;			/* 起動コードを積む (R0) */
		*(ssp+1) = (VW)(tcb->exinf);	/* タスク拡張情報を積む (R1) */
		make_ready(tcb);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

void
_ter_tsk(TCB *tcb)
{
	TSTAT	state;

	if ((state = tcb->state) == TS_READY) {
		make_non_ready(tcb);
	}
	else if (state & TS_WAIT) {
		timer_delete(&(tcb->wtmeb));
		queue_delete(&(tcb->tskque));
	}
}

SYSCALL void
i_ext_tsk(void)
{
	/*
	 *  make_dormant で，使用中のスタックを破壊しないように，スタッ
	 *  ク上にダミーエリアを確保する．
	 */
	(void) alloca(sizeof(VW) * EIT_FRAME_SIZE);

#ifdef CHK_CTX1
	if (in_ddsp()) {
		i_syslog(LOG_KERN|LOG_ALERT, "ext_tsk reports E_CTX.");
	}
#endif /* CHK_CTX1 */

	DISABLE_INTERRUPT;
	_ter_tsk(ctxtsk);
	make_dormant(ctxtsk);

	force_dispatch();
}

SYSCALL void
i_exd_tsk(void)
{
#ifdef CHK_CTX1
	if (in_ddsp()) {
		i_syslog(LOG_KERN|LOG_ALERT, "exd_tsk reports E_CTX.");
	}
#endif /* CHK_CTX1 */

	DISABLE_INTERRUPT;
	_ter_tsk(ctxtsk);

	/*
	 *  ここで _del_tsk を呼ぶ方法は，マルチプロセッサシステムの場
	 *  合で，他のプロセッサとシステムメモリプールを共有している場
	 *  合には正しくない．
	 */
	_del_tsk(ctxtsk);

	force_dispatch();
}

SYSCALL ER
i_ter_tsk(ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	CHECK_NONSELF(tskid);
	CHECK_INTSK();
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (!task_alive(state = tcb->state)) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else {
		_ter_tsk(tcb);
		make_dormant(tcb);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  タスク優先度の変更
 */
SYSCALL ER
i_chg_pri(ID tskid, PRI tskpri)
{
	TCB	*tcb;
	ER	ercd = E_OK;

	CHECK_TSKID_SELF(tskid);
	CHECK_TSKACV(tskid);
	CHECK_PRI_INI(tskpri);
	tcb = get_tcb_self(tskid);

	BEGIN_CRITICAL_SECTION;
	if (tcb->state == TS_NONEXIST) {
		ercd = E_NOEXS;
	}
	else if (tskpri == TPRI_INI) {
		change_task_priority(tcb, tcb->ipriority);
	}
	else {
		change_task_priority(tcb, int_priority(tskpri));
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  レディキューの回転
 */
SYSCALL ER
i_rot_rdq(PRI tskpri)
{
	CHECK_PRI_RUN(tskpri);

	BEGIN_CRITICAL_SECTION;
	if (tskpri == TPRI_RUN) {
		if (in_indp()) {
			change_running_task();
		}
		else {
			rotate_ready_queue(ctxtsk->priority);
		}
	}
	else {
		rotate_ready_queue(int_priority(tskpri));
	}
	END_CRITICAL_SECTION;
	return(E_OK);
}

/*
 *  他タスクの待ち状態解除
 */
SYSCALL ER
i_rel_wai(ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID(tskid);
	CHECK_TSKACV(tskid);
	tcb = get_tcb(tskid);

	BEGIN_CRITICAL_SECTION;
	if (!((state = tcb->state) & TS_WAIT)) {
		ercd = (state == TS_NONEXIST) ? E_NOEXS : E_OBJ;
	}
	else {
		*(tcb->wercd) = E_RLWAI;
		wait_release(tcb);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  自タスクのタスクID参照
 */
SYSCALL ER
i_get_tid(ID* p_tskid)
{
	*p_tskid = in_indp() ? FALSE : ctxtsk->tskid;
	return(E_OK);
}

/*
 *  タスク状態参照
 */
SYSCALL ER 
i_ref_tsk(T_RTSK *pk_rtsk, ID tskid)
{
	TCB	*tcb;
	TSTAT	state;
	ER	ercd = E_OK;

	CHECK_TSKID_SELF(tskid);
	CHECK_TSKACV(tskid);
	tcb = get_tcb_self(tskid);

	BEGIN_CRITICAL_SECTION;
	if ((state = tcb->state) == TS_NONEXIST) {
		ercd = E_NOEXS;
	}
	else {
		pk_rtsk->exinf = tcb->exinf;
		pk_rtsk->tskpri = ext_tskpri(tcb->priority);
		if (state == TS_READY) {
			pk_rtsk->tskstat = (tcb == ctxtsk ? TTS_RUN : TTS_RDY);
		}
		else {
			pk_rtsk->tskstat = (UW)(state << 1);
		}
		if (state & TS_WAIT) {
			pk_rtsk->tskwait = tcb->tskwait;
			pk_rtsk->wid = tcb->wid;
		}
		else {
			pk_rtsk->tskwait = 0;
			pk_rtsk->wid = 0;
		}
		pk_rtsk->wupcnt = tcb->wupcnt;
		pk_rtsk->suscnt = tcb->suscnt;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}
