/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: misc_calls.c,v 1.7 1993/12/10 05:12:27 hiro Exp $
 */

/* 
 *  その他のシステムコール (主に CPU に依存するもの)
 *
 *  TRON仕様チップに依存している関数が多い．
 */

#include "itis_kernel.h"
#include "cpu_inline.h"
#include "task.h"
#include "check.h"
#include "patchlevel.h"

/*
 *  ディスパッチの禁止/許可機能
 */

#ifndef _i_dis_dsp

SYSCALL ER
i_dis_dsp(void)
{
	CHECK_CTX(!in_loc());
#ifndef TRON_NO_DI
	set_imask(14);
#else /* TRON_NO_DI */
	dispatch_disabled = 1;
#endif /* TRON_NO_DI */
	return(E_OK);
}

#endif /* _i_dis_dsp */
#ifndef _i_ena_dsp

SYSCALL ER
i_ena_dsp(void)
{
	CHECK_CTX(!in_loc());
#ifndef TRON_NO_DI
	set_imask(15);
#else /* TRON_NO_DI */
	dispatch_disabled = 0;
	dispatch();
#endif /* TRON_NO_DI */
	return(E_OK);
}

#endif /* _i_ena_dsp */

/*
 *  割込み管理機能
 */

extern void	inthdr_startup(void);

static FP	hll_eit_table[0xff];

#ifndef _i_def_int

SYSCALL ER
i_def_int(UINT dintno, T_DINT *pk_dint)
{
	FP	inthdr;
	UINT	eitatr;

	CHECK_PAR(0x01 <= dintno && dintno <= 0xff);
	CHECK_NOSPT(pk_dint != NADR);
	CHECK_RSATR(pk_dint->intatr, TA_HLNG|TA_EITATR);
	if (pk_dint->intatr & TA_EITATR) {
		CHECK_PAR((pk_dint->eitatr & ~(0x831f0000)) == 0);
		eitatr = pk_dint->eitatr;
	}
	else {
		eitatr = 0x000f0000;
	}
	inthdr = pk_dint->inthdr;

	BEGIN_CRITICAL_SECTION;
	if (pk_dint->intatr & TA_HLNG) {
		hll_eit_table[dintno - 1] = inthdr;
		inthdr = inthdr_startup;
	}
	define_eit(dintno, eitatr, inthdr);
	END_CRITICAL_SECTION;
	return(E_OK);
}

#endif /* _i_def_int */

/*
 *  割込みハンドラを起動のための高級言語対応ルーチン
 *
 *  TRON仕様チップに依存している．
 */
__asm__(".text				\n"
"	.align 1			\n"
"_inthdr_startup:			\n"
"	stm (r0-r6), @-sp		\n"
"	mova @(28,sp).w, r0.w		\n"	/* EITスタックフレームの先頭 */
"	movu @(7,r0).b, r1.w		\n"	/* ベクタ番号を取り出す */
"	jsr @@(_hll_eit_table-4,r1*4)	\n"
"	ldm @sp+, (r0-r6)		\n"
#ifndef TRON_NO_DI
"	reit				");
#else /* TRON_NO_DI */
"	bra __ret_int			");
#endif /* TRON_NO_DI */

/*
 *  割込み禁止/許可機能
 */

#ifndef _i_loc_cpu

SYSCALL ER
i_loc_cpu(void)
{
	CHECK_INTSK();
	set_imask(0);
	return(E_OK);
}

#endif /* _i_loc_cpu */
#ifndef _i_unl_cpu

SYSCALL ER
i_unl_cpu(void)
{
	CHECK_INTSK();
	set_imask(15);
#ifdef TRON_NO_DI
	dispatch_disabled = 0;
	dispatch();
#endif /* TRON_NO_DI */
	return(E_OK);
}

#endif /* _i_unl_cpu */
#ifndef _i_chg_ims

SYSCALL ER
i_chg_ims(UINT imask)
{
	CHECK_PAR(imask <= (in_indp() ? 7 : 15));
	set_imask(imask);
#ifdef TRON_NO_DI
	if (imask == 15) {
		dispatch();
	}
#endif /* TRON_NO_DI */
	return(E_OK);
}

#endif /* _i_chg_ims */
#ifndef _i_ref_ims

SYSCALL ER
i_ref_ims(UINT *p_imask)
{
	*p_imask = current_imask();
	return(E_OK);
}

#endif /* _i_ref_ims */

/*
 *  システム状態参照
 */

#ifndef _i_ref_sys

SYSCALL ER
i_ref_sys(T_RSYS *pk_rsys)
{
	if (in_indp()) {
		pk_rsys->sysstat = TTS_INDP;
	}
	else {
		if (in_qtsk()) {
			pk_rsys->sysstat = TTS_QTSK;
		}
		else {
			pk_rsys->sysstat = TTS_TSK;
		}
		if (in_loc()) {
			pk_rsys->sysstat |= TTS_LOC;
		}
		else if (in_ddsp()) {
			pk_rsys->sysstat |= TTS_DDSP;
		}
	}
	pk_rsys->runtskid = (ctxtsk->state == TS_READY ?
					ctxtsk->tskid : FALSE);
	pk_rsys->schedtskid = (schedtsk ? schedtsk->tskid : FALSE);
	return(E_OK);
}

#endif /* _i_ref_sys */

/*
 *  コンフィグレーション情報参照
 */

#ifndef _i_ref_cfg

SYSCALL ER
i_ref_cfg(T_RCFG *pk_rcfg)
{
	return(E_OK);
}

#endif /* _i_ref_cfg */

/*
 *  拡張SVC 管理機能
 */

#ifdef NUM_SVC

/*
 *  拡張SVC 管理ブロックの定義
 */

typedef ER	(*SVC)();	/* 拡張SVCハンドラアドレスの型 */

typedef struct extended_svc_control_block {
	ATR	svcatr;		/* 拡張SVCハンドラ属性 */
	SVC	svchdr;		/* 拡張SVCハンドラアドレス */
} SVCCB;

SVCCB	svccb_table[NUM_SVC];

#define get_svccb(fn)	(&(svccb_table[(fn)-1]))

extern ER	no_support(void);

/* 
 *  拡張SVC 管理ブロックの初期化
 */
void
extended_svc_initialize(void)
{
	INT	i;

	for (i = 0; i < NUM_SVC; i++) {
		svccb_table[i].svchdr = no_support;
	}
}

/*
 *  拡張SVC の定義
 */

#ifndef _i_def_svc

SYSCALL ER
i_def_svc(FN s_fncd, T_DSVC *pk_dsvc)
{
	SVCCB	*svccb;
	ER	ercd = E_OK;

	CHECK_PAR(0 < s_fncd && s_fncd <= NUM_SVC);
	if (pk_dsvc != NADR) {
		CHECK_RSATR(pk_dsvc->svcatr, TA_HLNG);
	}
	svccb = get_svccb(s_fncd);

	BEGIN_CRITICAL_SECTION;
	if (pk_dsvc == NADR) {
		svccb->svchdr = no_support;
	}
	else {
		svccb->svcatr = pk_dsvc->svcatr;
		svccb->svchdr = (SVC) pk_dsvc->svchdr;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_def_svc */

/*
 *  拡張SVCハンドラへの分岐ルーチン
 *
 *  s_fncd で指定する拡張SVCハンドラへ分岐する．ientry.s の中から呼ば
 *  れる．ctxtsk->sysmode に，準タスク部実行中であることを記録する．
 */
ER
svc_ientry(INT par1, INT par2, INT par3, INT par4, INT par5, INT par6,
		FN s_fncd)
{
	ER	ercd;

	if (s_fncd > NUM_SVC) {
		return(E_RSFN);
	}

	if (!in_indp()) {
		ctxtsk->sysmode++;
		ercd = (*(get_svccb(s_fncd)->svchdr))
				(par1, par2, par3, par4, par5, par6);
		ctxtsk->sysmode--;
	}
	else {
		ercd = (*(get_svccb(s_fncd)->svchdr))
				(par1, par2, par3, par4, par5, par6);
	}
	return(ercd);
}

#endif /* NUM_SVC */

/*
 *  バージョン情報参照
 */

#ifndef _i_get_ver

SYSCALL ER
i_get_ver(T_VER *pk_ver)
{
	pk_ver->maker = 0x0001;		/* 東京大学 */
	pk_ver->id = 0x5003;		/* (深い意味はない) */
	pk_ver->spver = 0x5301;		/* μITRON, Ver.3.01 */
	pk_ver->prver = (MAJOR_REL << 12) + (MINOR_REL << 4) + PATCH_LEVEL;
					/* Release X.YY.Z */
	memset(pk_ver->prno, 0, sizeof(pk_ver->prno));
					/* (未使用) */
	pk_ver->cpu = 0x0001;		/* TRON仕様チップ一般 */
	pk_ver->var = 0xc000;		/* レベルE */
	return(E_OK);
}

#endif /* _i_get_ver */
