/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 * 
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: semaphore.c,v 1.3 1993/11/19 09:18:20 hiro Exp $
 */

#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "check.h"

/*
 *  セマフォ管理ブロックの定義
 */

typedef struct sempahore_control_block {
	QUEUE	wait_queue;	/* セマフォ待ちキュー */
	ID	semid;		/* セマフォID */
	VP	exinf;		/* 拡張情報 */
	ATR	sematr;		/* セマフォ属性 */
	INT	semcnt;		/* セマフォ現在カウント値 */
	INT	maxsem;		/* セマフォ最大カウント値 */
} SEMCB;

static SEMCB	semcb_table[NUM_SEMID];

#define get_semcb(id)	(&(semcb_table[INDEX_SEM(id)]))

/*
 *  未使用のセマフォ管理ブロックのリスト
 */
#ifndef _i_vcre_sem
QUEUE	free_semcb;
#endif /* _i_vcre_sem */

/* 
 *  セマフォ管理ブロックの初期化
 */
void
semaphore_initialize(void)
{
	INT	i;
	SEMCB	*semcb;

#ifndef _i_vcre_sem
	queue_initialize(&free_semcb);
#endif /* _i_vcre_sem */

	for(semcb = semcb_table, i = 0; i < NUM_SEMID; semcb++, i++) {
		semcb->semid = 0;
#ifndef _i_vcre_sem
		if (!SYS_SEMID(ID_SEM(i))) {
			queue_insert(&(semcb->wait_queue), &free_semcb);
		}
#endif /* _i_vcre_sem */
	}
}

/*
 *  セマフォ管理機能
 */

#if !defined(_i_cre_sem) || !defined(_i_vcre_sem)

static void
_cre_sem(SEMCB *semcb, T_CSEM *pk_csem)
{
	ID	semid = ID_SEM(semcb - semcb_table);

#ifndef _i_vcre_sem
	if (!SYS_SEMID(semid)) {
		queue_delete(&(semcb->wait_queue));
	}
#endif /* _i_vcre_sem */

	queue_initialize(&(semcb->wait_queue));
	semcb->semid = semid;
	semcb->exinf = pk_csem->exinf;
	semcb->sematr = pk_csem->sematr;
	semcb->semcnt = pk_csem->isemcnt;
	semcb->maxsem = pk_csem->maxsem;
}

#endif /* !defined(_i_cre_sem) || !defined(_i_vcre_sem) */
#ifndef _i_cre_sem

SYSCALL ER
i_cre_sem(ID semid, T_CSEM *pk_csem)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;

	CHECK_SEMID(semid);
	CHECK_SEMACV(semid);
	CHECK_RSATR(pk_csem->sematr, TA_TPRI);
	CHECK_PAR(pk_csem->isemcnt >= 0);
	CHECK_PAR(pk_csem->maxsem >= pk_csem->isemcnt);
	semcb = get_semcb(semid);

	BEGIN_CRITICAL_SECTION;
	if (semcb->semid != 0) {
		ercd =  E_OBJ;
	}
	else {
		_cre_sem(semcb, pk_csem);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_sem */
#ifndef _i_vcre_sem

SYSCALL ER
i_vcre_sem(T_CSEM *pk_csem)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_csem->sematr, TA_TPRI);
	CHECK_PAR(pk_csem->isemcnt >= 0);
	CHECK_PAR(pk_csem->maxsem >= pk_csem->isemcnt);

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_semcb)) {
		ercd =  EV_FULL;
	}
	else {
		semcb = (SEMCB *)(free_semcb.next);
		_cre_sem(semcb, pk_csem);
		ercd = (ER)(semcb->semid);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_sem */

SYSCALL ER
i_del_sem(ID semid)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;
    
	CHECK_SEMID(semid);
	CHECK_SEMACV(semid);
	semcb = get_semcb(semid);

	BEGIN_CRITICAL_SECTION;
	if (semcb->semid == 0) {
		ercd = E_NOEXS;
	}
	else {
		wait_delete(&(semcb->wait_queue));
#ifndef _i_vcre_sem
		if (!SYS_SEMID(semcb->semid)) {
			queue_insert(&(semcb->wait_queue), &free_semcb);
		}
#endif /* _i_vcre_sem */
		semcb->semid = 0;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_sig_sem(ID semid)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;
    
	CHECK_SEMID(semid);
	CHECK_SEMACV(semid);
	semcb = get_semcb(semid);

	BEGIN_CRITICAL_SECTION;
	if (semcb->semid == 0) {
		ercd = E_NOEXS;
	}
	else if (!queue_empty_p(&(semcb->wait_queue))) {
		wait_release_ok((TCB *)(semcb->wait_queue.next));
	}
	else if (semcb->semcnt >= semcb->maxsem) {
		ercd = E_QOVR;
	}
	else {
		semcb->semcnt += 1;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_wai_sem(ID semid)
{
	return(i_twai_sem(semid, TMO_FEVR));
}

SYSCALL ER
i_preq_sem(ID semid)
{
	return(i_twai_sem(semid, TMO_POL));
}

SYSCALL ER
i_twai_sem(ID semid, TMO tmout)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;

	CHECK_SEMID(semid);
	CHECK_SEMACV(semid);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH();
	semcb = get_semcb(semid);

	BEGIN_CRITICAL_SECTION;
	if (semcb->semid == 0) {
		ercd = E_NOEXS;
	}
	else if (queue_empty_p(&(semcb->wait_queue)) && semcb->semcnt >= 1) {
		semcb->semcnt -= 1;
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_SEM;
			ctxtsk->wercd = &ercd;
			gcb_make_wait((GCB *) semcb, tmout);
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_ref_sem(T_RSEM *pk_rsem, ID semid)
{
	SEMCB	*semcb;
	ER	ercd = E_OK;

	CHECK_SEMID(semid);
	CHECK_SEMACV(semid);
	semcb = get_semcb(semid);

	BEGIN_CRITICAL_SECTION;
	if (semcb->semid == 0) {
		ercd = E_NOEXS;
	}
	else {
		pk_rsem->exinf = semcb->exinf;
		pk_rsem->wtsk = wait_tskid(&(semcb->wait_queue));
		pk_rsem->semcnt = semcb->semcnt;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  セマフォ待ちのタスクの優先度が変更された場合の処理
 */
void
sem_chg_pri(TCB *tcb)
{
	SEMCB	*semcb;

	semcb = get_semcb(tcb->wid);
	assert(semcb->semid != 0);
	gcb_change_priority((GCB *) semcb, tcb);
}
