/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: mempfix.c,v 1.4 1993/12/27 02:11:51 hiro Exp $
 */

#include "itis_kernel.h"
#include "task.h"
#include "wait.h"
#include "check.h"

/*
 *  固定長メモリプール管理ブロックの定義
 */

typedef struct free_list {
	struct free_list *next;
} FREEL;

typedef struct fix_memorypool_control_block {
	QUEUE	wait_queue;	/* メモリプール待ちキュー */
	ID	mpfid;		/* 固定長メモリプールID */
	VP	exinf;		/* 拡張情報 */
	ATR	mpfatr;		/* メモリプール属性 */
	INT	mpfcnt;		/* メモリプール全体のブロック数 */
	INT	blfsz;		/* 固定長メモリブロックのサイズ */
	INT	mpfsz;		/* メモリプール全体のサイズ */
	INT	frbcnt;		/* 空き領域のブロック数 */
	VP	mempool;	/* メモリプールの先頭アドレス */
	VP	unused;		/* 未使用領域の先頭 */
	FREEL	*freelist;	/* 空きブロックのリスト */
} MPFCB;

static MPFCB	mpfcb_table[NUM_MPFID];

#define get_mpfcb(id)	(&(mpfcb_table[INDEX_MPF(id)]))

/*
 *  未使用の固定長メモリプール管理ブロックのリスト
 */
#ifndef _i_vcre_mpf
QUEUE	free_mpfcb;
#endif /* _i_vcre_mpf */

/* 
 *  固定長メモリプール管理ブロックの初期化
 */
void
fix_memorypool_initialize(void)
{
	INT	i;
	MPFCB	*mpfcb;

#ifndef _i_vcre_mpf
	queue_initialize(&free_mpfcb);
#endif /* _i_vcre_mpf */

	for(mpfcb = mpfcb_table, i = 0; i < NUM_MPFID; mpfcb++, i++) {
		mpfcb->mpfid = 0;
#ifndef _i_vcre_mpf
		if (!SYS_MPFID(ID_MPF(i))) {
			queue_insert(&(mpfcb->wait_queue), &free_mpfcb);
		}
#endif /* _i_vcre_mpf */
	}
}

/*
 *  固定長メモリプール管理用ルーチン
 */

#define MINSIZE		(sizeof(FREEL))
#define	MINSZ(sz)	(((sz) + (MINSIZE-1)) & ~(MINSIZE-1))

Inline VP
mempool_end(MPFCB *mpfcb)
{
	return((VP)((VB *)(mpfcb->mempool) + mpfcb->mpfsz));
}

/*
 *  固定長メモリプール管理機能
 */

#if !defined(_i_cre_mpf) || !defined(_i_vcre_mpf)

static ER
_cre_mpf(MPFCB *mpfcb, T_CMPF *pk_cmpf, INT blfsz, INT mpfsz)
{
	ID	mpfid = ID_MPF(mpfcb - mpfcb_table);

	if ((mpfcb->mempool = sys_get_blk(mpfsz)) == 0) {
		return(E_NOMEM);
	}

#ifndef _i_vcre_mpf
	if (!SYS_MPFID(mpfid)) {
		queue_delete(&(mpfcb->wait_queue));
	}
#endif /* _i_vcre_mpf */

	queue_initialize(&(mpfcb->wait_queue));
	mpfcb->mpfid = mpfid;
	mpfcb->exinf = pk_cmpf->exinf;
	mpfcb->mpfatr = pk_cmpf->mpfatr;
	mpfcb->mpfcnt = mpfcb->frbcnt = pk_cmpf->mpfcnt;
	mpfcb->blfsz = blfsz;
	mpfcb->mpfsz = mpfsz;
	mpfcb->unused = mpfcb->mempool;
	mpfcb->freelist = (FREEL *) 0;
	return(E_OK);
}

#endif /* !defined(_i_cre_mpf) || !defined(_i_vcre_mpf) */
#ifndef _i_cre_mpf

SYSCALL ER
i_cre_mpf(ID mpfid, T_CMPF *pk_cmpf)
{
	MPFCB	*mpfcb;
	INT	blfsz, mpfsz;
	ER	ercd = E_OK;

	CHECK_MPFID(mpfid);
	CHECK_MPFACV(mpfid);
	CHECK_RSATR(pk_cmpf->mpfatr, TA_TPRI);
	CHECK_PAR(pk_cmpf->mpfcnt > 0);
	CHECK_PAR(pk_cmpf->blfsz > 0);
	blfsz = MINSZ(pk_cmpf->blfsz);
	mpfsz = blfsz * pk_cmpf->mpfcnt;
	mpfcb = get_mpfcb(mpfid);

	BEGIN_CRITICAL_SECTION;
	if (mpfcb->mpfid != 0) {
		ercd = E_OBJ;
	}
	else {
		ercd = _cre_mpf(mpfcb, pk_cmpf, blfsz, mpfsz);
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_cre_mpf */
#ifndef _i_vcre_mpf

SYSCALL ER
i_vcre_mpf(T_CMPF *pk_cmpf)
{
	MPFCB	*mpfcb;
	INT	blfsz, mpfsz;
	ER	ercd = E_OK;

	CHECK_RSATR(pk_cmpf->mpfatr, TA_TPRI);
	CHECK_PAR(pk_cmpf->mpfcnt > 0);
	CHECK_PAR(pk_cmpf->blfsz > 0);
	blfsz = MINSZ(pk_cmpf->blfsz);
	mpfsz = blfsz * pk_cmpf->mpfcnt;

	BEGIN_CRITICAL_SECTION;
	if (queue_empty_p(&free_mpfcb)) {
		ercd = EV_FULL;
	}
	else {
		mpfcb = (MPFCB *)(free_mpfcb.next);
		ercd = _cre_mpf(mpfcb, pk_cmpf, blfsz, mpfsz);
		if (ercd == E_OK) {
			ercd = (ER)(mpfcb->mpfid);
		}
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

#endif /* _i_vcre_mpf */

SYSCALL ER
i_del_mpf(ID mpfid)
{
	MPFCB	*mpfcb;
	ER	ercd = E_OK;
    
	CHECK_MPFID(mpfid);
	CHECK_MPFACV(mpfid);
	mpfcb = get_mpfcb(mpfid);

	BEGIN_CRITICAL_SECTION;
	if (mpfcb->mpfid == 0) {
		ercd = E_NOEXS;
	}
	else {
		sys_rel_blk(mpfcb->mempool);
		wait_delete(&(mpfcb->wait_queue));
#ifndef _i_vcre_mpf
		if (!SYS_MPFID(mpfcb->mpfid)) {
			queue_insert(&(mpfcb->wait_queue), &free_mpfcb);
		}
#endif /* _i_vcre_mpf */
		mpfcb->mpfid = 0;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_get_blf(VP *p_blf, ID mpfid)
{
	return(i_tget_blf(p_blf, mpfid, TMO_FEVR));
}

SYSCALL ER
i_pget_blf(VP *p_blf, ID mpfid)
{
	return(i_tget_blf(p_blf, mpfid, TMO_POL));
}

SYSCALL ER
i_tget_blf(VP* p_blf, ID mpfid, TMO tmout)
{
	MPFCB	*mpfcb;
	FREEL	*free;
	ER	ercd = E_OK;

	CHECK_MPFID(mpfid);
	CHECK_MPFACV(mpfid);
	CHECK_TMOUT(tmout);
	CHECK_DISPATCH_POL(tmout);
	mpfcb = get_mpfcb(mpfid);

	BEGIN_CRITICAL_SECTION;
	if (mpfcb->mpfid == 0) {
		ercd = E_NOEXS;
	}
	else if (mpfcb->freelist) {
		free = mpfcb->freelist;
		mpfcb->freelist = free->next;
		(mpfcb->frbcnt)--;
		*p_blf = (VP) free;
	}
	else if (mpfcb->unused < mempool_end(mpfcb)) {
		*p_blf = mpfcb->unused;
		mpfcb->unused = (VP)((VB *)(mpfcb->unused) + mpfcb->blfsz);
		(mpfcb->frbcnt)--;
	}
	else {
		if (tmout != TMO_POL) {
			ctxtsk->tskwait = TTW_MPF;
			ctxtsk->wercd = &ercd;
			ctxtsk->winfo.mpf.p_blf = p_blf;
			gcb_make_wait((GCB *) mpfcb, tmout);
		}
		ercd = E_TMOUT;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_rel_blf(ID mpfid, VP blf)
{
	MPFCB	*mpfcb;
	TCB	*tcb;
	FREEL	*free;
	ER	ercd = E_OK;
    
	CHECK_MPFID(mpfid);
	CHECK_MPFACV(mpfid);
	mpfcb = get_mpfcb(mpfid);

	BEGIN_CRITICAL_SECTION;
	if (mpfcb->mpfid == 0) {
		ercd = E_NOEXS;
	}				/* release the block */
#ifdef CHK_PAR
	else if (!(mpfcb->mempool <= blf && blf < mempool_end(mpfcb))) {
		ercd = E_PAR;
	}
#endif /* CHK_PAR */
	else if (!queue_empty_p(&(mpfcb->wait_queue))) {
		tcb = (TCB *)(mpfcb->wait_queue.next);
		*(tcb->winfo.mpf.p_blf) = blf;
		wait_release_ok(tcb);
	}
	else {
		free = (FREEL *) blf;
		free->next = mpfcb->freelist;
		mpfcb->freelist = free;
		(mpfcb->frbcnt)++;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

SYSCALL ER
i_ref_mpf(T_RMPF *pk_rmpf, ID mpfid)
{
	MPFCB	*mpfcb;
	ER	ercd = E_OK;

	CHECK_MPFID(mpfid);
	CHECK_MPFACV(mpfid);
	mpfcb = get_mpfcb(mpfid);

	BEGIN_CRITICAL_SECTION;
	if (mpfcb->mpfid == 0) {
		ercd = E_NOEXS;
	}
	else {
		pk_rmpf->exinf = mpfcb->exinf;
		pk_rmpf->wtsk = wait_tskid(&(mpfcb->wait_queue));
		pk_rmpf->frbcnt = mpfcb->frbcnt;
	}
	END_CRITICAL_SECTION;
	return(ercd);
}

/*
 *  固定長メモリプール待ちのタスクの優先度が変更された場合の処理
 */
void
mpf_chg_pri(TCB *tcb)
{
	MPFCB	*mpfcb;

	mpfcb = get_mpfcb(tcb->wid);
	assert(mpfcb->mpfid != 0);
	gcb_change_priority((GCB *) mpfcb, tcb);
}
