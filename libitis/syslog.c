/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: syslog.c,v 1.2 1993/11/05 12:59:17 hiro Exp $
 */

#include <stdarg.h>
#include <itis_services.h>
#include <itis_syslog.h>
#ifndef LINK_KERNEL
#include <svc_syslog.h>
#endif /* LINK_KERNEL */

/*
 *  アプリケーション用システムログ出力ライブラリ
 */

#define	FORMAT_BUFSIZ	1024

int	__logmask = LOG_UPTO(LOG_NOTICE);

void
syslog(int priority, const char *format, ...)
{
	char	buf[FORMAT_BUFSIZ];
	va_list	ap;
	int	len;

	if (LOG_MASK(priority) & __logmask) {
		va_start(ap, format);
#ifdef LINK_KERNEL
		len = itis_vsprintf(buf, format, ap);
		svc_syslog_send(buf, len);
#else /* LINK_KERNEL */
		len = vsprintf(buf, format, ap);
		syslog_send(buf, len);
#endif /* LINK_KERNEL */
		va_end(ap);
	}
}
