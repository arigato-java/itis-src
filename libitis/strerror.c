/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: strerror.c,v 1.3 1993/12/14 13:25:01 hiro Exp $
 */

#include <itis_services.h>

/*
 *  エラーメッセージ文字列を返す関数
 */
char *const
itron_strerror(ER ercd)
{
	switch (ercd) {
	case E_OK:
		return("E_OK (normal completion)");

	case E_SYS:
		return("E_SYS (system error)");
	case E_NOMEM:
		return("E_NOMEM (memory exhausted)");
	case E_NOSPT:
		return("E_NOSPT (unsupported function)");
	case E_INOSPT:
		return("E_INOSPT (unsupported function of ITRON/FILE)");
	case E_RSFN:
		return("E_RSFN (reserved function code)");
	case E_RSATR:
		return("E_RSATR (reserved attribute)");
	case E_PAR:
		return("E_PAR (parameter error)");
	case E_ID:
		return("E_ID (illegal ID number)");
	case E_NOEXS:
		return("E_NOEXS (object not exist)");
	case E_OBJ:
		return("E_OBJ (illegal object status)");
	case E_MACV:
		return("E_MACV (memory access violation)");
	case E_OACV:
		return("E_OACV (object access violation)");
	case E_CTX:
		return("E_CTX (context error)");
	case E_QOVR:
		return("E_QOVR (queuing/nesting overflow)");
	case E_DLT:
		return("E_DLT (object being waited for is deleted)");
	case E_TMOUT:
		return("E_TMOUT (polling error/time-out)");
	case E_RLWAI:
		return("E_RLWAI (forced release from wait state)");

	case EN_NOND:
		return("EN_NOND (no destination node)");
	case EN_OBJNO:
		return("EN_OBJNO (illegal object number in destination node)");
	case EN_PROTO:
		return("EN_PROTO (unsupported protocol in destination node)");
	case EN_RSFN:
		return("EN_RSFN (unsupported function in destination node)");
	case EN_COMM:
		return("EN_COMM (communication error)");
	case EN_RLWAI:
		return("EN_RLWAI (forced release in connection function)");
	case EN_PAR:
		return("EN_PAR (parameter error in request packet)");
	case EN_RPAR:
		return("EN_RPAR (parameter error in reply packet)");
	case EN_CTXID:
		return("EN_CTXID (remote access from illegal context)");
	case EN_EXEC:
		return("EN_EXEC (resource exhausted in destination node)");
	case EN_NOSPT:
		return("EN_NOSPT (unsupported connection function)");

	case EV_FULL:
		return("EV_FULL (object table full)");

	default:
		return("unknown error");
	}
}
