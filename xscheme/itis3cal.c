/* itiscall.c - xscheme ItIs syscal call functions */
/* Copyright (c) 1991, 1993, by Hiroaki Takada */

#define NODE ITRON_NODE
#include <itis_services.h>
#include <itis_resources.h>
#undef NODE

#include "xscheme.h"

/*
 *  Task management system call functions.
 */

LVAL	x_cre_tsk()
{
	LVAL	val;
	int	tskid;
	T_CTSK	pk_ctsk;

	val = xlgafixnum(); tskid = getfixnum(val);
	val = xlgafixnum(); pk_ctsk.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_ctsk.tskatr = getfixnum(val);
	val = xlgafixnum(); pk_ctsk.task = (FP) getfixnum(val);
	val = xlgafixnum(); pk_ctsk.itskpri = getfixnum(val);
	val = xlgafixnum(); pk_ctsk.stksz = getfixnum(val);
	if (moreargs()) {
		val = xlgafixnum(); pk_ctsk.sstksz = getfixnum(val);
	}
	xllastarg();

	return(cvfixnum((int) cre_tsk(tskid, &pk_ctsk)));
}

LVAL	x_sta_tsk()
{
	LVAL	val;
	int	tskid, stacd;

	val = xlgafixnum(); tskid = getfixnum(val);
	val = xlgafixnum(); stacd = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) sta_tsk(tskid, stacd)));
}

LVAL	x_del_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_tsk(tskid)));
}

LVAL	x_ext_tsk()
{
	xllastarg();
	ext_tsk();
}

LVAL	x_exd_tsk()
{
	xllastarg();
	exd_tsk();
}

LVAL	x_ter_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) ter_tsk(tskid)));
}

LVAL	x_dis_dsp()
{
	xllastarg();
	return(cvfixnum((int) dis_dsp()));
}

LVAL	x_ena_dsp()
{
	xllastarg();
	return(cvfixnum((int) ena_dsp()));
}

LVAL	x_chg_pri()
{
	LVAL	val;
	int	tskid, tskpri;

	val = xlgafixnum(); tskid = getfixnum(val);
	val = xlgafixnum(); tskpri = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) chg_pri(tskid, tskpri)));
}

LVAL	x_rot_rdq()
{
	LVAL	val;
	int	tskpri;

	val = xlgafixnum(); tskpri = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) rot_rdq(tskpri)));
}

LVAL	x_rel_wai()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) rel_wai(tskid)));
}

LVAL	x_get_tid()
{
	LVAL	val;
	int	tskid, rc;

	xllastarg();

	rc = get_tid(&tskid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) tskid), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_ref_tsk()
{
	LVAL	val;
	int	tskid, rc;
	T_RTSK	pk_rtsk;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	rc = ref_tsk(&pk_rtsk, tskid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rtsk.wupcnt), NIL);
		val = cons(cvfixnum((int) pk_rtsk.suscnt), val);
		val = cons(cvfixnum((int) pk_rtsk.wid), val);
		val = cons(cvfixnum((int) pk_rtsk.tskwait), val);
		val = cons(cvfixnum((int) pk_rtsk.tskstat), val);
		val = cons(cvfixnum((int) pk_rtsk.tskpri), val);
		val = cons(cvfixnum((int) pk_rtsk.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

/*
 *  Task-dependent synchronization system call functions.
 */

LVAL	x_sus_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) sus_tsk(tskid)));
}

LVAL	x_rsm_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) rsm_tsk(tskid)));
}

LVAL	x_frsm_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) frsm_tsk(tskid)));
}

LVAL	x_slp_tsk()
{
	LVAL	val;

	xllastarg();
	return(cvfixnum((int) slp_tsk()));
}

LVAL	x_tslp_tsk()
{
	LVAL	val;
	int	tmout;

	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) tslp_tsk(tmout)));
}

LVAL	x_wup_tsk()
{
	LVAL	val;
	int	tskid;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) wup_tsk(tskid)));
}

LVAL	x_can_wup()
{
	LVAL	val;
	int	tskid, wupcnt, rc;

	val = xlgafixnum(); tskid = getfixnum(val);
	xllastarg();

	rc = can_wup(&wupcnt, tskid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) wupcnt), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

/*
 *  Synchronization and communication system call functions.
 */

LVAL	x_cre_sem()
{
	LVAL	val;
	int	semid;
	T_CSEM	pk_csem;

	val = xlgafixnum(); semid = getfixnum(val);
	val = xlgafixnum(); pk_csem.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_csem.sematr = getfixnum(val);
	val = xlgafixnum(); pk_csem.isemcnt = getfixnum(val);
	val = xlgafixnum(); pk_csem.maxsem = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) cre_sem(semid, &pk_csem)));
}

LVAL	x_del_sem()
{
	LVAL	val;
	int	semid;

	val = xlgafixnum(); semid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_sem(semid)));
}

LVAL	x_sig_sem()
{
	LVAL	val;
	int	semid;

	val = xlgafixnum(); semid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) sig_sem(semid)));
}

LVAL	x_wai_sem()
{
	LVAL	val;
	int	semid, tmout;

	val = xlgafixnum(); semid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) wai_sem(semid)));
}

LVAL	x_preq_sem()
{
	LVAL	val;
	int	semid, tmout;

	val = xlgafixnum(); semid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) preq_sem(semid)));
}

LVAL	x_twai_sem()
{
	LVAL	val;
	int	semid, tmout;

	val = xlgafixnum(); semid = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) twai_sem(semid, tmout)));
}

LVAL	x_ref_sem()
{
	LVAL	val;
	int	semid, rc;
	T_RSEM	pk_rsem;

	val = xlgafixnum(); semid = getfixnum(val);
	xllastarg();

	rc = ref_sem(&pk_rsem, semid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rsem.semcnt), NIL);
		val = cons(cvfixnum((int) pk_rsem.wtsk), val);
		val = cons(cvfixnum((int) pk_rsem.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_cre_flg()
{
	LVAL	val;
	int	flgid;
	T_CFLG	pk_cflg;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); pk_cflg.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_cflg.flgatr = getfixnum(val);
	val = xlgafixnum(); pk_cflg.iflgptn = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) cre_flg(flgid, &pk_cflg)));
}

LVAL	x_del_flg()
{
	LVAL	val;
	int	flgid;

	val = xlgafixnum(); flgid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_flg(flgid)));
}

LVAL	x_set_flg()
{
	LVAL	val;
	int	flgid, setptn;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); setptn = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) set_flg(flgid, setptn)));
}

LVAL	x_clr_flg()
{
	LVAL	val;
	int	flgid, clrptn;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); clrptn = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) clr_flg(flgid, clrptn)));
}

LVAL	x_wai_flg()
{
	LVAL	val;
	int	flgptn, flgid, waiptn, wfmode, rc;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); waiptn = getfixnum(val);
	val = xlgafixnum(); wfmode = getfixnum(val);
	xllastarg();

	rc = wai_flg(&flgptn, flgid, waiptn, wfmode);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) flgptn), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_pol_flg()
{
	LVAL	val;
	int	flgptn, flgid, waiptn, wfmode, rc;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); waiptn = getfixnum(val);
	val = xlgafixnum(); wfmode = getfixnum(val);
	xllastarg();

	rc = pol_flg(&flgptn, flgid, waiptn, wfmode);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) flgptn), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_twai_flg()
{
	LVAL	val;
	int	flgptn, flgid, waiptn, wfmode, tmout, rc;

	val = xlgafixnum(); flgid = getfixnum(val);
	val = xlgafixnum(); waiptn = getfixnum(val);
	val = xlgafixnum(); wfmode = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	rc = twai_flg(&flgptn, flgid, waiptn, wfmode, tmout);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) flgptn), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_ref_flg()
{
	LVAL	val;
	int	flgid, rc;
	T_RFLG	pk_rflg;

	val = xlgafixnum(); flgid = getfixnum(val);
	xllastarg();

	rc = ref_flg(&pk_rflg, flgid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rflg.flgptn), NIL);
		val = cons(cvfixnum((int) pk_rflg.wtsk), val);
		val = cons(cvfixnum((int) pk_rflg.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_cre_mbx()
{
	LVAL	val;
	int	mbxid;
	T_CMBX	pk_cmbx;

	val = xlgafixnum(); mbxid = getfixnum(val);
	val = xlgafixnum(); pk_cmbx.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_cmbx.mbxatr = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) cre_mbx(mbxid, &pk_cmbx)));
}

LVAL	x_del_mbx()
{
	LVAL	val;
	int	mbxid;

	val = xlgafixnum(); mbxid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_mbx(mbxid)));
}

LVAL	x_snd_msg()
{
	LVAL	val;
	int	mbxid;
	T_MSG	*pk_msg;

	val = xlgafixnum(); mbxid = getfixnum(val);
	val = xlgafixnum(); pk_msg = (T_MSG *) getfixnum(val);
	xllastarg();

	return(cvfixnum((int) snd_msg(mbxid, pk_msg)));
}

LVAL	x_rcv_msg()
{
	LVAL	val;
	int	mbxid, rc;
	T_MSG	*pk_msg;

	val = xlgafixnum(); mbxid = getfixnum(val);
	xllastarg();

	rc = rcv_msg(&pk_msg, mbxid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_msg), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_prcv_msg()
{
	LVAL	val;
	int	mbxid, rc;
	T_MSG	*pk_msg;

	val = xlgafixnum(); mbxid = getfixnum(val);
	xllastarg();

	rc = prcv_msg(&pk_msg, mbxid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_msg), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_trcv_msg()
{
	LVAL	val;
	int	mbxid, tmout, rc;
	T_MSG	*pk_msg;

	val = xlgafixnum(); mbxid = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	rc = trcv_msg(&pk_msg, mbxid, tmout);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_msg), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_ref_mbx()
{
	LVAL	val;
	int	mbxid, rc;
	T_RMBX	pk_rmbx;

	val = xlgafixnum(); mbxid = getfixnum(val);
	xllastarg();

	rc = ref_mbx(&pk_rmbx, mbxid);
	if (rc==E_OK) {
		val = cons(cvfixnum((int) pk_rmbx.pk_msg), NIL);
		val = cons(cvfixnum((int) pk_rmbx.wtsk), val);
		val = cons(cvfixnum((int) pk_rmbx.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

/*
 *  Extended synchronization and communication system call functions.
 */

LVAL	x_cre_mbf()
{
	LVAL	val;
	int	mbfid;
	T_CMBF	pk_cmbf;

	val = xlgafixnum(); mbfid = getfixnum(val);
	val = xlgafixnum(); pk_cmbf.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_cmbf.mbfatr = getfixnum(val);
	val = xlgafixnum(); pk_cmbf.bufsz = getfixnum(val);
	val = xlgafixnum(); pk_cmbf.maxmsz = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) cre_mbf(mbfid, &pk_cmbf)));
}

LVAL	x_del_mbf()
{
	LVAL	val;
	int	mbfid;

	val = xlgafixnum(); mbfid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_mbf(mbfid)));
}

LVAL	x_snd_mbf()
{
	LVAL	val;
	int	mbfid, msg, msgsz;

	val = xlgafixnum(); mbfid = getfixnum(val);
	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); msgsz = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) snd_mbf(mbfid, (VP) msg, msgsz)));
}

LVAL	x_psnd_mbf()
{
	LVAL	val;
	int	mbfid, msg, msgsz;

	val = xlgafixnum(); mbfid = getfixnum(val);
	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); msgsz = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) psnd_mbf(mbfid, (VP) msg, msgsz)));
}

LVAL	x_tsnd_mbf()
{
	LVAL	val;
	int	mbfid, msg, msgsz, tmout;

	val = xlgafixnum(); mbfid = getfixnum(val);
	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); msgsz = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) tsnd_mbf(mbfid, (VP) msg, msgsz, tmout)));
}

LVAL	x_rcv_mbf()
{
	LVAL	val;
	int	msg, msgsz, mbfid, rc;

	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); mbfid = getfixnum(val);
	xllastarg();

	rc = rcv_mbf((VP) msg, &msgsz, mbfid);
	if (rc == E_OK) {
		val = cons(cvfixnum(msgsz), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_prcv_mbf()
{
	LVAL	val;
	int	msg, msgsz, mbfid, rc;

	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); mbfid = getfixnum(val);
	xllastarg();

	rc = prcv_mbf((VP) msg, &msgsz, mbfid);
	if (rc == E_OK) {
		val = cons(cvfixnum(msgsz), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_trcv_mbf()
{
	LVAL	val;
	int	msg, msgsz, mbfid, tmout, rc;

	val = xlgafixnum(); msg = getfixnum(val);
	val = xlgafixnum(); mbfid = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	rc = trcv_mbf((VP) msg, &msgsz, mbfid, tmout);
	if (rc == E_OK) {
		val = cons(cvfixnum(msgsz), NIL);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_ref_mbf()
{
	LVAL	val;
	int	mbfid, rc;
	T_RMBF	pk_rmbf;

	val = xlgafixnum(); mbfid = getfixnum(val);
	xllastarg();

	rc = ref_mbf(&pk_rmbf, mbfid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rmbf.frbufsz), NIL);
		val = cons(cvfixnum((int) pk_rmbf.msgsz), val);
		val = cons(cvfixnum((int) pk_rmbf.stsk), val);
		val = cons(cvfixnum((int) pk_rmbf.wtsk), val);
		val = cons(cvfixnum((int) pk_rmbf.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

#if 0

/*
 *  Memory pool management system calls functions.
 */

LVAL	x_cre_mpl()
{
	LVAL	val;
	int	mplid, mplatr, mplsz, blksz;

	val = xlgafixnum(); mplid = getfixnum(val);
	val = xlgafixnum(); mplatr = getfixnum(val);
	val = xlgafixnum(); mplsz = getfixnum(val);
	val = xlgafixnum(); blksz = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) cre_mpl(mplid, mplatr, mplsz, blksz)));
}

LVAL	x_del_mpl()
{
	LVAL	val;
	int	mplid;

	val = xlgafixnum(); mplid = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) del_mpl(mplid)));
}

LVAL	x_get_blk()
{
	LVAL	val;
	int	blk, mplid, bcnt, tmout, rc;

	val = xlgafixnum(); mplid = getfixnum(val);
	val = xlgafixnum(); bcnt = getfixnum(val);
	val = xlgafixnum(); tmout = getfixnum(val);
	xllastarg();

	rc = get_blk(&((VP) blk), mplid, bcnt, tmout);
	if (rc==E_OK) {
		val = cons(cvfixnum(blk), NIL);
	}
	else {
		val = cvfixnum((int)rc);
	}
	return(val);
}

LVAL	x_rel_blk()
{
	LVAL	val;
	int	mplid, blk;

	val = xlgafixnum(); mplid = getfixnum(val);
	val = xlgafixnum(); blk = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) rel_blk(mplid, (VP)blk)));
}

#endif /* 0 */

LVAL	x_ref_mpl()
{
	LVAL	val;
	int	mplid, rc;
	T_RMPL	pk_rmpl;

	val = xlgafixnum(); mplid = getfixnum(val);
	xllastarg();

	rc = ref_mpl(&pk_rmpl, mplid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rmpl.maxsz), NIL);
		val = cons(cvfixnum((int) pk_rmpl.frsz), val);
		val = cons(cvfixnum((int) pk_rmpl.wtsk), val);
		val = cons(cvfixnum((int) pk_rmpl.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_ref_mpf()
{
	LVAL	val;
	int	mpfid, rc;
	T_RMPF	pk_rmpf;

	val = xlgafixnum(); mpfid = getfixnum(val);
	xllastarg();

	rc = ref_mpf(&pk_rmpf, mpfid);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rmpf.frbcnt), NIL);
		val = cons(cvfixnum((int) pk_rmpf.wtsk), val);
		val = cons(cvfixnum((int) pk_rmpf.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

/*
 *  Time management system call functions.
 */

typedef struct longlong {
	int	utime;
	int	ltime;
} TIME;

LVAL	x_set_tim()
{
	LVAL	val;
	TIME	time;

	val = xlgafixnum(); time.utime = getfixnum(val);
	val = xlgafixnum(); time.ltime = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) set_tim((SYSTIME *) &time)));
}

LVAL	x_get_tim()
{
	LVAL	val;
	TIME	time;
	int	rc;

	xllastarg();

	rc = get_tim((SYSTIME *) &time);
	if (rc == E_OK) {
		val = cons(cvfixnum(time.ltime), NIL);
		val = cons(cvfixnum(time.utime), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_dly_tsk()
{
	LVAL	val;
	int	dlytim;

	val = xlgafixnum(); dlytim = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) dly_tsk(dlytim)));
}

LVAL	x_def_cyc()
{
	LVAL	val;
	int	cycno;
	T_DCYC	pk_dcyc;

	val = xlgafixnum(); cycno = getfixnum(val);
	val = xlgafixnum(); pk_dcyc.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cycatr = getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cychdr = (FP) getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cycact = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dcyc.cyctim))->utime = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dcyc.cyctim))->ltime = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) def_cyc(cycno, &pk_dcyc)));
}

LVAL	x_act_cyc()
{
	LVAL	val;
	int	cycno, cycact;

	val = xlgafixnum(); cycno = getfixnum(val);
	val = xlgafixnum(); cycact = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) act_cyc(cycno, cycact)));
}

LVAL	x_ref_cyc()
{
	LVAL	val;
	int	cycno, rc;
	T_RCYC	pk_rcyc;

	val = xlgafixnum(); cycno = getfixnum(val);
	xllastarg();

	rc = ref_cyc(&pk_rcyc, cycno);
	if (rc == E_OK) {
		val = cons(cvfixnum((int) pk_rcyc.cycact), NIL);
		val = cons(cvfixnum(((TIME*)(&pk_rcyc.lfttim))->ltime), val);
		val = cons(cvfixnum(((TIME*)(&pk_rcyc.lfttim))->utime), val);
		val = cons(cvfixnum((int) pk_rcyc.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_def_alm()
{
	LVAL	val;
	int	almno;
	T_DALM	pk_dalm;

	val = xlgafixnum(); almno = getfixnum(val);
	val = xlgafixnum(); pk_dalm.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_dalm.almatr = getfixnum(val);
	val = xlgafixnum(); pk_dalm.almhdr = (FP) getfixnum(val);
	val = xlgafixnum(); pk_dalm.tmmode = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dalm.almtim))->utime = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dalm.almtim))->ltime = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) def_alm(almno, &pk_dalm)));
}

LVAL	x_ref_alm()
{
	LVAL	val;
	int	almno, rc;
	T_RALM	pk_ralm;

	val = xlgafixnum(); almno = getfixnum(val);
	xllastarg();

	rc = ref_alm(&pk_ralm, almno);
	if (rc == E_OK) {
		val = cons(cvfixnum(((TIME*)(&pk_ralm.lfttim))->ltime), NIL);
		val = cons(cvfixnum(((TIME*)(&pk_ralm.lfttim))->utime), val);
		val = cons(cvfixnum((int) pk_ralm.exinf), val);
	}
	else {
		val = cvfixnum((int) rc);
	}
	return(val);
}

LVAL	x_string_snd_mbf()
{
	LVAL	val;
	LVAL	str;
	int	mbfid, msg, msgsz;

	val = xlgafixnum(); mbfid = getfixnum(val);
	str = xlgastring();
	xllastarg();

	msg = (int) getstring(str);
	msgsz = getslength(str) - 1;
	return(cvfixnum((int) snd_mbf(mbfid, (VP) msg, msgsz)));
}

/*
 *  Memory access functions.
 */

LVAL	x_read_mem_w()
{
	LVAL	val;
	int	addr;

	val = xlgafixnum(); addr = getfixnum(val);
	xllastarg();

	return(cvfixnum(*((unsigned int *) addr)));
}

LVAL	x_read_mem_h()
{
	LVAL	val;
	int	addr;

	val = xlgafixnum(); addr = getfixnum(val);
	xllastarg();

	return(cvfixnum(*((unsigned short *) addr)));
}

LVAL	x_read_mem_b()
{
	LVAL	val;
	int	addr;

	val = xlgafixnum(); addr = getfixnum(val);
	xllastarg();

	return(cvfixnum(*((unsigned char *) addr)));
}

LVAL	x_write_mem_w()
{
	LVAL	val;
	int	addr, data;

	val = xlgafixnum(); addr = getfixnum(val);
	val = xlgafixnum(); data = getfixnum(val);
	xllastarg();

	*((unsigned int *) addr) = ((unsigned int) data);
	return(val);
}

LVAL	x_write_mem_h()
{
	LVAL	val;
	int	addr, data;

	val = xlgafixnum(); addr = getfixnum(val);
	val = xlgafixnum(); data = getfixnum(val);
	xllastarg();

	*((unsigned short *) addr) = ((unsigned short) data);
	return(val);
}

LVAL	x_write_mem_b()
{
	LVAL	val;
	int	addr, data;

	val = xlgafixnum(); addr = getfixnum(val);
	val = xlgafixnum(); data = getfixnum(val);
	xllastarg();

	*((unsigned char *) addr) = ((unsigned char) data);
	return(val);
}

#ifdef USE_AUTO_ID

LVAL	x_vcre_tsk()
{
	LVAL	val;
	T_CTSK	pk_ctsk;

	val = xlgafixnum(); pk_ctsk.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_ctsk.tskatr = getfixnum(val);
	val = xlgafixnum(); pk_ctsk.task = (FP) getfixnum(val);
	val = xlgafixnum(); pk_ctsk.itskpri = getfixnum(val);
	val = xlgafixnum(); pk_ctsk.stksz = getfixnum(val);
	if (moreargs()) {
		val = xlgafixnum(); pk_ctsk.sstksz = getfixnum(val);
	}
	xllastarg();

	return(cvfixnum((int) vcre_tsk(&pk_ctsk)));
}

LVAL	x_vcre_sem()
{
	LVAL	val;
	T_CSEM	pk_csem;

	val = xlgafixnum(); pk_csem.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_csem.sematr = getfixnum(val);
	val = xlgafixnum(); pk_csem.isemcnt = getfixnum(val);
	val = xlgafixnum(); pk_csem.maxsem = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) vcre_sem(&pk_csem)));
}

LVAL	x_vdef_cyc()
{
	LVAL	val;
	T_DCYC	pk_dcyc;

	val = xlgafixnum(); pk_dcyc.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cycatr = getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cychdr = (FP) getfixnum(val);
	val = xlgafixnum(); pk_dcyc.cycact = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dcyc.cyctim))->utime = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dcyc.cyctim))->ltime = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) vdef_cyc(&pk_dcyc)));
}

LVAL	x_vdef_alm()
{
	LVAL	val;
	T_DALM	pk_dalm;

	val = xlgafixnum(); pk_dalm.exinf = (VP) getfixnum(val);
	val = xlgafixnum(); pk_dalm.almatr = getfixnum(val);
	val = xlgafixnum(); pk_dalm.almhdr = (FP) getfixnum(val);
	val = xlgafixnum(); pk_dalm.tmmode = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dalm.almtim))->utime = getfixnum(val);
	val = xlgafixnum(); ((TIME*)(&pk_dalm.almtim))->ltime = getfixnum(val);
	xllastarg();

	return(cvfixnum((int) vdef_alm(&pk_dalm)));
}

#endif /* USE_AUTO_ID */
