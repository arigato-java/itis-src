/* osdefs.h - extern declarations for machine specific functions */

#ifdef MACINTOSH
extern LVAL xhidepen(),xshowpen(),xgetpen(),xpensize(),xpenmode();
extern LVAL xpenpat(),xpennormal(),xmoveto(),xmove(),xlineto(),xline();
extern LVAL xshowgraphics(),xhidegraphics(),xcleargraphics();
#endif

#ifdef MSDOS
extern LVAL xsystem(),xgetkey(),xtime(),xdifftime();
#ifdef NOTDEF
extern LVAL xint86(),xinbyte(),xoutbyte();
#endif
#endif

#ifdef UNIX
extern LVAL xsystem();
#endif

#ifdef ITIS
extern LVAL x_cre_tsk(), x_sta_tsk();
extern LVAL x_del_tsk(), x_ext_tsk(), x_exd_tsk(), x_ter_tsk();
extern LVAL x_dis_dsp(), x_ena_dsp(), x_chg_pri(), x_rot_rdq();
extern LVAL x_rot_rdq(), x_rel_wai(), x_get_tid(), x_ref_tsk();

extern LVAL x_sus_tsk(), x_rsm_tsk(), x_frsm_tsk();
extern LVAL x_slp_tsk(), x_tslp_tsk(), x_wup_tsk(), x_can_wup();

extern LVAL x_cre_sem(), x_del_sem(), x_sig_sem();
extern LVAL x_wai_sem(), x_preq_sem(), x_twai_sem(), x_ref_sem();
extern LVAL x_cre_flg(), x_del_flg(), x_set_flg(), x_clr_flg();
extern LVAL x_wai_flg(), x_pol_flg(), x_twai_flg(), x_ref_flg();
extern LVAL x_cre_mbx(), x_del_mbx(), x_snd_msg();
extern LVAL x_rcv_msg(), x_prcv_msg(), x_trcv_msg(), x_ref_mbx();

extern LVAL x_cre_mbf(), x_del_mbf();
extern LVAL x_snd_mbf(), x_psnd_mbf(), x_tsnd_mbf();
extern LVAL x_rcv_mbf(), x_prcv_mbf(), x_trcv_mbf(), x_ref_mbf();

extern LVAL x_ref_mpl(), x_ref_mpf();

extern LVAL x_set_tim(), x_get_tim(), x_dly_tsk();
extern LVAL x_def_cyc(), x_act_cyc(), x_ref_cyc();
extern LVAL x_def_alm(), x_ref_alm();

extern LVAL x_string_snd_mbf();

extern LVAL x_read_mem_w(), x_read_mem_h(), x_read_mem_b();
extern LVAL x_write_mem_w(), x_write_mem_h(), x_write_mem_b();

#ifdef USE_AUTO_ID
extern LVAL x_vcre_tsk(), x_vcre_sem();
extern LVAL x_vdef_cyc(), x_vdef_alm();
#endif /* USE_AUTO_ID */
#endif /* ITIS */
