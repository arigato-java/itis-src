/* itisstuff.c - ItIs3 specific routines. */

#define NODE ITIS_NODE
#include <itis_services.h>
#include <itis_resources.h>
#include <itis_ioctl.h>
#undef NODE

#include "xscheme.h"
FILE *fopen();

#define LBSIZE 200

/* external variables */
extern FILE *tfp;

/* local variables */
static char lbuf[LBSIZE];
static int lpos[LBSIZE];
static int lindex;
static int lcount;
static int lposition;
static long rseed = 1L;

static void xinfo(void);
static void xflush(void);
static int xgetc(void);
static void xputc(int ch);
static int xcheck(void);

/* main - the main routine */
#ifdef LINK_KERNEL
void first_task(int stacd)
#else /* LINK_KERNEL */
void main(int stacd)
#endif /* LINK_KERNEL */
{
	static char *empty_argv[] = { "xscheme", 0 };

	malloc_initialize(0x10000);
	setbuf(stdin, NULL);
	ioctl(0, ECHO, 0);
	ioctl(0, INPUT, RAW);
	xlmain(1, empty_argv);
	exit(1);
}

/* osinit - initialize */
void osinit(char *banner)
{
	ostputs(banner);
	ostputc('\n');
	lposition = 0;
	lindex = 0;
	lcount = 0;
}

/* osfinish - clean up before returning to the operating system */
void osfinish(void)
{
}

/* oserror - print an error message */
void oserror(char *msg)
{
	ostputs("error: ");
	ostputs(msg);
	ostputc('\n');
}

/* osrand - return a random number between 0 and n-1 */
int osrand(int n)
{
	long k1;

	/* make sure we don't get stuck at zero */
	if (rseed == 0L) rseed = 1L;

	/* algorithm taken from Dr. Dobbs Journal, November 1985, page 91 */
	k1 = rseed / 127773L;
	if ((rseed = 16807L * (rseed - k1 * 127773L) - k1 * 2836L) < 0L)
		rseed += 2147483647L;

	/* return a random number between 0 and n-1 */
	return ((int)(rseed % (long)n));
}

/* osaopen - open an ascii file */
FILE *osaopen(char *name, char *mode)
{
	fprintf(stderr, "called: fopen(\"%s\", \"%s\")\n", name, mode);
	return(0);
/*	return(fopen(name,mode));*/
}

/* osbopen - open a binary file */
FILE *osbopen(char *name, char *mode)
{
	char bmode[10];
	strcpy(bmode,mode); strcat(bmode,"b");
	fprintf(stderr, "called: fopen(\"%s\", \"%s\")\n", name, mode);
	return(0);
/*	return (fopen(name,bmode));*/
}

/* osclose - close a file */
int osclose(FILE *fp)
{
	fprintf(stderr, "called: fclose\n");
	return(-1);
/*	return (fclose(fp));*/
}

/* ostell - get the current file position */
long ostell(FILE *fp)
{
	fprintf(stderr, "called: ftell\n");
	return(-1);
/*	return(ftell(fp));*/
}

/* osseek - set the current file position */
int osseek(FILE *fp, long offset, int whence)
{
	fprintf(stderr, "called: fseek\n");
	return(-1);
/*	return(fseek(fp,offset,whence));*/
}

/* osagetc - get a character from an ascii file */
int osagetc(FILE *fp)
{
	return(getc(fp));
}

/* osaputc - put a character to an ascii file */
int osaputc(int ch, FILE *fp)
{
	return(putc(ch, fp));
}

/* osbgetc - get a character from a binary file */
int osbgetc(FILE *fp)
{
	return(getc(fp));
}

/* osbputc - put a character to a binary file */
int osbputc(int ch, FILE *fp)
{
	return(putc(ch,fp));
}

/* ostgetc - get a character from the terminal */
int ostgetc(void)
{
	int ch;

	/* check for a buffered character */
	if (lcount--)
		return (lbuf[lindex++]);

	/* get an input line */
	for (lcount = 0; ; )
		switch (ch = xgetc()) {
		case '\r':
		case '\n':
			lbuf[lcount++] = '\n';
			xputc('\n'); lposition = 0;
			if (tfp)
			    for (lindex = 0; lindex < lcount; ++lindex)
				osaputc(lbuf[lindex],tfp);
			lindex = 0; lcount--;
			return (lbuf[lindex++]);
		case '\010':
		case '\177':
			if (lcount) {
			    lcount--;
			    while (lposition > lpos[lcount]) {
				xputc('\010'); xputc(' '); xputc('\010');
				lposition--;
			    }
			}
			break;
		case '\004':
			xlfatal("goodbye");
		default:
			if (ch == '\t' || (ch >= 0x20 && ch < 0x7F)) {
			    lbuf[lcount] = ch;
			    lpos[lcount] = lposition;
			    if (ch == '\t')
				do {
				    xputc(' ');
				} while (++lposition & 7);
			else {
				xputc(ch); lposition++;
			}
			lcount++;
		}
		else {
		    xflush();
		    switch (ch) {
		    case '\003':	xltoplevel();	/* control-c */
		    case '\007':	xlcleanup();	/* control-g */
		    case '\020':	xlcontinue();	/* control-p */
		    case '\032':	return (EOF);	/* control-z */
		    case '\034':	xlwrapup();	/* control-\ */
		    default:		/* return (ch)      ignore */ ;
		    }
		}
	}
}

/* ostputc - put a character to the terminal */
void ostputc(int ch)
{
	/* check for control characters */
	oscheck();

	/* output the character */
	if (ch == '\n') {
		xputc('\r'); xputc('\n');
		lposition = 0;
	}
	else {
		xputc(ch);
		lposition++;
	}

	/* output the character to the transcript file */
	if (tfp) {
		osaputc(ch,tfp);
	}
}

/* ostputs - output a string to the terminal */
void ostputs(char *str)
{
	while (*str != '\0') {
		ostputc(*str++);
	}
}

/* osflush - flush the terminal input buffer */
void osflush()
{
	lindex = lcount = lposition = 0;
}

/* oscheck - check for control characters during execution */
void oscheck()
{
	switch (xcheck()) {
	case '\002':	/* control-b */
		xflush();
		xlbreak();
		break;
	case '\003':	/* control-c */
		xflush();
		xltoplevel();
		break;
	case '\024':	/* control-t */
		xinfo();
		break;
	case '\023':	/* control-s */
		while (xcheck() != '\021') ;
		break;
	case '\034':	/* control-\ */
		xlwrapup();
		break;
	}
}

/* xinfo - show information on control-t */
static void xinfo()
{
	extern int nfree,gccalls;
	extern long total;
	char buf[80];

	sprintf(buf,"\n[ Free: %d, GC calls: %d, Total: %ld ]",
		nfree,gccalls,total);
	ostputs(buf);
}

/* xflush - flush the input line buffer and start a new line */
static void xflush(void)
{
	osflush();
	ostputc('\n');
}

/* xgetc - get a character from the terminal without echo */
static int xgetc(void)
{
	return(getchar());
}

/* xputc - put a character to the terminal */
static void xputc(int ch)
{
	if (ch!='\r') {
		putchar(ch);
	}
}

/* xcheck - check for a character */
static int xcheck(void)
{
	return(0);
}

/* ossymbols - enter os specific symbols */
void ossymbols(void)
{
}

#define SCHEME_MPL	(-5)

malloc_initialize(int size)
{
	T_CMPL	pk_cmpl;

	pk_cmpl.exinf = 0;
	pk_cmpl.mplatr = TA_HFIT|TA_TFIFO;
	pk_cmpl.mplsz = size;
	syscall(cre_mpl(SCHEME_MPL, &pk_cmpl));
}

void *
malloc(int size)
{
	void	*x;

	syscall(pget_blk(&x, SCHEME_MPL, size));
	return(x);
}

void *
calloc(int nelem, int elsize)
{
	void	*x;
	int	size = nelem*elsize;

	x = malloc(size);
	bzero(x, size);
	return(x);
}

void
free(void *x)
{
	syscall(rel_blk(SCHEME_MPL, x));
}
