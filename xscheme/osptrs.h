/* osptrs.h - table entries for machine specific functions */

#ifdef MACINTOSH
{	"HIDEPEN",				xhidepen	},
{	"SHOWPEN",				xshowpen	},
{	"GETPEN",				xgetpen		},
{	"PENSIZE",				xpensize	},
{	"PENMODE",				xpenmode	},
{	"PENPAT",				xpenpat		},
{	"PENNORMAL",				xpennormal	},
{	"MOVETO",				xmoveto		},
{	"MOVE",					xmove		},
{	"LINETO",				xlineto		},
{	"LINE",					xline		},
{	"SHOW-GRAPHICS",			xshowgraphics	},
{	"HIDE-GRAPHICS",			xhidegraphics	},
{	"CLEAR-GRAPHICS",			xcleargraphics	},
#endif

#ifdef MSDOS
{	"SYSTEM",				xsystem		},
{	"GET-KEY",				xgetkey		},
{	"TIME",					xtime		},
{	"DIFFTIME",				xdifftime	},
#ifdef NOTDEF
{	"INT86",				xint86		},
{	"INBYTE",				xinbyte		},
{	"OUTBYTE",				xoutbyte	},
#endif
#endif

#ifdef UNIX
{	"SYSTEM",				xsystem		},
#endif

#ifdef ITIS
{	"CRE_TSK",				x_cre_tsk	},
{	"STA_TSK",				x_sta_tsk	},
{	"DEL_TSK",				x_del_tsk	},
/* {	"EXT_TSK",				x_ext_tsk	}, */
/* {	"EXD_TSK",				x_exd_tsk	}, */
{	"TER_TSK",				x_ter_tsk	},
{	"DIS_DSP",				x_dis_dsp	},
{	"ENA_DSP",				x_ena_dsp	},
{	"CHG_PRI",				x_chg_pri	},
{	"ROT_RDQ",				x_rot_rdq	},
{	"REL_WAI",				x_rel_wai	},
{	"GET_TID",				x_get_tid	},
{	"REF_TSK",				x_ref_tsk	},

{	"SUS_TSK",				x_sus_tsk	},
{	"RSM_TSK",				x_rsm_tsk	},
{	"FRSM_TSK",				x_frsm_tsk	},
{	"SLP_TSK",				x_slp_tsk	},
{	"TSLP_TSK",				x_tslp_tsk	},
{	"WUP_TSK",				x_wup_tsk	},
{	"CAN_WUP",				x_can_wup	},

{	"CRE_SEM",				x_cre_sem	},
{	"DEL_SEM",				x_del_sem	},
{	"SIG_SEM",				x_sig_sem	},
{	"WAI_SEM",				x_wai_sem	},
{	"PREQ_SEM",				x_preq_sem	},
{	"TWAI_SEM",				x_twai_sem	},
{	"REF_SEM",				x_ref_sem	},
{	"CRE_FLG",				x_cre_flg	},
{	"DEL_FLG",				x_del_flg	},
{	"SET_FLG",				x_set_flg	},
{	"CLR_FLG",				x_clr_flg	},
{	"WAI_FLG",				x_wai_flg	},
{	"POL_FLG",				x_pol_flg	},
{	"TWAI_FLG",				x_twai_flg	},
{	"REF_FLG",				x_ref_flg	},

{	"CRE_MBF",				x_cre_mbf	},
{	"DEL_MBF",				x_del_mbf	},
{	"SND_MBF",				x_snd_mbf	},
{	"PSND_MBF",				x_psnd_mbf	},
{	"TSND_MBF",				x_tsnd_mbf	},
{	"RCV_MBF",				x_rcv_mbf	},
{	"PRCV_MBF",				x_prcv_mbf	},
{	"TRCV_MBF",				x_trcv_mbf	},
{	"REF_MBF",				x_ref_mbf	},

{	"REF_MPL",				x_ref_mpl	},
{	"REF_MPF",				x_ref_mpf	},

{	"SET_TIM",				x_set_tim	},
{	"GET_TIM",				x_get_tim	},
{	"DLY_TSK",				x_dly_tsk	},
{	"DEF_CYC",				x_def_cyc	},
{	"ACT_CYC",				x_act_cyc	},
{	"REF_CYC",				x_ref_cyc	},
{	"DEF_ALM",				x_def_alm	},
{	"REF_ALM",				x_ref_alm	},

{	"STRING_SND_MBF",			x_string_snd_mbf },

{	"READ_MEMORY",				x_read_mem_w	},
{	"READ_MEMORY_WORD",			x_read_mem_w	},
{	"READ_MEMORY_HALF",			x_read_mem_h	},
{	"READ_MEMORY_BYTE",			x_read_mem_b	},
{	"WRITE_MEMORY",				x_write_mem_w	},
{	"WRITE_MEMORY_WORD",			x_write_mem_w	},
{	"WRITE_MEMORY_HALF",			x_write_mem_h	},
{	"WRITE_MEMORY_BYTE",			x_write_mem_b	},

#ifdef USE_AUTO_ID
{	"VCRE_TSK",				x_vcre_tsk	},
{	"VCRE_SEM",				x_vcre_sem	},
{	"VDEF_CYC",				x_vdef_cyc	},
{	"VDEF_ALM",				x_vdef_alm	},
#endif /* USE_AUTO_ID */
#endif /* ITIS */
