/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: logtask.c,v 1.1 1993/10/15 10:16:05 hiro Exp $
 */

#include <stdarg.h>
#include <itis_services.h>
#include <itis_resources.h>
#include <itis_syslog.h>

/*
 *  シリアルインタフェースドライバの関数
 *
 *  シリアルインタフェースドライバとシステムログタスクがリンクされるこ
 *  とを利用して，直接呼び出す．
 */
extern int	serial_init(int portid);
extern int	serial_write(int portid, char *buf, unsigned int len);

/*
 *  システムログタスク
 */

int	logtask_alive = 0;	/* ログタスクが動いているか */
int	log_msg_maxmsz;		/* ログメッセージの最大長 */

/*
 *  前方参照宣言
 */
ER	svc_syslog_send(const char *string, int len);

/*
 *  ログタスクの起動と初期化，拡張SVCの定義
 */

#define DSVC(handler)	&((T_DSVC) { TA_HLNG, (FP)(handler) })

void
logtask_startup(int port)
{
	syscall(serial_init(port));
	syscall(cre_mbf(TMBF_OS, &((T_CMBF) MBF_LOG_CMBF)));
	syscall(cre_tsk(TSK_LOG, &((T_CTSK) TSK_LOG_CTSK)));
	syscall(sta_tsk(TSK_LOG, port));
	log_msg_maxmsz = MBF_LOG_MAXMSZ;
	logtask_alive = 1;

	syscall(def_svc(SVC_SYSLOG_SEND, DSVC(svc_syslog_send)));

	syslog(LOG_NOTICE, "System logging task started on port %d.", port);
}

/*
 *  ログタスクの本体
 */

static int	logtask_port;			/* ログ出力ポート番号 */
static char	logtask_buf[MBF_LOG_MAXMSZ+1];	/* ログタスク用バッファ */

void
log_task(int port)
{
	INT		msgsz;

	logtask_port = port;
	while ((itron_errno = rcv_mbf(logtask_buf, &msgsz, TMBF_OS)) == E_OK) {
		logtask_buf[msgsz++] = '\n';
		serial_write(logtask_port, logtask_buf, msgsz);
	}
	logtask_alive = 0;
	perror("logtask");
	ext_tsk();
}

/*
 *  拡張SVCハンドラ本体
 *
 *  ログタスクが動いている場合は，ログメッセージバッファへ送る．動いて
 *  いない場合は，直接低レベルの文字出力ルーチンを使って出力する．
 */

ER
svc_syslog_send(const char *string, int len)
{
	if (len > 0) {
		if (logtask_alive) {
			if (len > log_msg_maxmsz) {
				len = log_msg_maxmsz;
			}
			if (psnd_mbf(TMBF_OS, (VP) string, len) != E_OK) {
				logtask_alive = 0;
			}
		}
		if (!logtask_alive) {
			target_write(string, len);
			target_write("\n", 1);
		}
	}
	return(E_OK);
}
