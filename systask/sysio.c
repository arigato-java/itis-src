/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: sysio.c,v 1.2 1993/11/01 14:48:03 hiro Exp $
 */

/*
 *  カーネルと一緒にリンクするタスク用の低レベル入出力ルーチン
 *
 *  このモジュールは，用いるライブラリに依存している．
 */

#include <itis_services.h>
#include <itis_resources.h>

int
__console_read(void *dummy, char *buf, unsigned int len)
{
	return(serial_read(0, buf, len));
}

int
__console_write(void *dummy, char *buf, unsigned int len)
{
	return(serial_write(0, buf, len));
}

int
__console_ioctl(void *dummy, int req, int arg)
{
	return(serial_ioctl(0, req, arg));
}

void
__exit(int status)
{
	wup_tsk(TSK_INIT);
}
