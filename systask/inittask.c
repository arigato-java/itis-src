/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: inittask.c,v 1.1 1993/10/15 10:16:00 hiro Exp $
 */

#include <itis_services.h>
#include <itis_resources.h>
#include <itis_syslog.h>

/*
 *  システムタスク/ドライバの起動ルーチン
 */
extern void	serial_startup(int port);
extern void	logtask_startup(int port);

#define	CONSOLE_PORT	1	/* コンソール用に用いるシリアルポート番号 */
#define	LOGTASK_PORT	1	/* システムログを出力するシリアルポート番号 */

/*
 *  初期化タスク
 *
 *  必要なシステムタスクおよび初期起動タスクを起動する．具体的には，
 *   (a) シリアルインタフェースドライバを起動する．
 *   (b) システムログタスクを起動する．
 *   (c) 初期起動タスクを起動する．
 *  以上の処理を終えると，slp_tsk により待ち状態に入る．システム全体を
 *  安全に停止させる場合には，初期化タスクを wup_tsk で起床させる．
 */
void
init_task(INT stacd)
{
	syslog(LOG_NOTICE, "init: I'm now starting up some tasks.....");

	/*
	 *  シリアルインタフェースドライバの起動
	 */
	serial_startup(CONSOLE_PORT);

	/*
	 *  システムログタスクの起動
	 */
	logtask_startup(LOGTASK_PORT);

	/*
	 *  初期起動タスクの起動
	 */
	syscall(cre_tsk(TSK_FIRST, &((T_CTSK) TSK_FIRST_CTSK)));
	syscall(sta_tsk(TSK_FIRST, 0));

	syslog(LOG_NOTICE, "init: initialize OK.");
	syscall(slp_tsk());

	/*
	 *  システム停止処理
	 */
	syslog(LOG_NOTICE, "init: ItIs kernel shutdown.");
	serial_shutdown(CONSOLE_PORT);
	itis_exit();
}
