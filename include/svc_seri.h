/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: svc_serial.h,v 1.2 1993/10/20 09:09:45 hiro Exp $
 */

#ifndef	_SVC_SERIAL_
#define	_SVC_SERIAL_

#include <itis_stddefs.h>
#include <itis_resources.h>

#define CONSOLE_PORT	0	/* コンソールの I/Oポートの指定 */

Inline int
serial_read(int portid, char *buf, unsigned int len)
{
	REGISTER(r0) = (INT) portid;
	REGISTER(r1) = (INT) buf;
	REGISTER(r2) = (INT) len;
	REGISTER(r6) = SVC_SERIAL_READ;

	Asm("trapa #1"
	: "g="(r0)
	: "0"(r0), "g"(r1), "g"(r2), "g"(r6)
	: "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline int
serial_write(int portid, char *buf, unsigned int len)
{
	REGISTER(r0) = (INT) portid;
	REGISTER(r1) = (INT) buf;
	REGISTER(r2) = (INT) len;
	REGISTER(r6) = SVC_SERIAL_WRITE;

	Asm("trapa #1"
	: "g="(r0)
	: "0"(r0), "g"(r1), "g"(r2), "g"(r6)
	: "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline int
serial_ioctl(int portid, int req, int arg)
{
	REGISTER(r0) = (INT) portid;
	REGISTER(r1) = (INT) req;
	REGISTER(r2) = (INT) arg;
	REGISTER(r6) = SVC_SERIAL_IOCTL;

	Asm("trapa #1"
	: "g="(r0)
	: "0"(r0), "g"(r1), "g"(r2), "g"(r6)
	: "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

#endif	/* _SVC_SERIAL_ */
