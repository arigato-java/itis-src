/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itis_syslog.h,v 1.1 1993/10/15 10:27:08 hiro Exp $
 */

#ifndef	_ITIS_SYSLOG_
#define	_ITIS_SYSLOG_

/* 
 *  システムログ出力ライブラリの定義
 * 
 *  システムログへメッセージを出力するための関数の定義．UNIX のインタフェー
 *  スと近いインタフェースにしてあるが，今のところ facility は使っていない．
 *
 *  カーネル用の setlogmask は用意していない (作るのは簡単)．
 */

extern void	syslog(int priority, const char *format, ...);
extern int	setlogmask(int maskpri);

/*
 *  facility の定義
 */
#define LOG_KERN	(0 << 3)	/* カーネル */
#define LOG_NKERN	(1 << 3)	/* 外核 */
#define LOG_INDP	(2 << 3)	/* タスク独立部 */
#define LOG_EXTSVC	(3 << 3)	/* 拡張SVCハンドラ */
#define LOG_STASK	(4 << 3)	/* システムタスク */
#define LOG_UTASK	(5 << 3)	/* ユーザタスク */

#define LOG_NFACILITIES	16		/* facility の数 */
#define LOG_FACMASK	0x0078		/* facility を取り出す時のマスク値 */

/*
 *  priority の定義
 */
#define LOG_EMERG	0		/* システムダウンに値するエラー */
#define LOG_ALERT	1
#define LOG_CRIT	2
#define LOG_ERR		3		/* システムエラー */
#define LOG_WARNING	4
#define LOG_NOTICE	5
#define LOG_INFO	6
#define LOG_DEBUG	7		/* デバッグ用メッセージ */

#define LOG_PRIMASK	0x0007		/* priority を取り出すマスク値 */

/*
 *  setlogmask の引数を作るためのマクロ
 */
#define LOG_MASK(pri)	(1 << (pri))
#define LOG_UPTO(pri)	((1 << ((pri) + 1)) - 1)

#endif /* _ITIS_SYSLOG_ */
