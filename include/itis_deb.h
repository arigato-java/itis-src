/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itis_debug.h,v 1.2 1993/11/18 14:30:44 hiro Exp $
 */

#ifndef _ITIS_DEBUG_
#define _ITIS_DEBUG_

/*
 *  デバックサポート機能関係の定義
 */

typedef struct {
	VW	r0;		/* R0 */
	VW      r1;		/* R1 */
	VW      r2;		/* R2 */
	VW      r3;		/* R3 */
	VW      r4;		/* R4 */
	VW      r5;		/* R5 */
	VW      r6;		/* R6 */
	VW      r7;		/* R7 */
	VW      r8;		/* R8 */
	VW      r9;		/* R9 */
	VW      r10;            /* R10 */
	VW      r11;            /* R11 */
	VW      r12;            /* R12 */
	VW      r13;            /* R13 */
	VW      r14;            /* R14 (フレームポインタ) */
	VW      sp;		/* スタックポインタ */
} T_REGS;

typedef	struct {
	VW	pc;		/* PC (プログラムカウンタ) */
	VW	psw;		/* PSW */
} T_EIT;

typedef struct t_cregs {
	VW	csw;		/* CSW */
	VW	sp0;		/* SP0 (リング0 のスタックポインタ) */
	VW	sp1;		/* SP1 (リング1 のスタックポインタ) */
	VW	sp2;		/* SP2 (リング2 のスタックポインタ) */
	VW	sp3;		/* SP3 (リング3 のスタックポインタ) */
	VW	uatb;		/* UATB */
	VW	lsid;		/* LSID */
} T_CREGS;

#endif /* _ITIS_DEBUG_ */
