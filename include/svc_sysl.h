/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: svc_syslog.h,v 1.1 1993/10/15 10:27:20 hiro Exp $
 */

#ifndef	_SVC_SYSLOG_
#define	_SVC_SYSLOG_

#include <itis_stddefs.h>
#include <itis_resources.h>

Inline int
syslog_send(const char *string, int len)
{
	REGISTER(r0) = (INT) string;
	REGISTER(r1) = (INT) len;
	REGISTER(r6) = SVC_SYSLOG_SEND;

	Asm("trapa #1"
	: "g="(r0)
	: "0"(r0), "g"(r1), "g"(r6)
	: "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

#endif /* _SVC_SYSLOG_ */
