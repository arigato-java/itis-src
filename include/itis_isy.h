/*  This file is generated from syscall.def by genisyscall.  */

#ifndef _ITIS_ISYSCALL_
#define _ITIS_ISYSCALL_

#ifdef LINK_KERNEL

#define unl_cpu i_unl_cpu
#define loc_cpu i_loc_cpu
#define def_svc i_def_svc
#define ref_sys i_ref_sys
#define ref_cfg i_ref_cfg
#define get_ver i_get_ver
#define cre_tsk i_cre_tsk
#define del_tsk i_del_tsk
#define ref_tsk i_ref_tsk
#define ext_tsk i_ext_tsk
#define exd_tsk i_exd_tsk
#define sta_tsk i_sta_tsk
#define get_tid i_get_tid
#define ter_tsk i_ter_tsk
#define chg_pri i_chg_pri
#define rot_rdq i_rot_rdq
#define ena_dsp i_ena_dsp
#define dis_dsp i_dis_dsp
#define rel_wai i_rel_wai
#define sus_tsk i_sus_tsk
#define rsm_tsk i_rsm_tsk
#define frsm_tsk i_frsm_tsk
#define tslp_tsk i_tslp_tsk
#define slp_tsk i_slp_tsk
#define wup_tsk i_wup_tsk
#define can_wup i_can_wup
#define cre_flg i_cre_flg
#define del_flg i_del_flg
#define ref_flg i_ref_flg
#define wai_flg i_wai_flg
#define clr_flg i_clr_flg
#define set_flg i_set_flg
#define cre_sem i_cre_sem
#define del_sem i_del_sem
#define ref_sem i_ref_sem
#define wai_sem i_wai_sem
#define sig_sem i_sig_sem
#define cre_mbx i_cre_mbx
#define del_mbx i_del_mbx
#define ref_mbx i_ref_mbx
#define rcv_msg i_rcv_msg
#define snd_msg i_snd_msg
#define def_int i_def_int
#define chg_ims i_chg_ims
#define ref_ims i_ref_ims
#define cre_mpf i_cre_mpf
#define del_mpf i_del_mpf
#define ref_mpf i_ref_mpf
#define get_blf i_get_blf
#define rel_blf i_rel_blf
#define vget_tim i_vget_tim
#define set_tim i_set_tim
#define get_tim i_get_tim
#define dly_tsk i_dly_tsk
#define def_alm i_def_alm
#define def_cyc i_def_cyc
#define ref_alm i_ref_alm
#define ref_cyc i_ref_cyc
#define act_cyc i_act_cyc
#define pget_blf i_pget_blf
#define pget_blk i_pget_blk
#define pol_flg i_pol_flg
#define preq_sem i_preq_sem
#define prcv_msg i_prcv_msg
#define psnd_mbf i_psnd_mbf
#define prcv_mbf i_prcv_mbf
#define pcal_por i_pcal_por
#define pacp_por i_pacp_por
#define cre_mpl i_cre_mpl
#define del_mpl i_del_mpl
#define ref_mpl i_ref_mpl
#define get_blk i_get_blk
#define rel_blk i_rel_blk
#define cre_por i_cre_por
#define del_por i_del_por
#define ref_por i_ref_por
#define cal_por i_cal_por
#define acp_por i_acp_por
#define rpl_rdv i_rpl_rdv
#define fwd_por i_fwd_por
#define cre_mbf i_cre_mbf
#define del_mbf i_del_mbf
#define ref_mbf i_ref_mbf
#define snd_mbf i_snd_mbf
#define rcv_mbf i_rcv_mbf
#define tget_blf i_tget_blf
#define tget_blk i_tget_blk
#define twai_flg i_twai_flg
#define twai_sem i_twai_sem
#define trcv_msg i_trcv_msg
#define tsnd_mbf i_tsnd_mbf
#define trcv_mbf i_trcv_mbf
#define tcal_por i_tcal_por
#define tacp_por i_tacp_por
#define vset_reg i_vset_reg
#define vget_reg i_vget_reg
#define vcre_tsk i_vcre_tsk
#define vcre_sem i_vcre_sem
#define vcre_flg i_vcre_flg
#define vcre_mbx i_vcre_mbx
#define vcre_mbf i_vcre_mbf
#define vcre_por i_vcre_por
#define vcre_mpl i_vcre_mpl
#define vcre_mpf i_vcre_mpf
#define vdef_cyc i_vdef_cyc
#define vdef_alm i_vdef_alm

extern ER unl_cpu(void);
extern ER loc_cpu(void);
extern ER def_svc(FN s_fncd, T_DSVC *pk_dsvc);
extern ER ref_sys(T_RSYS *pk_rsys);
extern ER ref_cfg(T_RCFG *pk_rcfg);
extern ER get_ver(T_VER *pk_ver);
extern ER cre_tsk(ID tskid, T_CTSK *pk_ctsk);
extern ER del_tsk(ID tskid);
extern ER ref_tsk(T_RTSK *pk_rtsk, ID tskid);
extern void ext_tsk(void);
extern void exd_tsk(void);
extern ER sta_tsk(ID tskid, INT stacd);
extern ER get_tid(ID *p_tskid);
extern ER ter_tsk(ID tskid);
extern ER chg_pri(ID tskid, PRI tskpri);
extern ER rot_rdq(PRI tskpri);
extern ER ena_dsp(void);
extern ER dis_dsp(void);
extern ER rel_wai(ID tskid);
extern ER sus_tsk(ID tskid);
extern ER rsm_tsk(ID tskid);
extern ER frsm_tsk(ID tskid);
extern ER tslp_tsk(TMO tmout);
extern ER slp_tsk(void);
extern ER wup_tsk(ID tskid);
extern ER can_wup(INT *p_wupcnt, ID tskid);
extern ER cre_flg(ID flgid, T_CFLG *pk_cflg);
extern ER del_flg(ID flgid);
extern ER ref_flg(T_RFLG *pk_rflg, ID flgid);
extern ER wai_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode);
extern ER clr_flg(ID flgid, UINT clrptn);
extern ER set_flg(ID flgid, UINT setptn);
extern ER cre_sem(ID semid, T_CSEM *pk_csem);
extern ER del_sem(ID semid);
extern ER ref_sem(T_RSEM *pk_rsem, ID semid);
extern ER wai_sem(ID semid);
extern ER sig_sem(ID semid);
extern ER cre_mbx(ID mbxid, T_CMBX *pk_cmbx);
extern ER del_mbx(ID mbxid);
extern ER ref_mbx(T_RMBX *pk_rmbx, ID mbxid);
extern ER rcv_msg(T_MSG* *ppk_msg, ID mbxid);
extern ER snd_msg(ID mbxid, T_MSG *pk_msg);
extern ER def_int(UINT dintno, T_DINT *pk_dint);
extern ER chg_ims(UINT imask);
extern ER ref_ims(UINT *p_imask);
extern ER cre_mpf(ID mpfid, T_CMPF *pk_cmpf);
extern ER del_mpf(ID mpfid);
extern ER ref_mpf(T_RMPF *pk_rmpf, ID mpfid);
extern ER get_blf(VP *p_blf, ID mpfid);
extern ER rel_blf(ID mpfid, VP blf);
extern ER vget_tim(SYSUTIME *pk_utim);
extern ER set_tim(SYSTIME *pk_tim);
extern ER get_tim(SYSTIME *pk_tim);
extern ER dly_tsk(DLYTIME dlytim);
extern ER def_alm(HNO almno, T_DALM *pk_dalm);
extern ER def_cyc(HNO cycno, T_DCYC *pk_dcyc);
extern ER ref_alm(T_RALM *pk_ralm, HNO almno);
extern ER ref_cyc(T_RCYC *pk_rcyc, HNO cycno);
extern ER act_cyc(HNO cycno, UINT cycact);
extern ER pget_blf(VP *p_blf, ID mpfid);
extern ER pget_blk(VP *p_blk, ID mplid, INT blksz);
extern ER pol_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode);
extern ER preq_sem(ID semid);
extern ER prcv_msg(T_MSG* *ppk_msg, ID mbxid);
extern ER psnd_mbf(ID mbfid, VP msg, INT msgsz);
extern ER prcv_mbf(VP msg, INT *p_msgsz, ID mbfid);
extern ER pcal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz);
extern ER pacp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn);
extern ER cre_mpl(ID mplid, T_CMPL *pk_cmpl);
extern ER del_mpl(ID mplid);
extern ER ref_mpl(T_RMPL *pk_rmpl, ID mplid);
extern ER get_blk(VP *p_blk, ID mplid, INT blksz);
extern ER rel_blk(ID mplid, VP blk);
extern ER cre_por(ID porid, T_CPOR *pk_cpor);
extern ER del_por(ID porid);
extern ER ref_por(T_RPOR *pk_rpor, ID porid);
extern ER cal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz);
extern ER acp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn);
extern ER rpl_rdv(RNO rdvno, VP msg, INT rmsgsz);
extern ER fwd_por(ID porid, UINT calptn, RNO rdvno, VP msg, INT cmsgsz);
extern ER cre_mbf(ID mbfid, T_CMBF *pk_cmbf);
extern ER del_mbf(ID mbfid);
extern ER ref_mbf(T_RMBF *pk_rmbf, ID mbfid);
extern ER snd_mbf(ID mbfid, VP msg, INT msgsz);
extern ER rcv_mbf(VP msg, INT *p_msgsz, ID mbfid);
extern ER tget_blf(VP *p_blf, ID mpfid, TMO tmout);
extern ER tget_blk(VP *p_blk, ID mplid, INT blksz, TMO tmout);
extern ER twai_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode, TMO tmout);
extern ER twai_sem(ID semid, TMO tmout);
extern ER trcv_msg(T_MSG* *ppk_msg, ID mbxid, TMO tmout);
extern ER tsnd_mbf(ID mbfid, VP msg, INT msgsz, TMO tmout);
extern ER trcv_mbf(VP msg, INT *p_msgsz, ID mbfid, TMO tmout);
extern ER tcal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz, TMO tmout);
extern ER tacp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn, TMO tmout);
extern ER vset_reg(ID tskid, T_REGS *pk_regs, T_EIT *pk_eit, T_CREGS *pk_cregs);
extern ER vget_reg(T_REGS *pk_regs, T_EIT *pk_eit, T_CREGS *pk_cregs, ID tskid);
extern ER vcre_tsk(T_CTSK *pk_ctsk);
extern ER vcre_sem(T_CSEM *pk_csem);
extern ER vcre_flg(T_CFLG *pk_cflg);
extern ER vcre_mbx(T_CMBX *pk_cmbx);
extern ER vcre_mbf(T_CMBF *pk_cmbf);
extern ER vcre_por(T_CPOR *pk_cpor);
extern ER vcre_mpl(T_CMPL *pk_cmpl);
extern ER vcre_mpf(T_CMPF *pk_cmpf);
extern ER vdef_cyc(T_DCYC *pk_dcyc);
extern ER vdef_alm(T_DALM *pk_dalm);

#else /* LINK_KERNEL */

Inline ER
sys_cal(FN fncd, INT par1, INT par2, INT par3, INT par4, INT par5, INT par6)
{
	REGISTER(r0) = (INT) par1;
	REGISTER(r1) = (INT) par2;
	REGISTER(r2) = (INT) par3;
	REGISTER(r3) = (INT) par4;
	REGISTER(r4) = (INT) par5;
	REGISTER(r5) = (INT) par6;
	REGISTER(r6) = fncd;

	Asm("trapa #1"
	  : "g="(r0)
	  : "0"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r5), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
unl_cpu(void)
{
	REGISTER(r0);
	REGISTER(r6) = TFN_UNL_CPU;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
loc_cpu(void)
{
	REGISTER(r0);
	REGISTER(r6) = TFN_LOC_CPU;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
def_svc(FN s_fncd, T_DSVC *pk_dsvc)
{
	REGISTER(r0) = (INT) s_fncd;
	REGISTER(r1) = (INT) pk_dsvc;
	REGISTER(r6) = TFN_DEF_SVC;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_sys(T_RSYS *pk_rsys)
{
	REGISTER(r0) = (INT) pk_rsys;
	REGISTER(r6) = TFN_REF_SYS;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_cfg(T_RCFG *pk_rcfg)
{
	REGISTER(r0) = (INT) pk_rcfg;
	REGISTER(r6) = TFN_REF_CFG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
get_ver(T_VER *pk_ver)
{
	REGISTER(r0) = (INT) pk_ver;
	REGISTER(r6) = TFN_GET_VER;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_tsk(ID tskid, T_CTSK *pk_ctsk)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r1) = (INT) pk_ctsk;
	REGISTER(r6) = TFN_CRE_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_DEL_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_tsk(T_RTSK *pk_rtsk, ID tskid)
{
	REGISTER(r0) = (INT) pk_rtsk;
	REGISTER(r1) = (INT) tskid;
	REGISTER(r6) = TFN_REF_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline void
ext_tsk(void)
{
	REGISTER(r6) = TFN_EXT_TSK;

	Asm("trapa #1"
	  : /* no output */
	  : "g"(r6)
	  : "r0", "r1", "r2", "r3", "r4", "r5", "r6");
}

Inline void
exd_tsk(void)
{
	REGISTER(r6) = TFN_EXD_TSK;

	Asm("trapa #1"
	  : /* no output */
	  : "g"(r6)
	  : "r0", "r1", "r2", "r3", "r4", "r5", "r6");
}

Inline ER
sta_tsk(ID tskid, INT stacd)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r1) = (INT) stacd;
	REGISTER(r6) = TFN_STA_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
get_tid(ID *p_tskid)
{
	REGISTER(r0) = (INT) p_tskid;
	REGISTER(r6) = TFN_GET_TID;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ter_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_TER_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
chg_pri(ID tskid, PRI tskpri)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r1) = (INT) tskpri;
	REGISTER(r6) = TFN_CHG_PRI;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rot_rdq(PRI tskpri)
{
	REGISTER(r0) = (INT) tskpri;
	REGISTER(r6) = TFN_ROT_RDQ;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ena_dsp(void)
{
	REGISTER(r0);
	REGISTER(r6) = TFN_ENA_DSP;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
dis_dsp(void)
{
	REGISTER(r0);
	REGISTER(r6) = TFN_DIS_DSP;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rel_wai(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_REL_WAI;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
sus_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_SUS_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rsm_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_RSM_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
frsm_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_FRSM_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tslp_tsk(TMO tmout)
{
	REGISTER(r0) = (INT) tmout;
	REGISTER(r6) = TFN_TSLP_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
slp_tsk(void)
{
	REGISTER(r0);
	REGISTER(r6) = TFN_SLP_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
wup_tsk(ID tskid)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r6) = TFN_WUP_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
can_wup(INT *p_wupcnt, ID tskid)
{
	REGISTER(r0) = (INT) p_wupcnt;
	REGISTER(r1) = (INT) tskid;
	REGISTER(r6) = TFN_CAN_WUP;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_flg(ID flgid, T_CFLG *pk_cflg)
{
	REGISTER(r0) = (INT) flgid;
	REGISTER(r1) = (INT) pk_cflg;
	REGISTER(r6) = TFN_CRE_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_flg(ID flgid)
{
	REGISTER(r0) = (INT) flgid;
	REGISTER(r6) = TFN_DEL_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_flg(T_RFLG *pk_rflg, ID flgid)
{
	REGISTER(r0) = (INT) pk_rflg;
	REGISTER(r1) = (INT) flgid;
	REGISTER(r6) = TFN_REF_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
wai_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode)
{
	REGISTER(r0) = (INT) p_flgptn;
	REGISTER(r1) = (INT) flgid;
	REGISTER(r2) = (INT) waiptn;
	REGISTER(r3) = (INT) wfmode;
	REGISTER(r6) = TFN_WAI_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
clr_flg(ID flgid, UINT clrptn)
{
	REGISTER(r0) = (INT) flgid;
	REGISTER(r1) = (INT) clrptn;
	REGISTER(r6) = TFN_CLR_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
set_flg(ID flgid, UINT setptn)
{
	REGISTER(r0) = (INT) flgid;
	REGISTER(r1) = (INT) setptn;
	REGISTER(r6) = TFN_SET_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_sem(ID semid, T_CSEM *pk_csem)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r1) = (INT) pk_csem;
	REGISTER(r6) = TFN_CRE_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_sem(ID semid)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r6) = TFN_DEL_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_sem(T_RSEM *pk_rsem, ID semid)
{
	REGISTER(r0) = (INT) pk_rsem;
	REGISTER(r1) = (INT) semid;
	REGISTER(r6) = TFN_REF_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
wai_sem(ID semid)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r6) = TFN_WAI_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
sig_sem(ID semid)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r6) = TFN_SIG_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_mbx(ID mbxid, T_CMBX *pk_cmbx)
{
	REGISTER(r0) = (INT) mbxid;
	REGISTER(r1) = (INT) pk_cmbx;
	REGISTER(r6) = TFN_CRE_MBX;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_mbx(ID mbxid)
{
	REGISTER(r0) = (INT) mbxid;
	REGISTER(r6) = TFN_DEL_MBX;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_mbx(T_RMBX *pk_rmbx, ID mbxid)
{
	REGISTER(r0) = (INT) pk_rmbx;
	REGISTER(r1) = (INT) mbxid;
	REGISTER(r6) = TFN_REF_MBX;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rcv_msg(T_MSG* *ppk_msg, ID mbxid)
{
	REGISTER(r0) = (INT) ppk_msg;
	REGISTER(r1) = (INT) mbxid;
	REGISTER(r6) = TFN_RCV_MSG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
snd_msg(ID mbxid, T_MSG *pk_msg)
{
	REGISTER(r0) = (INT) mbxid;
	REGISTER(r1) = (INT) pk_msg;
	REGISTER(r6) = TFN_SND_MSG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
def_int(UINT dintno, T_DINT *pk_dint)
{
	REGISTER(r0) = (INT) dintno;
	REGISTER(r1) = (INT) pk_dint;
	REGISTER(r6) = TFN_DEF_INT;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
chg_ims(UINT imask)
{
	REGISTER(r0) = (INT) imask;
	REGISTER(r6) = TFN_CHG_IMS;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_ims(UINT *p_imask)
{
	REGISTER(r0) = (INT) p_imask;
	REGISTER(r6) = TFN_REF_IMS;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_mpf(ID mpfid, T_CMPF *pk_cmpf)
{
	REGISTER(r0) = (INT) mpfid;
	REGISTER(r1) = (INT) pk_cmpf;
	REGISTER(r6) = TFN_CRE_MPF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_mpf(ID mpfid)
{
	REGISTER(r0) = (INT) mpfid;
	REGISTER(r6) = TFN_DEL_MPF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_mpf(T_RMPF *pk_rmpf, ID mpfid)
{
	REGISTER(r0) = (INT) pk_rmpf;
	REGISTER(r1) = (INT) mpfid;
	REGISTER(r6) = TFN_REF_MPF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
get_blf(VP *p_blf, ID mpfid)
{
	REGISTER(r0) = (INT) p_blf;
	REGISTER(r1) = (INT) mpfid;
	REGISTER(r6) = TFN_GET_BLF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rel_blf(ID mpfid, VP blf)
{
	REGISTER(r0) = (INT) mpfid;
	REGISTER(r1) = (INT) blf;
	REGISTER(r6) = TFN_REL_BLF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vget_tim(SYSUTIME *pk_utim)
{
	REGISTER(r0) = (INT) pk_utim;
	REGISTER(r6) = TFN_VGET_TIM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
set_tim(SYSTIME *pk_tim)
{
	REGISTER(r0) = (INT) pk_tim;
	REGISTER(r6) = TFN_SET_TIM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
get_tim(SYSTIME *pk_tim)
{
	REGISTER(r0) = (INT) pk_tim;
	REGISTER(r6) = TFN_GET_TIM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
dly_tsk(DLYTIME dlytim)
{
	REGISTER(r0) = (INT) dlytim;
	REGISTER(r6) = TFN_DLY_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
def_alm(HNO almno, T_DALM *pk_dalm)
{
	REGISTER(r0) = (INT) almno;
	REGISTER(r1) = (INT) pk_dalm;
	REGISTER(r6) = TFN_DEF_ALM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
def_cyc(HNO cycno, T_DCYC *pk_dcyc)
{
	REGISTER(r0) = (INT) cycno;
	REGISTER(r1) = (INT) pk_dcyc;
	REGISTER(r6) = TFN_DEF_CYC;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_alm(T_RALM *pk_ralm, HNO almno)
{
	REGISTER(r0) = (INT) pk_ralm;
	REGISTER(r1) = (INT) almno;
	REGISTER(r6) = TFN_REF_ALM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_cyc(T_RCYC *pk_rcyc, HNO cycno)
{
	REGISTER(r0) = (INT) pk_rcyc;
	REGISTER(r1) = (INT) cycno;
	REGISTER(r6) = TFN_REF_CYC;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
act_cyc(HNO cycno, UINT cycact)
{
	REGISTER(r0) = (INT) cycno;
	REGISTER(r1) = (INT) cycact;
	REGISTER(r6) = TFN_ACT_CYC;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
pget_blf(VP *p_blf, ID mpfid)
{
	REGISTER(r0) = (INT) p_blf;
	REGISTER(r1) = (INT) mpfid;
	REGISTER(r6) = TFN_PGET_BLF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
pget_blk(VP *p_blk, ID mplid, INT blksz)
{
	REGISTER(r0) = (INT) p_blk;
	REGISTER(r1) = (INT) mplid;
	REGISTER(r2) = (INT) blksz;
	REGISTER(r6) = TFN_PGET_BLK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
pol_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode)
{
	REGISTER(r0) = (INT) p_flgptn;
	REGISTER(r1) = (INT) flgid;
	REGISTER(r2) = (INT) waiptn;
	REGISTER(r3) = (INT) wfmode;
	REGISTER(r6) = TFN_POL_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
preq_sem(ID semid)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r6) = TFN_PREQ_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
prcv_msg(T_MSG* *ppk_msg, ID mbxid)
{
	REGISTER(r0) = (INT) ppk_msg;
	REGISTER(r1) = (INT) mbxid;
	REGISTER(r6) = TFN_PRCV_MSG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
psnd_mbf(ID mbfid, VP msg, INT msgsz)
{
	REGISTER(r0) = (INT) mbfid;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) msgsz;
	REGISTER(r6) = TFN_PSND_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
prcv_mbf(VP msg, INT *p_msgsz, ID mbfid)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_msgsz;
	REGISTER(r2) = (INT) mbfid;
	REGISTER(r6) = TFN_PRCV_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
pcal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_rmsgsz;
	REGISTER(r2) = (INT) porid;
	REGISTER(r3) = (INT) calptn;
	REGISTER(r4) = (INT) cmsgsz;
	REGISTER(r6) = TFN_PCAL_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
pacp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn)
{
	REGISTER(r0) = (INT) p_rdvno;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) p_cmsgsz;
	REGISTER(r3) = (INT) porid;
	REGISTER(r4) = (INT) acpptn;
	REGISTER(r6) = TFN_PACP_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_mpl(ID mplid, T_CMPL *pk_cmpl)
{
	REGISTER(r0) = (INT) mplid;
	REGISTER(r1) = (INT) pk_cmpl;
	REGISTER(r6) = TFN_CRE_MPL;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_mpl(ID mplid)
{
	REGISTER(r0) = (INT) mplid;
	REGISTER(r6) = TFN_DEL_MPL;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_mpl(T_RMPL *pk_rmpl, ID mplid)
{
	REGISTER(r0) = (INT) pk_rmpl;
	REGISTER(r1) = (INT) mplid;
	REGISTER(r6) = TFN_REF_MPL;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
get_blk(VP *p_blk, ID mplid, INT blksz)
{
	REGISTER(r0) = (INT) p_blk;
	REGISTER(r1) = (INT) mplid;
	REGISTER(r2) = (INT) blksz;
	REGISTER(r6) = TFN_GET_BLK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rel_blk(ID mplid, VP blk)
{
	REGISTER(r0) = (INT) mplid;
	REGISTER(r1) = (INT) blk;
	REGISTER(r6) = TFN_REL_BLK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_por(ID porid, T_CPOR *pk_cpor)
{
	REGISTER(r0) = (INT) porid;
	REGISTER(r1) = (INT) pk_cpor;
	REGISTER(r6) = TFN_CRE_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_por(ID porid)
{
	REGISTER(r0) = (INT) porid;
	REGISTER(r6) = TFN_DEL_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_por(T_RPOR *pk_rpor, ID porid)
{
	REGISTER(r0) = (INT) pk_rpor;
	REGISTER(r1) = (INT) porid;
	REGISTER(r6) = TFN_REF_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_rmsgsz;
	REGISTER(r2) = (INT) porid;
	REGISTER(r3) = (INT) calptn;
	REGISTER(r4) = (INT) cmsgsz;
	REGISTER(r6) = TFN_CAL_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
acp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn)
{
	REGISTER(r0) = (INT) p_rdvno;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) p_cmsgsz;
	REGISTER(r3) = (INT) porid;
	REGISTER(r4) = (INT) acpptn;
	REGISTER(r6) = TFN_ACP_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rpl_rdv(RNO rdvno, VP msg, INT rmsgsz)
{
	REGISTER(r0) = (INT) rdvno;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) rmsgsz;
	REGISTER(r6) = TFN_RPL_RDV;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
fwd_por(ID porid, UINT calptn, RNO rdvno, VP msg, INT cmsgsz)
{
	REGISTER(r0) = (INT) porid;
	REGISTER(r1) = (INT) calptn;
	REGISTER(r2) = (INT) rdvno;
	REGISTER(r3) = (INT) msg;
	REGISTER(r4) = (INT) cmsgsz;
	REGISTER(r6) = TFN_FWD_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
cre_mbf(ID mbfid, T_CMBF *pk_cmbf)
{
	REGISTER(r0) = (INT) mbfid;
	REGISTER(r1) = (INT) pk_cmbf;
	REGISTER(r6) = TFN_CRE_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
del_mbf(ID mbfid)
{
	REGISTER(r0) = (INT) mbfid;
	REGISTER(r6) = TFN_DEL_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
ref_mbf(T_RMBF *pk_rmbf, ID mbfid)
{
	REGISTER(r0) = (INT) pk_rmbf;
	REGISTER(r1) = (INT) mbfid;
	REGISTER(r6) = TFN_REF_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
snd_mbf(ID mbfid, VP msg, INT msgsz)
{
	REGISTER(r0) = (INT) mbfid;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) msgsz;
	REGISTER(r6) = TFN_SND_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
rcv_mbf(VP msg, INT *p_msgsz, ID mbfid)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_msgsz;
	REGISTER(r2) = (INT) mbfid;
	REGISTER(r6) = TFN_RCV_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tget_blf(VP *p_blf, ID mpfid, TMO tmout)
{
	REGISTER(r0) = (INT) p_blf;
	REGISTER(r1) = (INT) mpfid;
	REGISTER(r2) = (INT) tmout;
	REGISTER(r6) = TFN_TGET_BLF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tget_blk(VP *p_blk, ID mplid, INT blksz, TMO tmout)
{
	REGISTER(r0) = (INT) p_blk;
	REGISTER(r1) = (INT) mplid;
	REGISTER(r2) = (INT) blksz;
	REGISTER(r3) = (INT) tmout;
	REGISTER(r6) = TFN_TGET_BLK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
twai_flg(UINT *p_flgptn, ID flgid, UINT waiptn, UINT wfmode, TMO tmout)
{
	REGISTER(r0) = (INT) p_flgptn;
	REGISTER(r1) = (INT) flgid;
	REGISTER(r2) = (INT) waiptn;
	REGISTER(r3) = (INT) wfmode;
	REGISTER(r4) = (INT) tmout;
	REGISTER(r6) = TFN_TWAI_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
twai_sem(ID semid, TMO tmout)
{
	REGISTER(r0) = (INT) semid;
	REGISTER(r1) = (INT) tmout;
	REGISTER(r6) = TFN_TWAI_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
trcv_msg(T_MSG* *ppk_msg, ID mbxid, TMO tmout)
{
	REGISTER(r0) = (INT) ppk_msg;
	REGISTER(r1) = (INT) mbxid;
	REGISTER(r2) = (INT) tmout;
	REGISTER(r6) = TFN_TRCV_MSG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tsnd_mbf(ID mbfid, VP msg, INT msgsz, TMO tmout)
{
	REGISTER(r0) = (INT) mbfid;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) msgsz;
	REGISTER(r3) = (INT) tmout;
	REGISTER(r6) = TFN_TSND_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
trcv_mbf(VP msg, INT *p_msgsz, ID mbfid, TMO tmout)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_msgsz;
	REGISTER(r2) = (INT) mbfid;
	REGISTER(r3) = (INT) tmout;
	REGISTER(r6) = TFN_TRCV_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tcal_por(VP msg, INT *p_rmsgsz, ID porid, UINT calptn, INT cmsgsz, TMO tmout)
{
	REGISTER(r0) = (INT) msg;
	REGISTER(r1) = (INT) p_rmsgsz;
	REGISTER(r2) = (INT) porid;
	REGISTER(r3) = (INT) calptn;
	REGISTER(r4) = (INT) cmsgsz;
	REGISTER(r5) = (INT) tmout;
	REGISTER(r6) = TFN_TCAL_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r5), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
tacp_por(RNO *p_rdvno, VP msg, INT *p_cmsgsz, ID porid, UINT acpptn, TMO tmout)
{
	REGISTER(r0) = (INT) p_rdvno;
	REGISTER(r1) = (INT) msg;
	REGISTER(r2) = (INT) p_cmsgsz;
	REGISTER(r3) = (INT) porid;
	REGISTER(r4) = (INT) acpptn;
	REGISTER(r5) = (INT) tmout;
	REGISTER(r6) = TFN_TACP_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r4), "g"(r5), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vset_reg(ID tskid, T_REGS *pk_regs, T_EIT *pk_eit, T_CREGS *pk_cregs)
{
	REGISTER(r0) = (INT) tskid;
	REGISTER(r1) = (INT) pk_regs;
	REGISTER(r2) = (INT) pk_eit;
	REGISTER(r3) = (INT) pk_cregs;
	REGISTER(r6) = TFN_VSET_REG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vget_reg(T_REGS *pk_regs, T_EIT *pk_eit, T_CREGS *pk_cregs, ID tskid)
{
	REGISTER(r0) = (INT) pk_regs;
	REGISTER(r1) = (INT) pk_eit;
	REGISTER(r2) = (INT) pk_cregs;
	REGISTER(r3) = (INT) tskid;
	REGISTER(r6) = TFN_VGET_REG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r1), "g"(r2), "g"(r3), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_tsk(T_CTSK *pk_ctsk)
{
	REGISTER(r0) = (INT) pk_ctsk;
	REGISTER(r6) = TFN_VCRE_TSK;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_sem(T_CSEM *pk_csem)
{
	REGISTER(r0) = (INT) pk_csem;
	REGISTER(r6) = TFN_VCRE_SEM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_flg(T_CFLG *pk_cflg)
{
	REGISTER(r0) = (INT) pk_cflg;
	REGISTER(r6) = TFN_VCRE_FLG;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_mbx(T_CMBX *pk_cmbx)
{
	REGISTER(r0) = (INT) pk_cmbx;
	REGISTER(r6) = TFN_VCRE_MBX;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_mbf(T_CMBF *pk_cmbf)
{
	REGISTER(r0) = (INT) pk_cmbf;
	REGISTER(r6) = TFN_VCRE_MBF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_por(T_CPOR *pk_cpor)
{
	REGISTER(r0) = (INT) pk_cpor;
	REGISTER(r6) = TFN_VCRE_POR;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_mpl(T_CMPL *pk_cmpl)
{
	REGISTER(r0) = (INT) pk_cmpl;
	REGISTER(r6) = TFN_VCRE_MPL;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vcre_mpf(T_CMPF *pk_cmpf)
{
	REGISTER(r0) = (INT) pk_cmpf;
	REGISTER(r6) = TFN_VCRE_MPF;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vdef_cyc(T_DCYC *pk_dcyc)
{
	REGISTER(r0) = (INT) pk_dcyc;
	REGISTER(r6) = TFN_VDEF_CYC;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

Inline ER
vdef_alm(T_DALM *pk_dalm)
{
	REGISTER(r0) = (INT) pk_dalm;
	REGISTER(r6) = TFN_VDEF_ALM;

	Asm("trapa #1"
	  : "g="(r0)
	  : "g"(r0), "g"(r6)
	  : "r1", "r2", "r3", "r4", "r5", "r6");
	return(r0);
}

#endif /* LINK_KERNEL */

#endif /* _ITIS_ISYSCALL_ */
