/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itron_errno.h,v 1.2 1993/11/19 09:19:19 hiro Exp $
 */

#ifndef _ITRON_ERRNO_
#define _ITRON_ERRNO_

/* 
 *  ITRON のエラーコードの定義
 */

#define	E_OK		0		/* 正常終了 */

#define E_SYS		(-5)		/* システムエラー */
#define	E_NOMEM		(-10)		/* メモリ不足 */
#define	E_NOSPT		(-17)		/* 未サポート機能 */
#define	E_INOSPT	(-18)		/* ITRON/FILE での未サポート機能 */
#define	E_RSFN		(-20)		/* 予約機能番号 */
#define	E_RSATR		(-24)		/* 予約属性 */
#define	E_PAR		(-33)		/* パラメターエラー */
#define	E_ID		(-35)		/* 不正ID番号 */
#define	E_NOEXS		(-52)		/* オブジェクトが存在していない */
#define	E_OBJ		(-63)		/* オブジェクトの状態が不正 */
#define	E_MACV		(-65)		/* メモリアクセス不能/アクセス権違反 */
#define	E_OACV		(-66)		/* オブジェクトアクセス権違反 */
#define	E_CTX		(-69)		/* コンテキストエラー */
#define	E_QOVR		(-73)		/* キューイングのオーバーフロー */
#define	E_DLT		(-81)		/* 待ちオブジェクトが削除された */
#define	E_TMOUT		(-85)		/* ポーリング失敗/タイムアウト */
#define	E_RLWAI		(-86)		/* 待ち状態強制解除 */

#define	EN_NOND		(-113)		/* 対象ノードが存在しない */
#define	EN_OBJNO	(-114)		/* アクセスできないオブジェクト番号 */
#define	EN_PROTO	(-115)		/* 未サポートプロトコル */
#define	EN_RSFN		(-116)		/* 未サポートシステムコール */
#define	EN_COMM		(-117)		/* 対象ノードからの応答がない */
#define	EN_RLWAI	(-118)		/* 接続機能に関わる待ち状態強制解除 */
#define	EN_PAR		(-119)		/* 要求時のパラメータエラー */
#define	EN_RPAR		(-120)		/* リターン時のパラメータエラー */
#define	EN_CTXID	(-121)		/* 接続機能を使えないコンテキスト */
#define	EN_EXEC		(-122)		/* 対象ノードにおける資源不足 */
#define	EN_NOSPT	(-123)		/* 未サポート接続機能 */

#define EV_FULL		(-225)		/* オブジェクトテーブルフル */

#endif /* _ITRON_ERRNO_ */
