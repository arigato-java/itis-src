/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itis_services.h,v 1.3 1993/12/14 13:23:58 hiro Exp $
 */

#ifndef _ITIS_SERVICES_
#define _ITIS_SERVICES_

/* 
 *  ItIsアプリケーション 標準インクルードファイル
 *
 *  ItIs のシステムコールを呼び出すアプリケーションは，すべてのこのファ
 *  イルをインクルードすべきである．
 *  システムコールをサブルーチンコールで呼び出す場合には，このファイル
 *  をインクルードする前に，LINK_KERNEL を定義 (#define) すること．
 */

#include <itron.h>
#include <itron_errno.h>
#include <itron_fncode.h>
#include <itis_debug.h>
#include <itis_stddefs.h>
#include <itis_isyscall.h>

/*
 *  システムコール呼出マクロ
 */
extern ER	itron_errno;
extern void	itis_perror(const char *file, int line, const char *expr);
extern void	itis_panic(const char *file, int line, const char *expr);

#define	syscall(s)	{ if ((itron_errno = (s)) < 0) {		\
				itis_perror(__FILE__, __LINE__, #s);	\
			} }

#define	_syscall(s)	{ if ((itron_errno = (s)) < 0) {		\
				itis_panic(__FILE__, __LINE__, #s);	\
			} }

#endif /* _ITIS_SERVICES_ */
