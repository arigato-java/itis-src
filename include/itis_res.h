/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: itis_resources.h,v 1.4 1994/01/19 09:36:29 hiro Exp $
 */

#ifndef	_ITIS_RESOURCES_
#define	_ITIS_RESOURCES_

/*
 *  システムで利用する資源の定義
 */

/*
 *  初期化タスク関連の定義
 */
#define TSK_INIT	(-5)		/* 初期化タスクのID */
#define TSK_INIT_CTSK	{ 0, TA_HLNG|TA_SSTKSZ, init_task, MIN_PRI, 8192, 512 }
extern void		init_task(int);

/*
 *  システムログタスク関連の定義
 */
#define	TSK_LOG		(-6)		/* システムログタスクのID */
#define TSK_LOG_CTSK	{ 0, TA_HLNG|TA_SSTKSZ, log_task, 5, 8192, 512 }
extern void		log_task(int);

#define	MBF_LOG_BUFSZ	2048		/* ログ用メッセージバッファのサイズ */
#define	MBF_LOG_MAXMSZ	256		/* ログメッセージの最大長 */
#define MBF_LOG_CMBF	{ 0, 0, MBF_LOG_BUFSZ, MBF_LOG_MAXMSZ }

/*
 *  シリアルインタフェースドライバ関連の定義
 */
#define SEM_SERIAL1_IN	(-5)
#define SEM_SERIAL1_OUT	(-6)

#define SEM_SERIAL2_IN	(-7)
#define SEM_SERIAL2_OUT	(-8)

/*
 *  初期起動タスク関連の定義
 */
#define TSK_FIRST	(-7)		/* 初期起動タスクのID */
#define TSK_FIRST_CTSK	{ 0, TA_HLNG, first_task, 20, 8192 }
extern void		first_task(int);

/*
 *  拡張SVC番号の定義
 */

/*
 *  シリアルインタフェースドライバの呼び出し
 */
#define SVC_SERIAL_INIT		1
#define SVC_SERIAL_READ		2
#define SVC_SERIAL_WRITE	3
#define SVC_SERIAL_IOCTL	4
#define SVC_SERIAL_FLUSH	5

/*
 *  システムログタスクへの出力
 */
#define SVC_SYSLOG_SEND		6

#endif /* _ITIS_RESOURCES_ */
