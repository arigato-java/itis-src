/**
 * 
 * 	    ItIs - ITRON Implementation by Sakamura Lab
 * 
 * Copyright (C) 1989-93 by Sakamura Lab, the University of Tokyo, JAPAN
 * 
 * 			All Rights Reserved
 * 
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose is hereby granted without fee, provided
 * that both the above copyright notice and this permission notice appear
 * in all copies and supporting documentation, and that the names of
 * Sakamura Lab and the University of Tokyo not be used in advertising or
 * publicity pertaining to distribution of the software without specific,
 * written prior permission.  Sakamura Lab makes no representations about
 * the suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 * 
 *                              NO WARRANTY
 *   
 * SAKAMURA LAB DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO
 * EVENT SHALL SAKAMURA LAB BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF
 * USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 * 
 *  @(#) $Id: mcubestart.S,v 1.2 1994/01/19 08:24:58 hiro Exp $
 */

#include "cregs.h"

/*
 *  ItIs 動作時のメモリマップ (デフォルト時)
 *
 *	80000000 - 8000ffff	ROMモニタワークエリア
 *	80020000 -		カーネルコード領域
 *		 - 8007efff	タスク独立部用スタック領域 (ItIs の SPI)
 *	8007f000 - 8007ffff	EITベクタテーブル (実際はこの半分のみ使用)
 *	80080000 - 800fffff	カーネルデータ領域 (大きくすることも可能)
 *	80100000 -		未使用 (ユーザプログラム領域)
 *		 - 80ffffff	ROMモニタの初期SPI (数バイト壊される)
 */

/*
 *  スタックエリア，ヒープエリアの定義
 *
 *  _end は，プログラムで使用するデータエリアの最後の次の番地．
 */

#define STACKTOP	0x8007f000	/* タスク独立部用スタックの初期値 */
#define HEAPTOP		_end		/* ヒープエリアの先頭 */
#define HEAPLIMIT	0x80100000	/* ヒープエリアの上限 */

/*
 *  EITベクタテーブル関連の定義
 *
 *  MCUBE の ROMモニタ (BMS) では，EITベクタテーブルを FFFFF000 番地か
 *  ら ROM に置いている．そこで，ItIs 起動時に，EITベクタテーブルを RAM 
 *  上に作る必要がある．eitvb_itis 番地に，EITベクタテーブルの初期設定
 *  を ROM からコピーして使う．
 */

#define EITVB_BMS	0xfffff000
#define EITVB_ITIS	0x8007f000
#define EITVT_LEN	0x800

/*
 *  MCUBE用スタートアップモジュール
 */
	.text
	.align 1
	.globl ___sys_init
___sys_init:
mcube_start:
	mov r0, r13			/* 戻り番地を R13 に保存 */
	stc @PSW, r0
	and #0x1fffffff, r0		/* SMRNG を 0 に設定 */
	ldc r0, @PSW
	mov #STACKTOP, sp		/* スタックポインタ (SPI) の初期化 */
	mov #0, fp			/* フレームポインタの初期化 */

	mov #EITVB_BMS, r0		/* EITベクタテーブルのコピー */
	mov #EITVB_ITIS, r1
	mov #(EITVT_LEN/4), r2
	smov/f/n.w
	ldc #EITVB_ITIS, @EITVB		/* EITVB の設定 */

	bsr ___rawio_init		/* ライブラリの初期化 */
	jmp @r13

/*
 *  低レベルメモリエリア確保ルーチン
 *
 *  void *sbrk(int size)
 */
	.text
	.align 1
	.globl _sbrk
_sbrk:
	mov @_break, r1			/* _break の現在値を取り出す */
	add r1, r0
	bxs sbrk_overflow		/* メモリフルのチェック */
	cmpu #HEAPLIMIT, r0
	bgt sbrk_overflow
	mov r0, @_break			/* _break の更新 */
	mov r1, r0			/* 更新前の _break を返す */
	rts
sbrk_overflow:
	mov #-1, r0			/* オーバーフローの場合は NADR */
	rts

	.data
	.align 2
_break:
	.int HEAPTOP
